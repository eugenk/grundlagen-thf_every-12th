thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_eps,type, (eps : (($i>$o)>$i)), unknown, [hashroot('5778fdda0af904b6c32faecb7a44b8df0c03c4b7b92d76304ab3c39a10b01cf7')]).
thf(epsR,axiom, (! [X0:($i>$o)] : ! [X1:$i] : ((X0@X1) => (X0@(eps@X0)))), unknown, [hashroot('7052a2a4c177ce4661bfa59cbb327cca2b47eb070ecf45ec47b2cb98464c0d3c')]).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_d_Subq,type, (d_Subq : ($i>$i>$o)), unknown, [hashroot('fa261ca77ccca447f1dc85420b1d3f16beee42e22be4ee36bbe046b6ce551ade')]).
thf(def_d_Subq,definition, (d_Subq = (^ [X0:$i] : ^ [X1:$i] : ! [X2:$i] : ((in@X2@X0) => (in@X2@X1)))), unknown).
thf(set_ext,axiom, (! [X0:$i] : ! [X1:$i] : ((d_Subq@X0@X1) => ((d_Subq@X1@X0) => (X0 = X1)))), unknown, [hashroot('47d5a9a699a18b14f18d38a39ffab3c71919e6a98e7afdd7669246c66b524b21')]).
thf(k_In_ind,axiom, (! [X0:($i>$o)] : ((! [X1:$i] : ((! [X2:$i] : ((in@X2@X1) => (X0@X2))) => (X0@X1))) => (! [X1:$i] : (X0@X1)))), unknown, [hashroot('8893139b5374d15c33e7c3b10b10fd11e43da2d80e765a5762f644ddf023f686')]).
thf(typ_emptyset,type, (emptyset : $i), unknown, [hashroot('7a53cc5deb60512f3dacacc7695dd5072077c6f4984dbedbff76e27092393b1c')]).
thf(k_EmptyAx,axiom, (~ ((? [X0:$i] : (in@X0@emptyset)))), unknown, [hashroot('d2dd48cebdba61cb762d298fa968b5c30d792941178817acd9f08a3fd82c888d')]).
thf(typ_union,type, (union : ($i>$i)), unknown, [hashroot('2d88972e9536ba54deed37db380728b3c58eab9bca1d173ae523098b0774cdf5')]).
thf(k_UnionEq,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@(union@X0)) <=> (? [X2:$i] : ((in@X1@X2) & (in@X2@X0))))), unknown, [hashroot('bfa424f6d68797dc7c75dbfb972b955baefdb2f0c311f7c1072a35e62133fc75')]).
thf(typ_power,type, (power : ($i>$i)), file('grundlagen.aut','l_e_st_set'), [hashroot('0c5490ca2f6d61c2d410e7907be97b3bc36b3e4de614e1f5431278dbccad4c79')]).
thf(k_PowerEq,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@(power@X0)) <=> (d_Subq@X1@X0))), unknown, [hashroot('061d71e299fe5f40f83bbfdf01f06534cd0d6ef76ef1afc8d773351cfa1f2d25')]).
thf(typ_repl,type, (repl : ($i>($i>$i)>$i)), unknown, [hashroot('9fb9d223021739797a567e44cd3d39edabcb391801c049e88f6eae6829ac67bc')]).
thf(k_ReplEq,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(repl@X0@X1)) <=> (? [X3:$i] : ((in@X3@X0) & (X2 = (X1@X3)))))), unknown, [hashroot('021c5e4a880db1ed2b233ee168f92ab63de3827e478aab9ac3697df720b07e8e')]).
thf(typ_d_Union_closed,type, (d_Union_closed : ($i>$o)), unknown, [hashroot('5eb766dcf08046e085c3a948c0fa8c33e993faaf644c341d4c45db4f58e6ed46')]).
thf(def_d_Union_closed,definition, (d_Union_closed = (^ [X0:$i] : ! [X1:$i] : ((in@X1@X0) => (in@(union@X1)@X0)))), unknown).
thf(typ_d_Power_closed,type, (d_Power_closed : ($i>$o)), unknown, [hashroot('ef429645cf56bac466ae74e836d483a96da0ee2ef535387e877462b4d2a92f77')]).
thf(def_d_Power_closed,definition, (d_Power_closed = (^ [X0:$i] : ! [X1:$i] : ((in@X1@X0) => (in@(power@X1)@X0)))), unknown).
thf(typ_d_Repl_closed,type, (d_Repl_closed : ($i>$o)), unknown, [hashroot('b244e382bffdf9135c788a33e9a26d4767be9b8b57d3ba8413df2f68144dad9f')]).
thf(def_d_Repl_closed,definition, (d_Repl_closed = (^ [X0:$i] : ! [X1:$i] : ((in@X1@X0) => (! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X1) => (in@(X2@X3)@X0))) => (in@(repl@X1@X2)@X0)))))), unknown).
thf(typ_d_ZF_closed,type, (d_ZF_closed : ($i>$o)), unknown, [hashroot('54097936e3f3f798a8c5f68aafa64c92ef0d0929b7fa78f9ce329aae66f8f180')]).
thf(def_d_ZF_closed,definition, (d_ZF_closed = (^ [X0:$i] : (((d_Union_closed@X0) & (d_Power_closed@X0)) & (d_Repl_closed@X0)))), unknown).
thf(typ_univof,type, (univof : ($i>$i)), unknown, [hashroot('047da525b8931691a621d885c817ad0e55dd2adce0d7df803729281769029bb5')]).
thf(k_UnivOf_In,axiom, (! [X0:$i] : (in@X0@(univof@X0))), unknown, [hashroot('0daf6f3c3c1fd3dd9f8edce26e165679b5afe75e8e1a590b30a37e4cc8f062c8')]).
thf(k_UnivOf_ZF_closed,axiom, (! [X0:$i] : (d_ZF_closed@(univof@X0))), unknown, [hashroot('dfa130cbecd91b9df497b18dbdaf3c4b06dcbde2660e51dced9937458e0d2076')]).
thf(typ_if,type, (if : ($o>$i>$i>$i)), unknown, [hashroot('69b3bf7e5be7a73d68c0df523b826ab42331e621d95845fe34dc35070d4876a8')]).
thf(def_if,definition, (if = (^ [X0:$o] : ^ [X1:$i] : ^ [X2:$i] : (eps@(^ [X3:$i] : ((X0 & (X3 = X1)) | ((~ (X0)) & (X3 = X2))))))), unknown).
thf(if_i_correct,axiom, (! [X0:$o] : ! [X1:$i] : ! [X2:$i] : ((X0 & ((if@X0@X1@X2) = X1)) | ((~ (X0)) & ((if@X0@X1@X2) = X2)))), unknown, [hashroot('4ab3e3061e21eb8a32b5b3e35254b74867bb46d3ccf81f7b97a0e0806e68b96d')]).
thf(if_i_0,axiom, (! [X0:$o] : ! [X1:$i] : ! [X2:$i] : ((~ (X0)) => ((if@X0@X1@X2) = X2))), unknown, [hashroot('510c6ef2c40f34685cc8864b5ad6747ff2b184181cba60a251aa2d887e4e6e9c')]).
thf(if_i_1,axiom, (! [X0:$o] : ! [X1:$i] : ! [X2:$i] : (X0 => ((if@X0@X1@X2) = X1))), unknown, [hashroot('5e290aa0732c017bac934f407c5fdcf3dfea0c05079606d0746528eb73db3f25')]).
thf(if_i_or,axiom, (! [X0:$o] : ! [X1:$i] : ! [X2:$i] : (((if@X0@X1@X2) = X1) | ((if@X0@X1@X2) = X2))), unknown, [hashroot('d74dc53a3ece97513518ab2e6a4ec07df39bab2d55bf4cf0b20a35d26f3001df')]).
thf(typ_nIn,type, (nIn : ($i>$i>$o)), unknown, [hashroot('a29fe5543416387f0f12f2507d0fd25095a37aaec1210c08270bf85e6a903cd6')]).
thf(def_nIn,definition, (nIn = (^ [X0:$i] : ^ [X1:$i] : (~ ((in@X0@X1))))), unknown).
thf(k_Subq_ref,axiom, (! [X0:$i] : (d_Subq@X0@X0)), unknown, [hashroot('78be750104403f3d07730d74523262758ad78d485f3657025362567e5277f38d')]).
thf(k_EmptyE,axiom, (! [X0:$i] : (nIn@X0@emptyset)), unknown, [hashroot('51012b893d688c46065510a8459b2361f1defc37cf479ce7d882fb7fa2013708')]).
thf(k_Subq_Empty,axiom, (! [X0:$i] : (d_Subq@emptyset@X0)), unknown, [hashroot('19a9f85a34c89eacf1a30a84724ef48d96ef1496c997a0e0680341c78c4e1503')]).
thf(k_Empty_Subq_eq,axiom, (! [X0:$i] : ((d_Subq@X0@emptyset) => (X0 = emptyset))), unknown, [hashroot('eccd124d12d6761646ce958a0b00e2b5901f0329b0db03b8d90e1f79e9357ea0')]).
thf(k_Empty_eq,axiom, (! [X0:$i] : ((! [X1:$i] : (nIn@X1@X0)) => (X0 = emptyset))), unknown, [hashroot('c49a813ea9b5cbc5ee79a0d54253ba744242d17d8d169ec8fc1f1bfa1bea29c1')]).
thf(k_UnionE,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@(union@X0)) => (? [X2:$i] : ((in@X1@X2) & (in@X2@X0))))), unknown, [hashroot('2e53c4d9fd306049b4405486fdd06239d5e7a5c8c9a73bb9aa915eecdefc35df')]).
thf(k_UnionE2,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@(union@X0)) => (! [X2:$o] : ((! [X3:$i] : ((in@X1@X3) => ((in@X3@X0) => X2))) => X2)))), unknown, [hashroot('a65cfa0117acb4a1b6094e144b36c15c217a727061d4852165af10dbee285cae')]).
thf(k_UnionI,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X1@X2) => ((in@X2@X0) => (in@X1@(union@X0))))), unknown, [hashroot('7f9f76079cce0ad85e77d5e48628642ac54524b57fd8c08e413fd5a5cbefaea6')]).
thf(k_PowerE,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@(power@X0)) => (d_Subq@X1@X0))), unknown, [hashroot('7cf148e8f96d649a01eca786e13b78bb10acecebbbc3f03a0ef99f14259fb6d2')]).
thf(k_PowerI,axiom, (! [X0:$i] : ! [X1:$i] : ((d_Subq@X1@X0) => (in@X1@(power@X0)))), unknown, [hashroot('8b9445a2bdec6181d0eed3fddea74af1a3215e773d2e2619271814aabbdd62f0')]).
thf(k_Empty_In_Power,axiom, (! [X0:$i] : (in@emptyset@(power@X0))), unknown, [hashroot('fcf0df2a2884fceb30f493338f3bade93d8905a0ac991f5131fbd95a2dfad5e4')]).
thf(k_Self_In_Power,axiom, (! [X0:$i] : (in@X0@(power@X0))), unknown, [hashroot('57d2441e5467aae861dbef63429a05820f6f758dce3fb3f50d9a05d66b6e42dc')]).
thf(k_ReplE,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(repl@X0@X1)) => (? [X3:$i] : ((in@X3@X0) & (X2 = (X1@X3)))))), unknown, [hashroot('99388d8c8143a4978cf49d73e4c8117544ffdf9475f3723974c3f21b205d3639')]).
thf(k_ReplE2,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(repl@X0@X1)) => (! [X3:$o] : ((! [X4:$i] : ((in@X4@X0) => ((X2 = (X1@X4)) => X3))) => X3)))), unknown, [hashroot('ce2d97750f2e1c44639f8abdbe468b2fc688d1a2577366e664798bd1d5ce0be0')]).
thf(k_ReplI,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@X0) => (in@(X1@X2)@(repl@X0@X1)))), unknown, [hashroot('91c57b954679a73f963b9cbdc3bee1b34a3710c22d7cdf9b9524e5369057bd5e')]).
thf(k_Repl_Empty,axiom, (! [X0:($i>$i)] : ((repl@emptyset@X0) = emptyset)), unknown, [hashroot('e0711fabc3bd9c57e6120be7b661778fd70f0369cb2149652c5f8fc1cbb78952')]).
thf(typ_d_UPair,type, (d_UPair : ($i>$i>$i)), unknown, [hashroot('d454c01b8630bbf8a86e58d23d704d7df1119d261af493e5dbdb5c3901c85567')]).
thf(def_d_UPair,definition, (d_UPair = (^ [X0:$i] : ^ [X1:$i] : (repl@(power@(power@emptyset))@(^ [X2:$i] : (if@(in@emptyset@X2)@X0@X1))))), unknown).
thf(k_UPairE,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X0@(d_UPair@X1@X2)) => ((X0 = X1) | (X0 = X2)))), unknown, [hashroot('cf63d417b37cad68ffa712b9b13b42d3f5ed4751c4b72204495c36224051d2a0')]).
thf(k_UPairI1,axiom, (! [X0:$i] : ! [X1:$i] : (in@X0@(d_UPair@X0@X1))), unknown, [hashroot('6c3e97d6fb86eb6020582e5b4ca2e9aba3b3e2830f1e28c2f1acf013fb9b8317')]).
thf(k_UPairI2,axiom, (! [X0:$i] : ! [X1:$i] : (in@X1@(d_UPair@X0@X1))), unknown, [hashroot('2b06c47227c75d2ad730649e57fab4d1eb5cbb63fcedb258088cb11fced4d272')]).
thf(typ_d_Sing,type, (d_Sing : ($i>$i)), unknown, [hashroot('3d0e7d87abaf8c35704a9643f83ae90cf9305eae59effbeab1d27883173ef118')]).
thf(def_d_Sing,definition, (d_Sing = (^ [X0:$i] : (d_UPair@X0@X0))), unknown).
thf(k_SingI,axiom, (! [X0:$i] : (in@X0@(d_Sing@X0))), unknown, [hashroot('d9fa09a928a3d9573b51badc577ce28783bbd40e9869e602f2d274b19d9dbb91')]).
thf(k_SingE,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@(d_Sing@X0)) => (X1 = X0))), unknown, [hashroot('db27f17436db31402c18aad371037549bb25b9c544a3271afcedff752167ada4')]).
thf(typ_binunion,type, (binunion : ($i>$i>$i)), unknown, [hashroot('e7307b80a73d6a7225c5d6678864be5f73f4d4917612b3a13cf5b54f15a08bbd')]).
thf(def_binunion,definition, (binunion = (^ [X0:$i] : ^ [X1:$i] : (union@(d_UPair@X0@X1)))), unknown).
thf(binunionI1,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@X0) => (in@X2@(binunion@X0@X1)))), unknown, [hashroot('662242cf3d2e8cc5c49f20c35e655dab6f2f7fb5cbd7ecdd8bdcfd3d03d651c9')]).
thf(binunionI2,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@X1) => (in@X2@(binunion@X0@X1)))), unknown, [hashroot('884fdfb6b348163deed4b75f0f38a0af40a728a5370a62ffa58ed9915ecdf2f3')]).
thf(binunionE,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@(binunion@X0@X1)) => ((in@X2@X0) | (in@X2@X1)))), unknown, [hashroot('83de53e1dc155ae3d110b0da0c6fe61779aa713693fdd6ec8aad20900ddae2b2')]).
thf(k_Repl_restr_1,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X0) => ((X1@X3) = (X2@X3)))) => (d_Subq@(repl@X0@X1)@(repl@X0@X2)))), unknown, [hashroot('71b0c0225cc8400659003a981bbfe6fed8c60503b48819b7c8d37d7539593502')]).
thf(k_Repl_restr,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X0) => ((X1@X3) = (X2@X3)))) => ((repl@X0@X1) = (repl@X0@X2)))), unknown, [hashroot('624f45fcfbe4a6e26861c9a0473188fdfae3d329cbcdb4cdcf4bac9f0b9ca14d')]).
thf(typ_famunion,type, (famunion : ($i>($i>$i)>$i)), unknown, [hashroot('9af9f7de1e7545aa87e6733228b6efd1f9d9d4c0cbca427a0da9aaf736473129')]).
thf(def_famunion,definition, (famunion = (^ [X0:$i] : ^ [X1:($i>$i)] : (union@(repl@X0@X1)))), unknown).
thf(famunionI,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ! [X3:$i] : ((in@X2@X0) => ((in@X3@(X1@X2)) => (in@X3@(famunion@X0@X1))))), unknown, [hashroot('2f9f19c852fe6b45819def2dbc3c01656142eed6ae23a00853ef18ac93e22456')]).
thf(famunionE,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(famunion@X0@X1)) => (? [X3:$i] : ((in@X3@X0) & (in@X2@(X1@X3)))))), unknown, [hashroot('d10bf42b56720ca1de13f4eea8b3c5d6fae18057bb12907da49b6e7779c31741')]).
thf(typ_d_Sep,type, (d_Sep : ($i>($i>$o)>$i)), [file('grundlagen.aut','l_e_ot'),file('grundlagen.aut','l_e_st_setof')], [hashroot('218a016b04c605cddd8baabd01b9102f19cbd21e943087fb26bfbb5c1990d944')]).
thf(def_d_Sep,definition, (d_Sep = (^ [X0:$i] : ^ [X1:($i>$o)] : (if@(? [X2:$i] : ((in@X2@X0) & (X1@X2)))@(repl@X0@(^ [X2:$i] : (if@(X1@X2)@X2@(eps@(^ [X3:$i] : ((in@X3@X0) & (X1@X3)))))))@emptyset))), [file('grundlagen.aut','l_e_ot'),file('grundlagen.aut','l_e_st_setof')]).
thf(k_SepI,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ! [X2:$i] : ((in@X2@X0) => ((X1@X2) => (in@X2@(d_Sep@X0@X1))))), unknown, [hashroot('684bc50b03ef9dfb344d009751624d7ba2666babf62f83c03d847388e6e82d2a')]).
thf(k_SepE,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ! [X2:$i] : ((in@X2@(d_Sep@X0@X1)) => ((in@X2@X0) & (X1@X2)))), unknown, [hashroot('94d2c0e3f231369dd865f04ac4edf897b14d3290827183c9e4382cf455535b61')]).
thf(k_SepE1,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ! [X2:$i] : ((in@X2@(d_Sep@X0@X1)) => (in@X2@X0))), unknown, [hashroot('23d9797f1f33195f72a35730c3557c7009925b9fc15b720d47cf93ea0f0d3817')]).
thf(k_SepE2,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ! [X2:$i] : ((in@X2@(d_Sep@X0@X1)) => (X1@X2))), unknown, [hashroot('781f090dda2dbb211f614ccedc3bad51103bdd0d61383b2a07feb69960faa38b')]).
thf(k_Sep_Subq,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (d_Subq@(d_Sep@X0@X1)@X0)), unknown, [hashroot('8a3ddf56312639bf913240c2a1d11be4ce045a078cbcbd5d760c3d91f307068e')]).
thf(k_Sep_In_Power,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (in@(d_Sep@X0@X1)@(power@X0))), unknown, [hashroot('04ccc04d6a480c67377f08465a786dac3c442195b2a6c78ef0706d89f117e9e7')]).
thf(typ_d_ReplSep,type, (d_ReplSep : ($i>($i>$o)>($i>$i)>$i)), unknown, [hashroot('f61ff10d2fedbd5a5c91c7330102c72a9e0df067cd1c500eb73872f642ec4f00')]).
thf(def_d_ReplSep,definition, (d_ReplSep = (^ [X0:$i] : ^ [X1:($i>$o)] : (repl@(d_Sep@X0@X1)))), unknown).
thf(k_ReplSepI,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ! [X2:($i>$i)] : ! [X3:$i] : ((in@X3@X0) => ((X1@X3) => (in@(X2@X3)@(d_ReplSep@X0@X1@X2))))), unknown, [hashroot('6a530426e83202ac9f54704d5b01f776c52e4f5954f2925f76c6d8542f8197f9')]).
thf(k_ReplSepE,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ! [X2:($i>$i)] : ! [X3:$i] : ((in@X3@(d_ReplSep@X0@X1@X2)) => (? [X4:$i] : (((in@X4@X0) & (X1@X4)) & (X3 = (X2@X4)))))), unknown, [hashroot('f9daae7c25acc944a92c39c1b9867a2489278ec47d13891a5db10939a7b5a4bc')]).
thf(k_ReplSepE2,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ! [X2:($i>$i)] : ! [X3:$i] : ((in@X3@(d_ReplSep@X0@X1@X2)) => (! [X4:$o] : ((! [X5:$i] : ((in@X5@X0) => ((X1@X5) => ((X3 = (X2@X5)) => X4)))) => X4)))), unknown, [hashroot('e16873e40678fb98954ce6c610b590b82e94a47a5716a9399b9c5ea0d51203b3')]).
thf(typ_setminus,type, (setminus : ($i>$i>$i)), unknown, [hashroot('5bda0a01cfa0de8611905e9e39c4534b76408c13473290c7bf62374b9a3ad4c0')]).
thf(def_setminus,definition, (setminus = (^ [X0:$i] : ^ [X1:$i] : (d_Sep@X0@(^ [X2:$i] : (nIn@X2@X1))))), unknown).
thf(setminusI,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@X0) => ((nIn@X2@X1) => (in@X2@(setminus@X0@X1))))), unknown, [hashroot('893130275377645b3fc6d338f5d8a406af5f5899c2a61f1d6e2fa7496d18836e')]).
thf(setminusE,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@(setminus@X0@X1)) => ((in@X2@X0) & (nIn@X2@X1)))), unknown, [hashroot('8996e2f68d2ae7e6880207bd3d62892979a22ecbb34cf0efe9c477f74723be0e')]).
thf(setminusE1,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@(setminus@X0@X1)) => (in@X2@X0))), unknown, [hashroot('eb4546cbcef5957d1f46a7dfda8093c0291ddfd7551fb9bb3f4b1b9d0370d51f')]).
thf(k_In_no2cycle,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X0@X1) => (nIn@X1@X0))), unknown, [hashroot('145139b160d45d83109157b80193c99b664364bc7cde8b1505c97957cdcc4770')]).
thf(typ_d_In_rec_G,type, (d_In_rec_G : (($i>($i>$i)>$i)>$i>$i>$o)), unknown, [hashroot('400833ba534654e1194cf7709beb2ee1e9b0bd2e0472a98846ec1026e3555898')]).
thf(def_d_In_rec_G,definition, (d_In_rec_G = (^ [X0:($i>($i>$i)>$i)] : ^ [X1:$i] : ^ [X2:$i] : ! [X3:($i>$i>$o)] : ((! [X4:$i] : ! [X5:($i>$i)] : ((! [X6:$i] : ((in@X6@X4) => (X3@X6@(X5@X6)))) => (X3@X4@(X0@X4@X5)))) => (X3@X1@X2)))), unknown).
thf(typ_d_In_rec,type, (d_In_rec : (($i>($i>$i)>$i)>$i>$i)), unknown, [hashroot('2f185987597aece852744f5cdd607286d384c1347d3c31d217804d7efb205c85')]).
thf(def_d_In_rec,definition, (d_In_rec = (^ [X0:($i>($i>$i)>$i)] : ^ [X1:$i] : (eps@(d_In_rec_G@X0@X1)))), unknown).
thf(k_In_rec_G_c,axiom, (! [X0:($i>($i>$i)>$i)] : ! [X1:$i] : ! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X1) => (d_In_rec_G@X0@X3@(X2@X3)))) => (d_In_rec_G@X0@X1@(X0@X1@X2)))), unknown, [hashroot('9719109570eeb29e27161e0f92bf386412ad86a36eddfdf2387a8243bf68c516')]).
thf(k_In_rec_G_inv,axiom, (! [X0:($i>($i>$i)>$i)] : ! [X1:$i] : ! [X2:$i] : ((d_In_rec_G@X0@X1@X2) => (? [X3:($i>$i)] : ((! [X4:$i] : ((in@X4@X1) => (d_In_rec_G@X0@X4@(X3@X4)))) & (X2 = (X0@X1@X3)))))), unknown, [hashroot('1d34bc1e00bd618fa7a86ff9cf07530c27e3a226dec7357622c12c896367e176')]).
thf(k_In_rec_G_f,axiom, (! [X0:($i>($i>$i)>$i)] : ((! [X1:$i] : ! [X2:($i>$i)] : ! [X3:($i>$i)] : ((! [X4:$i] : ((in@X4@X1) => ((X2@X4) = (X3@X4)))) => ((X0@X1@X2) = (X0@X1@X3)))) => (! [X1:$i] : ! [X2:$i] : ! [X3:$i] : ((d_In_rec_G@X0@X1@X2) => ((d_In_rec_G@X0@X1@X3) => (X2 = X3)))))), unknown, [hashroot('1c4ca987356086decb0d543d6e18017c24cf91d748a9b6c76f49eea4f974ce4f')]).
thf(k_In_rec_G_In_rec,axiom, (! [X0:($i>($i>$i)>$i)] : ((! [X1:$i] : ! [X2:($i>$i)] : ! [X3:($i>$i)] : ((! [X4:$i] : ((in@X4@X1) => ((X2@X4) = (X3@X4)))) => ((X0@X1@X2) = (X0@X1@X3)))) => (! [X1:$i] : (d_In_rec_G@X0@X1@(d_In_rec@X0@X1))))), unknown, [hashroot('f3cd1c80f12ee4be8ff066af7bfbfb5d9f724fd166fd79f23755966aba443933')]).
thf(k_In_rec_G_In_rec_d,axiom, (! [X0:($i>($i>$i)>$i)] : ((! [X1:$i] : ! [X2:($i>$i)] : ! [X3:($i>$i)] : ((! [X4:$i] : ((in@X4@X1) => ((X2@X4) = (X3@X4)))) => ((X0@X1@X2) = (X0@X1@X3)))) => (! [X1:$i] : (d_In_rec_G@X0@X1@(X0@X1@(d_In_rec@X0)))))), unknown, [hashroot('83c1d2c18975b73ecf6c34110ec51418b1b36fdcd662d9114c37e1cd2c37d7ca')]).
thf(k_In_rec_eq,axiom, (! [X0:($i>($i>$i)>$i)] : ((! [X1:$i] : ! [X2:($i>$i)] : ! [X3:($i>$i)] : ((! [X4:$i] : ((in@X4@X1) => ((X2@X4) = (X3@X4)))) => ((X0@X1@X2) = (X0@X1@X3)))) => (! [X1:$i] : ((d_In_rec@X0@X1) = (X0@X1@(d_In_rec@X0)))))), unknown, [hashroot('c2ec0bf8a6e73ad7776f1e763ecab1b3b4ab04b66c6bf2cfb238e0c7f0aaba7a')]).
thf(typ_ordsucc,type, (ordsucc : ($i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_suc'),file('grundlagen.aut','l_e_st_eq_landau_n_8274_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8276_xs')], [hashroot('6f2bd357b24438c387f3433d2366b843d110c03ef0b8bd82c5a0cb92bfc5b50d')]).
thf(def_ordsucc,definition, (ordsucc = (^ [X0:$i] : (binunion@X0@(d_Sing@X0)))), [file('grundlagen.aut','l_e_st_eq_landau_n_suc'),file('grundlagen.aut','l_e_st_eq_landau_n_8274_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8276_xs')]).
thf(ordsuccI2,axiom, (! [X0:$i] : (in@X0@(ordsucc@X0))), unknown, [hashroot('d43645e9caac9565a495242de6b882d2daa2f1f1ec85810e9c589a7d894d3efd')]).
thf(ordsuccE,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@(ordsucc@X0)) => ((in@X1@X0) | (X1 = X0)))), unknown, [hashroot('9462946aa9bc9bf7c308df71d37fa2ed0e35c358111211225eb8dcfbb8bcfb87')]).
thf(neq_0_ordsucc,axiom, (! [X0:$i] : (emptyset != (ordsucc@X0))), unknown, [hashroot('c1d31fd9899f8c0db7499c282ffcb2372d7699389e5078eee123be77b037d758')]).
thf(neq_ordsucc_0,axiom, (! [X0:$i] : ((ordsucc@X0) != emptyset)), unknown, [hashroot('04ee3c23dcd80f48864fd91cc6f02b9c5d3cc48a1297b01e6c7d2b2594cc889a')]).
thf(ordsucc_inj,axiom, (! [X0:$i] : ! [X1:$i] : (((ordsucc@X0) = (ordsucc@X1)) => (X0 = X1))), unknown, [hashroot('c02831052099da1ebadfe8777c05e7a053c53dcb0d69492dbe73c72bfe3bbab6')]).
thf(k_In_0_1,axiom, (in@emptyset@(ordsucc@emptyset)), unknown, [hashroot('b69031fd4452ceca19ac4a3b970e518795b48372d08bc76cebe4f03d1592cf5e')]).
thf(neq_1_0,axiom, ((ordsucc@emptyset) != emptyset), unknown, [hashroot('75ada4502f40d8fe8b4527799f8ca64317756e7026d37a0164df6ac02a100d61')]).
thf(k_Subq_1_Sing0,axiom, (d_Subq@(ordsucc@emptyset)@(d_Sing@emptyset)), unknown, [hashroot('5e131abd7072896ef8dba0e6fdaa7d061b6f76e663be8aa02a2bd9718b7b2256')]).
thf(typ_nat_p,type, (nat_p : ($i>$o)), unknown, [hashroot('5474028e209a7062cbd928510f0100d3ceddae245b1fc8076893a89c61bb9e3e')]).
thf(def_nat_p,definition, (nat_p = (^ [X0:$i] : ! [X1:($i>$o)] : ((X1@emptyset) => ((! [X2:$i] : ((X1@X2) => (X1@(ordsucc@X2)))) => (X1@X0))))), unknown).
thf(nat_0,axiom, (nat_p@emptyset), unknown, [hashroot('76d29d988f041cc6006339b4d2b5dae1bf321fd59db7607bc0ac344a579632d0')]).
thf(nat_ordsucc,axiom, (! [X0:$i] : ((nat_p@X0) => (nat_p@(ordsucc@X0)))), unknown, [hashroot('d137d30a6a6a2f902df1a1cad2cc95f7c39eb1ada834028e260d70f577bb4469')]).
thf(nat_1,axiom, (nat_p@(ordsucc@emptyset)), unknown, [hashroot('b709dd10f96f38a3b31d4fb656271fe75306bf5b20cd4cf10c06bbc5fe2ec21a')]).
thf(nat_ind,axiom, (! [X0:($i>$o)] : ((X0@emptyset) => ((! [X1:$i] : ((nat_p@X1) => ((X0@X1) => (X0@(ordsucc@X1))))) => (! [X1:$i] : ((nat_p@X1) => (X0@X1)))))), unknown, [hashroot('53e26ae95acb9290290ea61a97fea3e5cc2bf975f4c89ff34243f23b439634ff')]).
thf(nat_inv,axiom, (! [X0:$i] : ((nat_p@X0) => ((X0 = emptyset) | (? [X1:$i] : ((nat_p@X1) & (X0 = (ordsucc@X1))))))), unknown, [hashroot('06ad06ee2165944dd2b12cecd2dab3ca7692eb1e8aef227c18cf20310b2d1e5a')]).
thf(k_ZF_closeE,axiom, (! [X0:$i] : ((d_ZF_closed@X0) => (! [X1:$o] : (((d_Union_closed@X0) => ((d_Power_closed@X0) => ((d_Repl_closed@X0) => X1))) => X1)))), unknown, [hashroot('7537e8ddc6e4d93b1da67eca50ee82bad6975fb24fcbd81e4e87baa1851bc2be')]).
thf(k_ZF_Union_closed,axiom, (! [X0:$i] : ((d_ZF_closed@X0) => (! [X1:$i] : ((in@X1@X0) => (in@(union@X1)@X0))))), unknown, [hashroot('fdc63b805417cbd38ed10d190e4c35da458845d956376fc736d51e24b269cc78')]).
thf(k_ZF_Power_closed,axiom, (! [X0:$i] : ((d_ZF_closed@X0) => (! [X1:$i] : ((in@X1@X0) => (in@(power@X1)@X0))))), unknown, [hashroot('cedc27c3e68c98e157f44e7f7ad5e50f85236f4983e9a51cb34633926049703b')]).
thf(k_ZF_Repl_closed,axiom, (! [X0:$i] : ((d_ZF_closed@X0) => (! [X1:$i] : ((in@X1@X0) => (! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X1) => (in@(X2@X3)@X0))) => (in@(repl@X1@X2)@X0))))))), unknown, [hashroot('1c7d05904474f247b311bbf7833cc9e80e4907020e7fcbc9cacd51c4e890b8ee')]).
thf(k_ZF_UPair_closed,axiom, (! [X0:$i] : ((d_ZF_closed@X0) => (! [X1:$i] : ((in@X1@X0) => (! [X2:$i] : ((in@X2@X0) => (in@(d_UPair@X1@X2)@X0))))))), unknown, [hashroot('9bc03edf29652583eab7d286ffa369204587c1b65a14c87b3b3e0987d2b49af7')]).
thf(k_ZF_Sing_closed,axiom, (! [X0:$i] : ((d_ZF_closed@X0) => (! [X1:$i] : ((in@X1@X0) => (in@(d_Sing@X1)@X0))))), unknown, [hashroot('e95f88b0f29fa9aded12c0e9619111f87c10971e054faf13486d111c8b5c2b5e')]).
thf(k_ZF_binunion_closed,axiom, (! [X0:$i] : ((d_ZF_closed@X0) => (! [X1:$i] : ((in@X1@X0) => (! [X2:$i] : ((in@X2@X0) => (in@(binunion@X1@X2)@X0))))))), unknown, [hashroot('a8e6ea7ba951ba5c2134883c4c3ad38a7782a07fc35fdcf1f7a33fa2b405400d')]).
thf(k_ZF_ordsucc_closed,axiom, (! [X0:$i] : ((d_ZF_closed@X0) => (! [X1:$i] : ((in@X1@X0) => (in@(ordsucc@X1)@X0))))), unknown, [hashroot('76fe1fb3b3506d4bfabc797f8f7f3ab6a401a6ce31eb172b7da6e34e4e323dc2')]).
thf(nat_p_UnivOf_Empty,axiom, (! [X0:$i] : ((nat_p@X0) => (in@X0@(univof@emptyset)))), unknown, [hashroot('435eccfe7610364675fa7482aacfd118462d49a0a1c8541a5fd0a175fbba2065')]).
thf(typ_omega,type, (omega : $i), unknown, [hashroot('9fd2c016c7970674e08e2597878ae33b31bbb68bc9e486bb8a7b0ba3cf635098')]).
thf(def_omega,definition, (omega = (d_Sep@(univof@emptyset)@nat_p)), unknown).
thf(omega_nat_p,axiom, (! [X0:$i] : ((in@X0@omega) => (nat_p@X0))), unknown, [hashroot('7109ade8beb99f38add1e8406e1002ae97ddfe6efe72ef4cbebd490d21da6d82')]).
thf(nat_p_omega,axiom, (! [X0:$i] : ((nat_p@X0) => (in@X0@omega))), unknown, [hashroot('fc6260159e64593873cbfc6957af156aef62c1982c830f7efc2994f44e6e8327')]).
thf(typ_d_Inj1,type, (d_Inj1 : ($i>$i)), unknown, [hashroot('a164ed09342e2b45a63c21d30bc8f313ee85ebab233f4bc7bc65fbc7e797d0e1')]).
thf(def_d_Inj1,definition, (d_Inj1 = (d_In_rec@(^ [X0:$i] : ^ [X1:($i>$i)] : (binunion@(d_Sing@emptyset)@(repl@X0@X1))))), unknown).
thf(k_Inj1_eq,axiom, (! [X0:$i] : ((d_Inj1@X0) = (binunion@(d_Sing@emptyset)@(repl@X0@d_Inj1)))), unknown, [hashroot('497a2f11d6f2085e2cc146739b6dce7342ca1c435d6815b205306dad72c38d79')]).
thf(k_Inj1I1,axiom, (! [X0:$i] : (in@emptyset@(d_Inj1@X0))), unknown, [hashroot('7246a8a94bfa30ce85539f970e2858330787d09916ecf28cf9b516233d3e3816')]).
thf(k_Inj1I2,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@X0) => (in@(d_Inj1@X1)@(d_Inj1@X0)))), unknown, [hashroot('0fc52d696494514fd2613e82e86d7c41cb0f79b6a1b8f394fd28b3cb694cea94')]).
thf(k_Inj1E,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@(d_Inj1@X0)) => ((X1 = emptyset) | (? [X2:$i] : ((in@X2@X0) & (X1 = (d_Inj1@X2))))))), unknown, [hashroot('ccf8fe03fb7ce5ae079643e38b99c4dcf2a5d92c7dc2de9bf6020af58f1d7ebc')]).
thf(k_Inj1NE1,axiom, (! [X0:$i] : ((d_Inj1@X0) != emptyset)), unknown, [hashroot('8df27023760a93e4af8da80c6922a6bd2f757e32923e73056a73410d5cd1d32c')]).
thf(k_Inj1NE2,axiom, (! [X0:$i] : (nIn@(d_Inj1@X0)@(d_Sing@emptyset))), unknown, [hashroot('253ecf9d2e6ae3f3701758dd379cc50937b21b3ac995956a1a3eb7ddac8cc991')]).
thf(typ_d_Inj0,type, (d_Inj0 : ($i>$i)), unknown, [hashroot('db62f399928872ac8bcace852d2e8d46ca4a39ecdf996b6462819dddd835285d')]).
thf(def_d_Inj0,definition, (d_Inj0 = (^ [X0:$i] : (repl@X0@d_Inj1))), unknown).
thf(k_Inj0I,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@X0) => (in@(d_Inj1@X1)@(d_Inj0@X0)))), unknown, [hashroot('8a69f03b19fde53176c4c11dd9ac6c150722dd1ea38cf734eb91d1191c5454c1')]).
thf(k_Inj0E,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@(d_Inj0@X0)) => (? [X2:$i] : ((in@X2@X0) & (X1 = (d_Inj1@X2)))))), unknown, [hashroot('e68b12ef340888dfa71ea40ed3408bca8df3985449454a1c3c41e43a418c1dd5')]).
thf(typ_d_Unj,type, (d_Unj : ($i>$i)), unknown, [hashroot('9f8b3822cc322adfdca97be97f7c5d997fd8977f245dabdf315f87503f679fe9')]).
thf(def_d_Unj,definition, (d_Unj = (d_In_rec@(^ [X0:$i] : (repl@(setminus@X0@(d_Sing@emptyset)))))), unknown).
thf(k_Unj_eq,axiom, (! [X0:$i] : ((d_Unj@X0) = (repl@(setminus@X0@(d_Sing@emptyset))@d_Unj))), unknown, [hashroot('fe8b745653bc9031c7c4ca22f66fc9cbf0b533a62b3ae29cd14642b437289145')]).
thf(k_Unj_Inj1_eq,axiom, (! [X0:$i] : ((d_Unj@(d_Inj1@X0)) = X0)), unknown, [hashroot('4b241b23f25c007542ad34914538940cd5571822d08cb6772302f81c9909e184')]).
thf(k_Inj1_inj,axiom, (! [X0:$i] : ! [X1:$i] : (((d_Inj1@X0) = (d_Inj1@X1)) => (X0 = X1))), unknown, [hashroot('2c08370b974bb7227086b6541dc2ffaca3df972fb40aa3f96b263e930d8b8554')]).
thf(k_Unj_Inj0_eq,axiom, (! [X0:$i] : ((d_Unj@(d_Inj0@X0)) = X0)), unknown, [hashroot('5a5ab3198e4baa646d2d00a582655cd29c954de39df46a04810d28d01a8ae2b1')]).
thf(k_Inj0_inj,axiom, (! [X0:$i] : ! [X1:$i] : (((d_Inj0@X0) = (d_Inj0@X1)) => (X0 = X1))), unknown, [hashroot('56f46efe2c77cfff8845a52bd7f9c610482380722ee1d0756046572e679a38f0')]).
thf(k_Inj0_0,axiom, ((d_Inj0@emptyset) = emptyset), unknown, [hashroot('dd6d4fca32f4e8e9ea5cf31a06ae6185abd394fca69a3fbac22d2c79b579f683')]).
thf(k_Inj0_Inj1_neq,axiom, (! [X0:$i] : ! [X1:$i] : ((d_Inj0@X0) != (d_Inj1@X1))), unknown, [hashroot('d77c6fbcc66bbd9789d8a4e0410815d316cb3795598110549ff28d6433d82402')]).
thf(typ_pair,type, (pair : ($i>$i>$i)), unknown, [hashroot('554ca707aceb730b8cbae63a4a990c49db6ef782d7483039c889d08d23d994a5')]).
thf(def_pair,definition, (pair = (^ [X0:$i] : ^ [X1:$i] : (binunion@(repl@X0@d_Inj0)@(repl@X1@d_Inj1)))), unknown).
thf(k_Inj0_setsum,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@X0) => (in@(d_Inj0@X2)@(pair@X0@X1)))), unknown, [hashroot('3f664f9f1bedd0f6572f53be942a0a91712c833b317fb73fd5956ad521b3ee9a')]).
thf(k_Inj1_setsum,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@X1) => (in@(d_Inj1@X2)@(pair@X0@X1)))), unknown, [hashroot('29d465c8eda667eb7ed37b51de1d859606789392ebc7bdb8908a6714c7d236ec')]).
thf(setsum_Inj_inv,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@(pair@X0@X1)) => ((? [X3:$i] : ((in@X3@X0) & (X2 = (d_Inj0@X3)))) | (? [X3:$i] : ((in@X3@X1) & (X2 = (d_Inj1@X3))))))), unknown, [hashroot('e41b80e03df41ebec628e524e871ce1739be094c74302c02005137678a78b0b3')]).
thf(k_Inj0_setsum_0L,axiom, (! [X0:$i] : ((pair@emptyset@X0) = (d_Inj0@X0))), unknown, [hashroot('169f81e2fca791edb1bd11ce1f9e65d95c6bde0e0ca06101c8fe0e4f11c177d4')]).
thf(k_Inj1_setsum_1L,axiom, (! [X0:$i] : ((pair@(ordsucc@emptyset)@X0) = (d_Inj1@X0))), unknown, [hashroot('8dde4ccfb7eb9fcdd422eed3e4a321246979b5522711d44123cf27c889d2bb82')]).
thf(typ_proj0,type, (proj0 : ($i>$i)), unknown, [hashroot('b1cf7211981652cdc94339b5b40bedf1086be94fb4ae8cea9e765fa5d06cb67f')]).
thf(def_proj0,definition, (proj0 = (^ [X0:$i] : (d_ReplSep@X0@(^ [X1:$i] : (? [X2:$i] : ((d_Inj0@X2) = X1)))@d_Unj))), unknown).
thf(typ_proj1,type, (proj1 : ($i>$i)), unknown, [hashroot('1a479ee09a2e963dfc9bac8dacee86c5baacd167b593490c3fc335e4b32344ab')]).
thf(def_proj1,definition, (proj1 = (^ [X0:$i] : (d_ReplSep@X0@(^ [X1:$i] : (? [X2:$i] : ((d_Inj1@X2) = X1)))@d_Unj))), unknown).
thf(k_Inj0_pair_0_eq,axiom, (d_Inj0 = (pair@emptyset)), unknown, [hashroot('2a071720c055b5a45f55f9ac98f83246338e9be08d64a59de3fba95669d67551')]).
thf(k_Inj1_pair_1_eq,axiom, (d_Inj1 = (pair@(ordsucc@emptyset))), unknown, [hashroot('2602dde0fbc6f55eb461e6defa1413faf724d08a68a8be59933f3a7e798dc2de')]).
thf(pairI0,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@X0) => (in@(pair@emptyset@X2)@(pair@X0@X1)))), unknown, [hashroot('cd44038ac407160755ba2c4f496f02dac20ea4162128d6a322513a25d9976f19')]).
thf(pairI1,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@X1) => (in@(pair@(ordsucc@emptyset)@X2)@(pair@X0@X1)))), unknown, [hashroot('4dd85d17d326bf9c648692c82e66b08b58d1a2792f6e38884b21f242a246105b')]).
thf(pairE,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@(pair@X0@X1)) => ((? [X3:$i] : ((in@X3@X0) & (X2 = (pair@emptyset@X3)))) | (? [X3:$i] : ((in@X3@X1) & (X2 = (pair@(ordsucc@emptyset)@X3))))))), unknown, [hashroot('ccc09e9176b3f1081fc125449589528ce5749ba9687fdda706e16d3b60907d43')]).
thf(pairE0,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@(pair@emptyset@X2)@(pair@X0@X1)) => (in@X2@X0))), unknown, [hashroot('1d0d67f2d55210dc1bbb179bb73e9707d1daec023338f20e430d813c555e43dd')]).
thf(pairE1,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@(pair@(ordsucc@emptyset)@X2)@(pair@X0@X1)) => (in@X2@X1))), unknown, [hashroot('23bd3646bca373ca63bc2f36ccb80c9b8e15519d5adadef616dfabd7b9975a05')]).
thf(proj0I,axiom, (! [X0:$i] : ! [X1:$i] : ((in@(pair@emptyset@X1)@X0) => (in@X1@(proj0@X0)))), unknown, [hashroot('800a45e4d124866c54644aaa397ec9915eb43ddd2b35f8be760ec0807d2433e4')]).
thf(proj0E,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@(proj0@X0)) => (in@(pair@emptyset@X1)@X0))), unknown, [hashroot('4184e032de2277c215d5d508ab680c0f74bb842605a78bd36a588d1188e96e6b')]).
thf(proj1I,axiom, (! [X0:$i] : ! [X1:$i] : ((in@(pair@(ordsucc@emptyset)@X1)@X0) => (in@X1@(proj1@X0)))), unknown, [hashroot('e39f195f5193e1fe30ddf34eb7b551cfbe5562c8895d50c277b68d2c53d11e19')]).
thf(proj1E,axiom, (! [X0:$i] : ! [X1:$i] : ((in@X1@(proj1@X0)) => (in@(pair@(ordsucc@emptyset)@X1)@X0))), unknown, [hashroot('783d975179a6cb150684a071e74bfd0189016c9edecca4ef66ef7b6179c26cda')]).
thf(proj0_pair_eq,axiom, (! [X0:$i] : ! [X1:$i] : ((proj0@(pair@X0@X1)) = X0)), unknown, [hashroot('bb21e45c92b81165c0aabfa6e2e1d28696dc84dd4ad2ba866a7e3b2fc28c100a')]).
thf(proj1_pair_eq,axiom, (! [X0:$i] : ! [X1:$i] : ((proj1@(pair@X0@X1)) = X1)), unknown, [hashroot('86be316790581211fd63460af1998e6ade270cffc3597b4700c4877080075047')]).
thf(typ_d_Sigma,type, (d_Sigma : ($i>($i>$i)>$i)), unknown, [hashroot('75c40645d5fe236c1ca11856fa4f32b0ebb4c95c94a9615ec27513c1c1b6b5d3')]).
thf(def_d_Sigma,definition, (d_Sigma = (^ [X0:$i] : ^ [X1:($i>$i)] : (famunion@X0@(^ [X2:$i] : (repl@(X1@X2)@(pair@X2)))))), unknown).
thf(pair_Sigma,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@X0) => (! [X3:$i] : ((in@X3@(X1@X2)) => (in@(pair@X2@X3)@(d_Sigma@X0@X1)))))), unknown, [hashroot('9456744260913bf66f02f8c4a6da84ffe8b9656f6bfca7a34fae3c5168b58248')]).
thf(k_Sigma_eta_proj0_proj1,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(d_Sigma@X0@X1)) => ((((pair@(proj0@X2)@(proj1@X2)) = X2) & (in@(proj0@X2)@X0)) & (in@(proj1@X2)@(X1@(proj0@X2)))))), unknown, [hashroot('4dbbec499cd5d2f54b85a3e4fa0bea03c103f3f3d8e239dbfd5930f28f5e70d8')]).
thf(proj_Sigma_eta,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(d_Sigma@X0@X1)) => ((pair@(proj0@X2)@(proj1@X2)) = X2))), unknown, [hashroot('32cff0cfc8a0467929bf28e648edb3b3503f16f78a8811f1852b354e666f57eb')]).
thf(proj0_Sigma,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(d_Sigma@X0@X1)) => (in@(proj0@X2)@X0))), unknown, [hashroot('288307190728f059c1e88165e1c11998478f588387784e4a639238e2d488954b')]).
thf(proj1_Sigma,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(d_Sigma@X0@X1)) => (in@(proj1@X2)@(X1@(proj0@X2))))), unknown, [hashroot('20cab71ce7beb74057aba25061702373e792afdd262cc9237e3a8eb1a2bdfa50')]).
thf(pair_Sigma_E1,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ! [X3:$i] : ((in@(pair@X2@X3)@(d_Sigma@X0@X1)) => (in@X3@(X1@X2)))), unknown, [hashroot('fa9380157c269f97b53da62acee4256951c6b7066351b984a2b2dcc7e8567da3')]).
thf(k_Sigma_E,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(d_Sigma@X0@X1)) => (? [X3:$i] : ((in@X3@X0) & (? [X4:$i] : ((in@X4@(X1@X3)) & (X2 = (pair@X3@X4)))))))), unknown, [hashroot('a8879dd6d70fd712deadb569b438f32517c7c53e9ee43ab1d31798fa2ff62d48')]).
thf(typ_setprod,type, (setprod : ($i>$i>$i)), file('grundlagen.aut','l_e_pairtype'), [hashroot('fb4689ffc13020b5172bd520f8cc647a174def57d464d1e0179acedecc331d0a')]).
thf(def_setprod,definition, (setprod = (^ [X0:$i] : ^ [X1:$i] : (d_Sigma@X0@(^ [X2:$i] : X1)))), file('grundlagen.aut','l_e_pairtype')).
thf(typ_ap,type, (ap : ($i>$i>$i)), unknown, [hashroot('a7decc6ab7cd5672b0a906b215a444cd9aa45edcb0967cd0c9d40e207f8df623')]).
thf(def_ap,definition, (ap = (^ [X0:$i] : ^ [X1:$i] : (d_ReplSep@X0@(^ [X2:$i] : (? [X3:$i] : (X2 = (pair@X1@X3))))@proj1))), unknown).
thf(apI,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@(pair@X1@X2)@X0) => (in@X2@(ap@X0@X1)))), unknown, [hashroot('42c2a1209c1a96b41944f644b8053c38a7f9f726e27acfeb0ee779bafa93c44d')]).
thf(apE,axiom, (! [X0:$i] : ! [X1:$i] : ! [X2:$i] : ((in@X2@(ap@X0@X1)) => (in@(pair@X1@X2)@X0))), unknown, [hashroot('c70526e186c1c71c0412c587c27b140b230cc1f72f92ac4b3b73c2e66bdfeb5e')]).
thf(beta,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@X0) => ((ap@(d_Sigma@X0@X1)@X2) = (X1@X2)))), unknown, [hashroot('2ad5629b8f82190c8d16e96ea95dadf81dafc029ae9a3396d9cb2d171e6fce86')]).
thf(proj0_ap_0,axiom, (! [X0:$i] : ((proj0@X0) = (ap@X0@emptyset))), unknown, [hashroot('c3051c10d4dc748d1a90e82263834f8b8ab43f700b5da952bab0a7aac3fe1f64')]).
thf(proj1_ap_1,axiom, (! [X0:$i] : ((proj1@X0) = (ap@X0@(ordsucc@emptyset)))), unknown, [hashroot('55f40c82eeb4f1dcb61f6e647fe699318ade0b3a2e69c9020cf81645dcef4777')]).
thf(pair_ap_0,axiom, (! [X0:$i] : ! [X1:$i] : ((ap@(pair@X0@X1)@emptyset) = X0)), unknown, [hashroot('0665c724aaedf8156c4346039f1312f94e35a2d6ea88f41eed42537cdd10973c')]).
thf(pair_ap_1,axiom, (! [X0:$i] : ! [X1:$i] : ((ap@(pair@X0@X1)@(ordsucc@emptyset)) = X1)), unknown, [hashroot('b61d14994ef88f238bd27b1eda61496508f2753c1729f907239d661d9f3a2cca')]).
thf(ap0_Sigma,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(d_Sigma@X0@X1)) => (in@(ap@X2@emptyset)@X0))), unknown, [hashroot('42dca1996ba1d2215d485bcfa83b104760596990e669fadb7bfd19c5ce587d59')]).
thf(typ_pair_p,type, (pair_p : ($i>$o)), unknown, [hashroot('1f1f3c6d92c97b8d98e7594b0f862a56e6621a9d8a281ade775dd517b6841b1a')]).
thf(def_pair_p,definition, (pair_p = (^ [X0:$i] : ((pair@(ap@X0@emptyset)@(ap@X0@(ordsucc@emptyset))) = X0))), unknown).
thf(pair_p_I,axiom, (! [X0:$i] : ! [X1:$i] : (pair_p@(pair@X0@X1))), unknown, [hashroot('72c2c930e0ffb4cb90bdbf91146efd71eaa30aab3f7ea977f79780b5203d73b5')]).
thf(typ_d_Pi,type, (d_Pi : ($i>($i>$i)>$i)), unknown, [hashroot('68d9661a0d30c2de45df1cf7708fadc581b6b52cb2ba22f5f89c21527ade885f')]).
thf(def_d_Pi,definition, (d_Pi = (^ [X0:$i] : ^ [X1:($i>$i)] : (d_Sep@(power@(d_Sigma@X0@(^ [X2:$i] : (union@(X1@X2)))))@(^ [X2:$i] : ! [X3:$i] : ((in@X3@X0) => (in@(ap@X2@X3)@(X1@X3))))))), unknown).
thf(k_PiI,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((! [X3:$i] : ((in@X3@X2) => ((pair_p@X3) & (in@(ap@X3@emptyset)@X0)))) => ((! [X3:$i] : ((in@X3@X0) => (in@(ap@X2@X3)@(X1@X3)))) => (in@X2@(d_Pi@X0@X1))))), unknown, [hashroot('9555c2bc9358afe4ef28dfb7ecbbc8f9fbc465c5211ef6bae485051cb9a23e6c')]).
thf(k_PiE,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(d_Pi@X0@X1)) => ((! [X3:$i] : ((in@X3@X2) => ((pair_p@X3) & (in@(ap@X3@emptyset)@X0)))) & (! [X3:$i] : ((in@X3@X0) => (in@(ap@X2@X3)@(X1@X3))))))), unknown, [hashroot('371b538a135b778c0e51337900734eb88b2ec819f650dff27b79be4774e28ef0')]).
thf(lam_Pi,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X0) => (in@(X2@X3)@(X1@X3)))) => (in@(d_Sigma@X0@X2)@(d_Pi@X0@X1)))), unknown, [hashroot('0188856999da4593b9c9db87d02370f335d61bff2c902d2a1a4414a301b486fb')]).
thf(ap_Pi,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ! [X3:$i] : ((in@X2@(d_Pi@X0@X1)) => ((in@X3@X0) => (in@(ap@X2@X3)@(X1@X3))))), unknown, [hashroot('273853e08fc3f975cbaf03343d417034c249ee5074275a7321273698d3bc1b72')]).
thf(k_Pi_ext_Subq,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(d_Pi@X0@X1)) => (! [X3:$i] : ((in@X3@(d_Pi@X0@X1)) => ((! [X4:$i] : ((in@X4@X0) => (d_Subq@(ap@X2@X4)@(ap@X3@X4)))) => (d_Subq@X2@X3)))))), unknown, [hashroot('f0aedba55a8c8b72083f71583637288e496b5dfb98c79ee0c5f80701d6a8997b')]).
thf(k_Pi_ext,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@(d_Pi@X0@X1)) => (! [X3:$i] : ((in@X3@(d_Pi@X0@X1)) => ((! [X4:$i] : ((in@X4@X0) => ((ap@X2@X4) = (ap@X3@X4)))) => (X2 = X3)))))), unknown, [hashroot('94c7b9b9e394ffeecde33ca25e4fe653678cdff3b1407d76d9ccf4892b722a83')]).
thf(xi_ext_lem,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X0) => ((X1@X3) = (X2@X3)))) => (d_Subq@(d_Sigma@X0@X1)@(d_Sigma@X0@X2)))), unknown, [hashroot('75aaa0c2358ce5a74cde8b98b65d43802e197eed9efcf92aaa88ac2456bc19ba')]).
thf(xi_ext,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X0) => ((X1@X3) = (X2@X3)))) => ((d_Sigma@X0@X1) = (d_Sigma@X0@X2)))), unknown, [hashroot('d5311817ac737eb5582f5c21119cd4b078644b4ca3796c512ae0f0381b658c49')]).
thf(k_If_In_01,axiom, (! [X0:$o] : ! [X1:$i] : ! [X2:$i] : ((X0 => (in@X1@X2)) => (in@(if@X0@X1@emptyset)@(if@X0@X2@(ordsucc@emptyset))))), unknown, [hashroot('d06db07c9b7f2301d5f491611c26a0bd3849aae0ead312109b0841d926d5b61a')]).
thf(k_If_In_then_E,axiom, (! [X0:$o] : ! [X1:$i] : ! [X2:$i] : ! [X3:$i] : (X0 => ((in@X1@(if@X0@X2@X3)) => (in@X1@X2)))), unknown, [hashroot('70ed4ae9dc6df638e27145af2fab125789ade69fad16f48a174352033942617c')]).
thf(typ_imp,type, (imp : ($o>$o>$o)), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')], [hashroot('c471f7f9b55e8fce15c5b7f92a344808917c7bdfbac8043315ef5a52fb233e79')]).
thf(def_imp,definition, (imp = (^ [X0:$o] : ^ [X1:$o] : (X0 => X1))), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')]).
thf(l_mp,axiom, (! [X0:$o] : ! [X1:$o] : (X0 => ((imp@X0@X1) => X1))), [file('grundlagen.aut','l_mp'),file('grundlagen.aut','l_r_mp')], [hashroot('015cd300e0c67d86633e1f151d83470dfe10f6723378f7a60a6693da57c11a26')]).
thf(refimp,axiom, (! [X0:$o] : (imp@X0@X0)), file('grundlagen.aut','l_refimp'), [hashroot('775a1935f23b3a2e1e047bca85a163d7ea52e1abb45cda358eefb7e0d6937b4c')]).
thf(trimp,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((imp@X0@X1) => ((imp@X1@X2) => (imp@X0@X2)))), file('grundlagen.aut','l_trimp'), [hashroot('2bcdf25024654d831bf93230a21fbfc901901f0d6d7f74ae8e2439f370d5b724')]).
thf(typ_d_not,type, (d_not : ($o>$o)), file('grundlagen.aut','l_not'), [hashroot('122455a7450b0bbfcd7daa48af7a7a4bf320a8f085b235e0bfe56f0a1de2967f')]).
thf(def_d_not,definition, (d_not = (^ [X0:$o] : (imp@X0@$false))), file('grundlagen.aut','l_not')).
thf(typ_wel,type, (wel : ($o>$o)), file('grundlagen.aut','l_wel'), [hashroot('3c7461ae99cc5fcbcf4c6eb8756a1630dde3bca516b9da8a16686247350ecfaf')]).
thf(def_wel,definition, (wel = (^ [X0:$o] : (d_not@(d_not@X0)))), file('grundlagen.aut','l_wel')).
thf(l_weli,axiom, (! [X0:$o] : (X0 => (wel@X0))), file('grundlagen.aut','l_weli'), [hashroot('b001b5c8fe4dbeee8d0cd64f883b86da672179a6bf2c4b5029c6c7ef33fd8bb2')]).
thf(l_et,axiom, (! [X0:$o] : ((wel@X0) => X0)), file('grundlagen.aut','l_et'), [hashroot('68f4fbe1a4cdb31228d4806333cc9f14319ec737076e00d4ccc0adf1acc3752f')]).
thf(imp_th1,axiom, (! [X0:$o] : ! [X1:$o] : ((imp@X0@X1) => ((imp@(d_not@X0)@X1) => X1))), file('grundlagen.aut','l_imp_th1'), [hashroot('04c5c38519e75565eac7c32ba3629ba44eafb2f064d01aea618037e35a128b9c')]).
thf(imp_th2,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X0) => (imp@X0@X1))), [file('grundlagen.aut','l_imp_th2'),file('grundlagen.aut','l_r_imp_th2')], [hashroot('6b7c0508ed33535a3ba7736ad0756cd985f81f18d5798e5a280c8edab53ba228')]).
thf(imp_th3,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X1) => ((imp@X0@X1) => (d_not@X0)))), file('grundlagen.aut','l_imp_th3'), [hashroot('a757d28bd2958af930c1a84d0628d57d516363eea09fd8bf5745aa31fa10c6a6')]).
thf(imp_th4,axiom, (! [X0:$o] : ! [X1:$o] : (X0 => ((d_not@X1) => (d_not@(imp@X0@X1))))), file('grundlagen.aut','l_imp_th4'), [hashroot('eeb540663ff0c9037cf1d49502d0882feafc5e7e8c126c9d02bf3cdf78e8ebae')]).
thf(imp_th5,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@(imp@X0@X1)) => X0)), file('grundlagen.aut','l_imp_th5'), [hashroot('c9c2c5116120f96a8dfc0b6247e76a94816c9c78a549e300501b927574d16da7')]).
thf(imp_th6,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@(imp@X0@X1)) => (d_not@X1))), file('grundlagen.aut','l_imp_th6'), [hashroot('9f42c4f62f1c0ab4074aedcbcef390f5283a3e7a1de2f6f1b0a492e4885149e1')]).
thf(imp_th7,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X1) => ((imp@(d_not@X0)@X1) => X0))), file('grundlagen.aut','l_imp_th7'), [hashroot('5e33c4bd1705dbe279441b87c1ab6be5ee7995dc9e3116548bfd3b4398d2bcb3')]).
thf(l_cp,axiom, (! [X0:$o] : ! [X1:$o] : ((imp@(d_not@X1)@(d_not@X0)) => (imp@X0@X1))), file('grundlagen.aut','l_cp'), [hashroot('15a798e5ed2317efca644f1b1724cea99850aea22d062ab1de75d4fa8531789f')]).
thf(typ_obvious,type, (obvious : $o), [file('grundlagen.aut','l_obvious'),file('grundlagen.aut','l_obviousi')], [hashroot('6b854cf8c087c35ac85502245a320e8f8f12a120a0c6e55e0e19de84d4aaec57')]).
thf(def_obvious,definition, (obvious = (imp@$false@$false)), [file('grundlagen.aut','l_obvious'),file('grundlagen.aut','l_obviousi')]).
thf(obviousi,axiom, obvious, [file('grundlagen.aut','l_obvious'),file('grundlagen.aut','l_obviousi')], [hashroot('6b854cf8c087c35ac85502245a320e8f8f12a120a0c6e55e0e19de84d4aaec57')]).
thf(typ_l_ec,type, (l_ec : ($o>$o>$o)), file('grundlagen.aut','l_ec'), [hashroot('f505a8e15aa62b7fbc5d6fc63dc5c3c8e06aade6488051165390a10e2e57ad6e')]).
thf(def_l_ec,definition, (l_ec = (^ [X0:$o] : ^ [X1:$o] : (imp@X0@(d_not@X1)))), file('grundlagen.aut','l_ec')).
thf(l_eci1,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X0) => (l_ec@X0@X1))), file('grundlagen.aut','l_eci1'), [hashroot('d7d867a93e2334a9721a7f936d566f448ed0d5fcb800fc5d4014be7ce010ad22')]).
thf(l_eci2,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X1) => (l_ec@X0@X1))), file('grundlagen.aut','l_eci2'), [hashroot('5a0d1220fd991f6ffaea18d9267184f7e6b53b334e78014915c1aae291c785cc')]).
thf(ec_th1,axiom, (! [X0:$o] : ! [X1:$o] : ((imp@X0@(d_not@X1)) => (l_ec@X0@X1))), file('grundlagen.aut','l_ec_th1'), [hashroot('1026a82b75e955acd1b176a529f7c5765ffef8458348c982394dc8ea6ca99ddc')]).
thf(ec_th2,axiom, (! [X0:$o] : ! [X1:$o] : ((imp@X1@(d_not@X0)) => (l_ec@X0@X1))), file('grundlagen.aut','l_ec_th2'), [hashroot('89f52d0c1fa59fd36335e49c38cfdb59e7841f5a8d2de97d1ae2d8029ae7be33')]).
thf(comec,axiom, (! [X0:$o] : ! [X1:$o] : ((l_ec@X0@X1) => (l_ec@X1@X0))), file('grundlagen.aut','l_comec'), [hashroot('2cd59a12b245082647a9a6e02a6a15410fa01984a0ee00645ff09a038b943faf')]).
thf(l_ece1,axiom, (! [X0:$o] : ! [X1:$o] : ((l_ec@X0@X1) => (X0 => (d_not@X1)))), file('grundlagen.aut','l_ece1'), [hashroot('d55faa0275e16c6427748315c1188f52fdd4ceeee735a07fd644f20e0c7d208e')]).
thf(l_ece2,axiom, (! [X0:$o] : ! [X1:$o] : ((l_ec@X0@X1) => (X1 => (d_not@X0)))), file('grundlagen.aut','l_ece2'), [hashroot('3c98b4f0275508707ede3d4bc4fc67fe24c476f50ea11384ec41a1da9a8a59b3')]).
thf(ec_th3,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_ec@X0@X1) => ((imp@X2@X0) => (l_ec@X2@X1)))), file('grundlagen.aut','l_ec_th3'), [hashroot('3b9304ca1310e786cb2e4f848b43ebcd8152e9037a3045b2e14086072025917f')]).
thf(ec_th4,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_ec@X0@X1) => ((imp@X2@X1) => (l_ec@X0@X2)))), file('grundlagen.aut','l_ec_th4'), [hashroot('b5d8de79cd05cceee3c929be92f369385c5d76c1a36464e06975dcc17cd08028')]).
thf(typ_d_and,type, (d_and : ($o>$o>$o)), file('grundlagen.aut','l_and'), [hashroot('256969ce0d10af13735c022198c8269b31ef9b2f35171d9ad3e422d566773b0f')]).
thf(def_d_and,definition, (d_and = (^ [X0:$o] : ^ [X1:$o] : (d_not@(l_ec@X0@X1)))), file('grundlagen.aut','l_and')).
thf(l_andi,axiom, (! [X0:$o] : ! [X1:$o] : (X0 => (X1 => (d_and@X0@X1)))), file('grundlagen.aut','l_andi'), [hashroot('664c148b6723cba4899fdc5186099c4824e04239845d93d04a7a3b225c63b566')]).
thf(ande1,axiom, (! [X0:$o] : ! [X1:$o] : ((d_and@X0@X1) => X0)), file('grundlagen.aut','l_ande1'), [hashroot('f86ba2056f592be37e215b2a130c1992347b7f492877e7f483ad9548044cd845')]).
thf(ande2,axiom, (! [X0:$o] : ! [X1:$o] : ((d_and@X0@X1) => X1)), file('grundlagen.aut','l_ande2'), [hashroot('85dfb44fcd44ed7e3ff57be2fbe562bf99d9c4261fd04e49252f8ce02c97e059')]).
thf(comand,axiom, (! [X0:$o] : ! [X1:$o] : ((d_and@X0@X1) => (d_and@X1@X0))), file('grundlagen.aut','l_comand'), [hashroot('36fc0b294998e55b484c6e1f06eae80bbba1c1624516f9e26a657c9e3662a125')]).
thf(and_th1,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X0) => (d_not@(d_and@X0@X1)))), file('grundlagen.aut','l_and_th1'), [hashroot('a918ab7ceab3349d7dc34325c7974e45311a681932ecc3cdf55e52528c797f2e')]).
thf(and_th2,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X1) => (d_not@(d_and@X0@X1)))), file('grundlagen.aut','l_and_th2'), [hashroot('c7a8f75757ecac6a769dc5e1e25d47d6e621b86081f50904edb5d5a1f5470a23')]).
thf(and_th3,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@(d_and@X0@X1)) => (X0 => (d_not@X1)))), file('grundlagen.aut','l_and_th3'), [hashroot('1e2cb18d6b4f36dd184f77caffaf99082a0d13ec95fa2a9b65db8fa7c4f70801')]).
thf(and_th4,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@(d_and@X0@X1)) => (X1 => (d_not@X0)))), file('grundlagen.aut','l_and_th4'), [hashroot('574ad6bc4b06eaefb50cf7ee4ffee3d013197afd94388b37afd5bde89afc5a47')]).
thf(and_th5,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@(d_and@X0@X1)) => (d_not@(d_and@X1@X0)))), file('grundlagen.aut','l_and_th5'), [hashroot('bae5e4de69baaf62c2c400c41663a5bb5669b17ecc63351abce1095fcbf7205a')]).
thf(and_th6,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((d_and@X0@X1) => ((imp@X0@X2) => (d_and@X2@X1)))), file('grundlagen.aut','l_and_th6'), [hashroot('0a80382fbdeb0111323aa46c9c7f64fbf1b9a8035be55d50b7edf4d3e29d431c')]).
thf(and_th7,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((d_and@X0@X1) => ((imp@X1@X2) => (d_and@X0@X2)))), file('grundlagen.aut','l_and_th7'), [hashroot('76e094aa9102f5584aa983dc196fd0c6bd9681a36877ee094eae413bb03745d3')]).
thf(typ_l_or,type, (l_or : ($o>$o>$o)), file('grundlagen.aut','l_or'), [hashroot('d609be23d396e7ff7e091175cfb557c25e3ced2809a41de7e4d98fc9524ed072')]).
thf(def_l_or,definition, (l_or = (^ [X0:$o] : (imp@(d_not@X0)))), file('grundlagen.aut','l_or')).
thf(l_ori1,axiom, (! [X0:$o] : ! [X1:$o] : (X0 => (l_or@X0@X1))), file('grundlagen.aut','l_ori1'), [hashroot('6b8b863b0e3574bc8bb22753010061408eb4df3a25d4e19f943ab78174f64c22')]).
thf(l_ori2,axiom, (! [X0:$o] : ! [X1:$o] : (X1 => (l_or@X0@X1))), file('grundlagen.aut','l_ori2'), [hashroot('479175177555e08b48fc986b110ad04bdd77ea085c60a75e3b4758761379a3a2')]).
thf(or_th1,axiom, (! [X0:$o] : ! [X1:$o] : ((imp@(d_not@X0)@X1) => (l_or@X0@X1))), file('grundlagen.aut','l_or_th1'), [hashroot('a94de59f834f848a144e111b789c122e731e3e5d15007cefa80db463271c911e')]).
thf(or_th2,axiom, (! [X0:$o] : ! [X1:$o] : ((imp@(d_not@X1)@X0) => (l_or@X0@X1))), file('grundlagen.aut','l_or_th2'), [hashroot('00bbe478c4a15525400e05de479f9e267e156395ca583e1987a4e4daae353a88')]).
thf(l_ore2,axiom, (! [X0:$o] : ! [X1:$o] : ((l_or@X0@X1) => ((d_not@X0) => X1))), file('grundlagen.aut','l_ore2'), [hashroot('cb0d79755104a84a56028121b7439ea3e6b815b22fcd397c2352cef5d9369ff2')]).
thf(l_ore1,axiom, (! [X0:$o] : ! [X1:$o] : ((l_or@X0@X1) => ((d_not@X1) => X0))), file('grundlagen.aut','l_ore1'), [hashroot('233a4b6a9fd4d3b3b72574a85b582c75b1d1c16b49856a94eed86a1372c5f738')]).
thf(comor,axiom, (! [X0:$o] : ! [X1:$o] : ((l_or@X0@X1) => (l_or@X1@X0))), file('grundlagen.aut','l_comor'), [hashroot('7e5159b305af83f597fc696eaef97da49a99b627baffd2761104668affcc1c25')]).
thf(or_th3,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X0) => ((d_not@X1) => (d_not@(l_or@X0@X1))))), file('grundlagen.aut','l_or_th3'), [hashroot('87f2027bdab21470a45f950a6f124a3c805cd8255fbe5db221ead3206f363d99')]).
thf(or_th4,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@(l_or@X0@X1)) => (d_not@X0))), file('grundlagen.aut','l_or_th4'), [hashroot('0d15a87c6bbe5796fe71b1ecdce3041d0ba7da20f27f1b60275f795e7bed4841')]).
thf(or_th5,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@(l_or@X0@X1)) => (d_not@X1))), file('grundlagen.aut','l_or_th5'), [hashroot('e07fb613b3d29b0f4e5f9d7ee72624370b696b2d6a12b7cbbe083c79297f957a')]).
thf(or_th6,axiom, (! [X0:$o] : (l_or@X0@(d_not@X0))), file('grundlagen.aut','l_or_th6'), [hashroot('4cd275b5776f0067a5c756d86a75133f904f048b0cd9f6ef0ce15a802d8928cb')]).
thf(orapp,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_or@X0@X1) => ((imp@X0@X2) => ((imp@X1@X2) => X2)))), file('grundlagen.aut','l_orapp'), [hashroot('1a7d976d4bece141dcf5bcc0bc43d00cd48009ed9895ccd5af11bf2b7476ac6c')]).
thf(or_th7,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_or@X0@X1) => ((imp@X0@X2) => (l_or@X2@X1)))), file('grundlagen.aut','l_or_th7'), [hashroot('3da1fd8eb69028dbb988ed77441a8180721df1860e4a18cddd8640077d5749e7')]).
thf(or_th8,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_or@X0@X1) => ((imp@X1@X2) => (l_or@X0@X2)))), file('grundlagen.aut','l_or_th8'), [hashroot('435ccc7682209150a12a622bb0993a16d24f1072061bef4b2151bbe20b299509')]).
thf(or_th9,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ! [X3:$o] : ((l_or@X0@X1) => ((imp@X0@X2) => ((imp@X1@X3) => (l_or@X2@X3))))), file('grundlagen.aut','l_or_th9'), [hashroot('7cc55870facd67ce42a356e2f4395b3904255b9c940cce9ea522c4d17cc5002f')]).
thf(or_th10,axiom, (! [X0:$o] : ! [X1:$o] : ((l_or@X0@X1) => (imp@(d_not@X0)@X1))), file('grundlagen.aut','l_or_th10'), [hashroot('dd98749029921a27303baea86b862c30449cf8f0108aa1e9e5ce1d77e550a6ef')]).
thf(or_th11,axiom, (! [X0:$o] : ! [X1:$o] : ((l_or@X0@X1) => (imp@(d_not@X1)@X0))), file('grundlagen.aut','l_or_th11'), [hashroot('095027c7f58b23ab1014331d08447bfe4b9a8bfe77bbe973af62aa231ac000f7')]).
thf(or_th12,axiom, (! [X0:$o] : ! [X1:$o] : ((l_or@(d_not@X0)@X1) => (imp@X0@X1))), file('grundlagen.aut','l_or_th12'), [hashroot('5b2747602f6849023920471c127f4bf5f2d16ea113c581d994212a55b2149be7')]).
thf(or_th13,axiom, (! [X0:$o] : ! [X1:$o] : ((imp@X0@X1) => (l_or@(d_not@X0)@X1))), file('grundlagen.aut','l_or_th13'), [hashroot('76d69c0719064daead6f4ebef53182ed7378b238a9f30240ce1f8380c2f36ef9')]).
thf(or_th14,axiom, (! [X0:$o] : ! [X1:$o] : ((l_or@(d_not@X0)@(d_not@X1)) => (d_not@(d_and@X0@X1)))), file('grundlagen.aut','l_or_th14'), [hashroot('f8b01ce39a3646356888fabef121b0220a17edef77891dd9a369848fb650c27f')]).
thf(or_th15,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@(d_and@X0@X1)) => (l_or@(d_not@X0)@(d_not@X1)))), file('grundlagen.aut','l_or_th15'), [hashroot('5560f94c0551ac51300908d32511fd3c16053d1b3e2f5effeff681da490fd060')]).
thf(or_th16,axiom, (! [X0:$o] : ! [X1:$o] : ((d_and@(d_not@X0)@(d_not@X1)) => (d_not@(l_or@X0@X1)))), file('grundlagen.aut','l_or_th16'), [hashroot('c45ed94dafeb1ee4188f7e2ec94f4cb24a6f31e886f0bc5ef2a74c3ec2963ec3')]).
thf(or_th17,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@(l_or@X0@X1)) => (d_and@(d_not@X0)@(d_not@X1)))), file('grundlagen.aut','l_or_th17'), [hashroot('a81db4eefc80531562bc2e7bd24728f4ed700c9e580114dc40bcc087e9e27d50')]).
thf(typ_orec,type, (orec : ($o>$o>$o)), file('grundlagen.aut','l_orec'), [hashroot('932af7a349464e5b2b9820e91486e866add9bee9198b2e4adf3e75924f128d54')]).
thf(def_orec,definition, (orec = (^ [X0:$o] : ^ [X1:$o] : (d_and@(l_or@X0@X1)@(l_ec@X0@X1)))), file('grundlagen.aut','l_orec')).
thf(oreci,axiom, (! [X0:$o] : ! [X1:$o] : ((l_or@X0@X1) => ((l_ec@X0@X1) => (orec@X0@X1)))), file('grundlagen.aut','l_oreci'), [hashroot('cfd60a6e21136ec2333d0bba73cd1f47fc30eefdaf63ed8cf7d6adc57c97bcd7')]).
thf(orec_th1,axiom, (! [X0:$o] : ! [X1:$o] : (X0 => ((d_not@X1) => (orec@X0@X1)))), file('grundlagen.aut','l_orec_th1'), [hashroot('f34b295c298ad595d9108541f51f7fa78881db87442836f1497d3d0b6b8ae3d7')]).
thf(orec_th2,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X0) => (X1 => (orec@X0@X1)))), file('grundlagen.aut','l_orec_th2'), [hashroot('699a9d9685b3974206402ecd642c4baebc1a17aca4a813aaaaf62cd2e842ddff')]).
thf(orece1,axiom, (! [X0:$o] : ! [X1:$o] : ((orec@X0@X1) => (l_or@X0@X1))), file('grundlagen.aut','l_orece1'), [hashroot('6f7be6587d22d9953651bcfc9bcb25eb260ce20ea348143a501b3df4380d9a9f')]).
thf(orece2,axiom, (! [X0:$o] : ! [X1:$o] : ((orec@X0@X1) => (l_ec@X0@X1))), file('grundlagen.aut','l_orece2'), [hashroot('5db9f0825672ce5f574c366668d5718c8beb9fa532393edc71c29274c4767b8c')]).
thf(comorec,axiom, (! [X0:$o] : ! [X1:$o] : ((orec@X0@X1) => (orec@X1@X0))), file('grundlagen.aut','l_comorec'), [hashroot('2d1b80f045ea2530133087ba26c636d91fd3acfacccc9a93c52d7685b7d54bfa')]).
thf(orec_th3,axiom, (! [X0:$o] : ! [X1:$o] : ((orec@X0@X1) => (X0 => (d_not@X1)))), file('grundlagen.aut','l_orec_th3'), [hashroot('6b053246e6db1b010c670949894243fa2b18e2690d22d43caed33868504a16b2')]).
thf(orec_th4,axiom, (! [X0:$o] : ! [X1:$o] : ((orec@X0@X1) => (X1 => (d_not@X0)))), file('grundlagen.aut','l_orec_th4'), [hashroot('91dd26de33ea847e6da332dd677d67766a12bd5319a0badf7e6d1126074d0802')]).
thf(orec_th5,axiom, (! [X0:$o] : ! [X1:$o] : ((orec@X0@X1) => ((d_not@X0) => X1))), file('grundlagen.aut','l_orec_th5'), [hashroot('d9d7d6c9e33816c1f518656b58d891b640640cc532d6d7967fc85a3e0e92e968')]).
thf(orec_th6,axiom, (! [X0:$o] : ! [X1:$o] : ((orec@X0@X1) => ((d_not@X1) => X0))), file('grundlagen.aut','l_orec_th6'), [hashroot('539baa90928de83ce0c4a4cdf44b66efe7e81e3abd8467b59bb8dae4a9d90f53')]).
thf(typ_l_iff,type, (l_iff : ($o>$o>$o)), file('grundlagen.aut','l_iff'), [hashroot('653c4e8d31479c5824e2b88fcbc30fca82aa02ddf5748d0a43e57f090a7c5d83')]).
thf(def_l_iff,definition, (l_iff = (^ [X0:$o] : ^ [X1:$o] : (d_and@(imp@X0@X1)@(imp@X1@X0)))), file('grundlagen.aut','l_iff')).
thf(l_iffi,axiom, (! [X0:$o] : ! [X1:$o] : ((imp@X0@X1) => ((imp@X1@X0) => (l_iff@X0@X1)))), file('grundlagen.aut','l_iffi'), [hashroot('0df5c1f299e8e9ed77217f2fa9bd4b68fdc182f4b34c25f1a2f76bdabaae674b')]).
thf(iff_th1,axiom, (! [X0:$o] : ! [X1:$o] : (X0 => (X1 => (l_iff@X0@X1)))), file('grundlagen.aut','l_iff_th1'), [hashroot('170bc1e43d6c1b0908946befe391c71f10a6b6d937cf061c710475055d186a03')]).
thf(iff_th2,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X0) => ((d_not@X1) => (l_iff@X0@X1)))), file('grundlagen.aut','l_iff_th2'), [hashroot('085bceb2c8ca64b543f2ed94f0d6f89a00ff00df28c6d158f6f9519e0ebf4ab7')]).
thf(iffe1,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X0@X1) => (imp@X0@X1))), file('grundlagen.aut','l_iffe1'), [hashroot('fad179671a94f24869ea4dda0de311ff60cfa04a943e4453ca729804ec08a77b')]).
thf(iffe2,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X0@X1) => (imp@X1@X0))), file('grundlagen.aut','l_iffe2'), [hashroot('c04974db3756316daf19a5520301f9137a6daef25115171085ce53ff9d15f3f0')]).
thf(comiff,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X0@X1) => (l_iff@X1@X0))), [file('grundlagen.aut','l_comiff'),file('grundlagen.aut','l_symiff')], [hashroot('d83d81dec2400cbf429db238fb2ef0b9c677a09e6bac2f6e7af330bbb6f79651')]).
thf(iff_th3,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X0@X1) => (X0 => X1))), file('grundlagen.aut','l_iff_th3'), [hashroot('78c990b15135b7bd18d0638932703118618cacdb09791fecafc874f5f9afc1a4')]).
thf(iff_th4,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X0@X1) => (X1 => X0))), file('grundlagen.aut','l_iff_th4'), [hashroot('3cddcda634b04a2294b4d571e36b48ef15e3d8d088767c887beada7217b1e475')]).
thf(iff_th5,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X0@X1) => ((d_not@X0) => (d_not@X1)))), file('grundlagen.aut','l_iff_th5'), [hashroot('de279d09dbb9f35832c51c4f7818e8e8482be3cb5f9e6f4176ba88cc3cc6d739')]).
thf(iff_th6,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X0@X1) => ((d_not@X1) => (d_not@X0)))), file('grundlagen.aut','l_iff_th6'), [hashroot('8a671c8a48c959d2e8980224a548a6c5fb9effd9c98da0a1c2ae1bc0edbdb45a')]).
thf(iff_th7,axiom, (! [X0:$o] : ! [X1:$o] : (X0 => ((d_not@X1) => (d_not@(l_iff@X0@X1))))), file('grundlagen.aut','l_iff_th7'), [hashroot('b2b367203d8a958de4e0d31167b48064cb92992470f165478471bf570b100904')]).
thf(iff_th8,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X0) => (X1 => (d_not@(l_iff@X0@X1))))), file('grundlagen.aut','l_iff_th8'), [hashroot('53ac0eafb714d9d9dfa5080507aebf08fa938d5295499e4c53862217179b4789')]).
thf(refiff,axiom, (! [X0:$o] : (l_iff@X0@X0)), file('grundlagen.aut','l_refiff'), [hashroot('51222e0242cddefab6d6d6d9d7ea764ee0c0e7ca4bbb8bf8d2f916a07463cdff')]).
thf(triff,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_iff@X0@X1) => ((l_iff@X1@X2) => (l_iff@X0@X2)))), file('grundlagen.aut','l_triff'), [hashroot('05d532de562e613e94d890a616a8cf9e375f386b9167e5c89d045e2223211301')]).
thf(iff_th9,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X0@X1) => (imp@(d_not@X0)@(d_not@X1)))), file('grundlagen.aut','l_iff_th9'), [hashroot('59626a3ba92dbf8081e98f699a1f15871a40e2ecc41d7b6110d9da107a2cbfc0')]).
thf(iff_th10,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X0@X1) => (imp@(d_not@X1)@(d_not@X0)))), file('grundlagen.aut','l_iff_th10'), [hashroot('515f5d96e38246e5a3f614ee2fea6de6a20a2bc8c8bab14ca0cedb1e57ab9cd4')]).
thf(iff_th11,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X0@X1) => (l_iff@(d_not@X0)@(d_not@X1)))), file('grundlagen.aut','l_iff_th11'), [hashroot('1e0000c54b3c2046975c4294c158e50f38ba8cc1b939d26e32e8388930885e01')]).
thf(iff_th12,axiom, (! [X0:$o] : ! [X1:$o] : ((imp@(d_not@X0)@(d_not@X1)) => ((imp@(d_not@X1)@(d_not@X0)) => (l_iff@X0@X1)))), file('grundlagen.aut','l_iff_th12'), [hashroot('d4be9842005c6389b7d3a65c2f600f3fcdfda5bc1067dfd4a3b95ca9397cc64f')]).
thf(iff_th13,axiom, (! [X0:$o] : ! [X1:$o] : ((orec@X0@X1) => (l_iff@X0@(d_not@X1)))), file('grundlagen.aut','l_iff_th13'), [hashroot('8e4b0b85c4a7150b4b15798abcd08c553f8d26e629074da79e42828c7641be6b')]).
thf(iff_th14,axiom, (! [X0:$o] : ! [X1:$o] : ((orec@X0@X1) => (l_iff@X1@(d_not@X0)))), file('grundlagen.aut','l_iff_th14'), [hashroot('a28b5357acf34fb244e1cf0c4069e7f3b6406ef92b64b97753fc7f14bb80c3a6')]).
thf(iff_th15,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X0@(d_not@X1)) => (orec@X0@X1))), file('grundlagen.aut','l_iff_th15'), [hashroot('1d484e2a07d4cf05dc4ca4dfa75edaf6814321dc160c5c92e35ef0ea0576ac3f')]).
thf(iff_th16,axiom, (! [X0:$o] : ! [X1:$o] : ((l_iff@X1@(d_not@X0)) => (orec@X0@X1))), file('grundlagen.aut','l_iff_th16'), [hashroot('39e5348f515d012fa02726f0ab518491af833a2d65f1c9645b65ba9ebc4acd31')]).
thf(thimp1,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_iff@X0@X1) => ((imp@X0@X2) => (imp@X1@X2)))), file('grundlagen.aut','l_iff_thimp1'), [hashroot('926f5ebb543da33c743d33c826260d93b72973e9cdf7b0249467755d7c11cb78')]).
thf(thimp2,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_iff@X0@X1) => ((imp@X2@X0) => (imp@X2@X1)))), file('grundlagen.aut','l_iff_thimp2'), [hashroot('19c038b98167621c1999e2b80fc1b428d6d470cfe824d7fe09bc7e1816a2893b')]).
thf(thec1,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_iff@X0@X1) => ((l_ec@X0@X2) => (l_ec@X1@X2)))), file('grundlagen.aut','l_iff_thec1'), [hashroot('ab7452ed152c3f91ca3059f7d0a28150f28f809526bc4f6bfafdd5229cab4308')]).
thf(thec2,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_iff@X0@X1) => ((l_ec@X2@X0) => (l_ec@X2@X1)))), file('grundlagen.aut','l_iff_thec2'), [hashroot('4e753d2f630df7291aaa2a2c16b3c3fb7f6da38900693229516e4e1b77ee0cc4')]).
thf(thand1,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_iff@X0@X1) => ((d_and@X0@X2) => (d_and@X1@X2)))), file('grundlagen.aut','l_iff_thand1'), [hashroot('518ab6e5f4f889a1e825b5b531f5e9ec43b17f59660169731c555caa1313feeb')]).
thf(thand2,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_iff@X0@X1) => ((d_and@X2@X0) => (d_and@X2@X1)))), file('grundlagen.aut','l_iff_thand2'), [hashroot('7e1a10ac6c6e53f68ae6ccea4ca84b7c0dc20c1ef1df01ef12f7ba75891483d4')]).
thf(thor1,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_iff@X0@X1) => ((l_or@X0@X2) => (l_or@X1@X2)))), file('grundlagen.aut','l_iff_thor1'), [hashroot('b2d91e4854b2422136c981bc1187518e1024753de9dc2771e99e8cbadad67081')]).
thf(thor2,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_iff@X0@X1) => ((l_or@X2@X0) => (l_or@X2@X1)))), file('grundlagen.aut','l_iff_thor2'), [hashroot('65c7c7a257b0d02905e7d2bbd0deed9f2ebec21e2affd4e19272c40b5ab258f9')]).
thf(thorec1,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_iff@X0@X1) => ((orec@X0@X2) => (orec@X1@X2)))), file('grundlagen.aut','l_iff_thorec1'), [hashroot('1274792465e7c65809aefae19dc1c0ecd825a3e8e201976a710ce30994a4a20d')]).
thf(thorec2,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_iff@X0@X1) => ((orec@X2@X0) => (orec@X2@X1)))), file('grundlagen.aut','l_iff_thorec2'), [hashroot('7820214c48049c344a66cd74f2bd92ed3e52c910e064d685f47048c31aebab91')]).
thf(typ_all,type, (all : ($i>($i>$o)>$o)), file('grundlagen.aut','l_all'), [hashroot('7af775405bb712aec9ce7cdde103343acfd567932781aa9cedcee8d57f9a249d')]).
thf(def_all,definition, (all = (^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@X0))))), file('grundlagen.aut','l_all')).
thf(l_alle,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((all@X0@X1) => (all_of@(^ [X2:$i] : (in@X2@X0))@X1))), file('grundlagen.aut','l_alle'), [hashroot('2f5c323be4b17cfc7b357451c72ef8d24da7362bf60ed22eb39a20c8ccb5b1db')]).
thf(all_th1,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : ((d_not@(X1@X2)) => (d_not@(all@X0@X1)))))), file('grundlagen.aut','l_all_th1'), [hashroot('0d1b137a287fc2620cc09034fc71b5e51fc1f385ad46e15ec7dc4da2bec114f1')]).
thf(typ_non,type, (non : ($i>($i>$o)>$i>$o)), file('grundlagen.aut','l_non'), [hashroot('e0c99b0aabd21f260538f4a9cc7b2f9dd1c1be40c3fd3c26d5355897e80de94b')]).
thf(def_non,definition, (non = (^ [X0:$i] : ^ [X1:($i>$o)] : ^ [X2:$i] : (d_not@(X1@X2)))), file('grundlagen.aut','l_non')).
thf(typ_l_some,type, (l_some : ($i>($i>$o)>$o)), file('grundlagen.aut','l_some'), [hashroot('e725afc4936069e5e8c753118fc6844ba6a33e0b7ed57300c90a0659591806c9')]).
thf(def_l_some,definition, (l_some = (^ [X0:$i] : ^ [X1:($i>$o)] : (d_not@(all_of@(^ [X2:$i] : (in@X2@X0))@(non@X0@X1))))), file('grundlagen.aut','l_some')).
thf(somei,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : ((X1@X2) => (l_some@X0@X1))))), file('grundlagen.aut','l_somei'), [hashroot('b362a912704aa7c3b18c4f800f5c6d75f45a2588ee1857023b39ab6b78747004')]).
thf(some_t1,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((all_of@(^ [X2:$i] : (in@X2@X0))@(non@X0@(non@X0@X1))) => (all_of@(^ [X2:$i] : (in@X2@X0))@X1))), file('grundlagen.aut','l_some_t1'), [hashroot('4817d99153019fbf4b287f106f2b1ce121d638c741b1be01a296018adcad1cf5')]).
thf(some_t2,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((d_not@(all@X0@X1)) => ((all_of@(^ [X2:$i] : (in@X2@X0))@(non@X0@(non@X0@X1))) => $false))), file('grundlagen.aut','l_some_t2'), [hashroot('3f41809add45b1590c1508878b05205b2694749b34d8d109f82f6e479ade2289')]).
thf(some_th1,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((d_not@(all@X0@X1)) => (l_some@X0@(non@X0@X1)))), file('grundlagen.aut','l_some_th1'), [hashroot('69d6b4cc334beaec739d5474e946120e0ae7506e33dc246f976ff355eb35c48c')]).
thf(some_t3,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((all@X0@X1) => (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (d_not@(d_not@(X1@X2))))))), file('grundlagen.aut','l_some_t3'), [hashroot('71e0451381c9f84198e4f608ded1809ca0d319fbd25a5a7661f5d3660b9ca8f8')]).
thf(some_t4,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((l_some@X0@(non@X0@X1)) => ((all@X0@X1) => $false))), file('grundlagen.aut','l_some_t4'), [hashroot('b21fdf04c20b1cc0db5f00a02d7cefcee22e00d44400880df3b42feb2380e9bf')]).
thf(some_th2,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((l_some@X0@(non@X0@X1)) => (d_not@(all@X0@X1)))), file('grundlagen.aut','l_some_th2'), [hashroot('90db77f542c230ef8c64a2b17daaa469d7332e86a19b3729a19e26ae08f7ac5c')]).
thf(some_th3,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((d_not@(l_some@X0@X1)) => (all_of@(^ [X2:$i] : (in@X2@X0))@(non@X0@X1)))), file('grundlagen.aut','l_some_th3'), [hashroot('de7b6ab76752a3cb00bb93226ec21c61d7c8508357cbc9a992c1d72b901efd58')]).
thf(some_th4,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((d_not@(l_some@X0@X1)) => (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (d_not@(X1@X2)))))), file('grundlagen.aut','l_some_th4'), [hashroot('7edf6699b7dca75cd7812a65a7922e7026537d073513bd3b9acb706724bc1f9a')]).
thf(some_th5,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((all_of@(^ [X2:$i] : (in@X2@X0))@(non@X0@X1)) => (d_not@(l_some@X0@X1)))), file('grundlagen.aut','l_some_th5'), [hashroot('efe45dc46023ce017b5dfa115e876349195920f43329e85007ddbcd4c5a3a619')]).
thf(some_t5,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ! [X2:$o] : ((all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : (imp@(X1@X3)@X2))) => ((d_not@X2) => (all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : (d_not@(X1@X3))))))), file('grundlagen.aut','l_some_t5'), [hashroot('8cbfba53e78124effe63c6b04de9abd213f6141c4f59ec950d77bd9c6fa8c334')]).
thf(some_t6,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((l_some@X0@X1) => (! [X2:$o] : ((all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : (imp@(X1@X3)@X2))) => ((d_not@X2) => $false))))), file('grundlagen.aut','l_some_t6'), [hashroot('8f558edc16a8eca7e840c993960c6e838c7e71a26442c2a4aa34feefb68eece6')]).
thf(someapp,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((l_some@X0@X1) => (! [X2:$o] : ((all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : (imp@(X1@X3)@X2))) => X2)))), file('grundlagen.aut','l_someapp'), [hashroot('d4d5f414178883980c8b8bc2074a4d3d039316e9f9d7903e7ec83c226af3539c')]).
thf(some_th6,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ! [X2:($i>$o)] : ((l_some@X0@X1) => ((all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : (imp@(X1@X3)@(X2@X3)))) => (l_some@X0@X2)))), file('grundlagen.aut','l_some_th6'), [hashroot('3fbe11d435bbedf81b5e87ccabb81925feea03a9d2efe27610f44d29c4aae151')]).
thf(typ_or3,type, (or3 : ($o>$o>$o>$o)), file('grundlagen.aut','l_or3'), [hashroot('418f21bcf146bb24b5eab2753c1a8363be2fc645e151822c7c92c194580b7f14')]).
thf(def_or3,definition, (or3 = (^ [X0:$o] : ^ [X1:$o] : ^ [X2:$o] : (l_or@X0@(l_or@X1@X2)))), file('grundlagen.aut','l_or3')).
thf(or3_th1,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((or3@X0@X1@X2) => ((d_not@X0) => (l_or@X1@X2)))), file('grundlagen.aut','l_or3_th1'), [hashroot('54577822fd3be929ac57d2c2b151c538ad30d6558f78152de605fe6876dd714f')]).
thf(or3e3,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((or3@X0@X1@X2) => ((d_not@X0) => ((d_not@X1) => X2)))), file('grundlagen.aut','l_or3e3'), [hashroot('3d15a755355b331499031374fdfff7cda1f0c6f6db5530109db8518f6c57944d')]).
thf(or3_th2,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((or3@X0@X1@X2) => ((d_not@X1) => (l_or@X2@X0)))), file('grundlagen.aut','l_or3_th2'), [hashroot('911948ceeea0760666ea35d8f72ac63034e1b35f8482a7bba4a9b9ada134bddc')]).
thf(or3e1,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((or3@X0@X1@X2) => ((d_not@X1) => ((d_not@X2) => X0)))), file('grundlagen.aut','l_or3e1'), [hashroot('638bb66aee4a2fd4f89a55cdd5698c06c6b28fe1749c4038046db8fb7c2e5289')]).
thf(or3_th3,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((or3@X0@X1@X2) => ((d_not@X2) => (l_or@X0@X1)))), file('grundlagen.aut','l_or3_th3'), [hashroot('213789b88f51f04eb03beafd5b428a19d6ca30c6661a345cd591c749c4332fa3')]).
thf(or3e2,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((or3@X0@X1@X2) => ((d_not@X2) => ((d_not@X0) => X1)))), file('grundlagen.aut','l_or3e2'), [hashroot('227fea58250f49debe28d5968a1d2930d946bce834a0775875a10cbdf4bc4444')]).
thf(or3_th4,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((or3@X0@X1@X2) => (or3@X1@X2@X0))), file('grundlagen.aut','l_or3_th4'), [hashroot('e4b1e41119638ea393c1064f6a5910d8514439c9898caaa91c87ecf3e9cf090b')]).
thf(or3_th5,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((or3@X0@X1@X2) => (or3@X2@X0@X1))), file('grundlagen.aut','l_or3_th5'), [hashroot('fad9321388dbaaab82503fbcc1a6af01c148281cdaea5868b3a3959ff0e0e547')]).
thf(or3i1,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : (X0 => (or3@X0@X1@X2))), file('grundlagen.aut','l_or3i1'), [hashroot('4b20677711138dfc38257343e7e99cddc25392f306cd8329a239ece27d51fbbd')]).
thf(or3i2,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : (X1 => (or3@X0@X1@X2))), file('grundlagen.aut','l_or3i2'), [hashroot('37b391001bdf0aca7d6cac13352f5d56a34c87b9424cba7784a942c69aa6ad69')]).
thf(or3i3,axiom, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : (X2 => (or3@X0@X1@X2))), file('grundlagen.aut','l_or3i3'), [hashroot('d1fbf8f9ff332d1f4d4da353820559b4d365429b8e9d8458fc14418029feab24')]).
thf(or3_th6,conjecture, (! [X0:$o] : ! [X1:$o] : ! [X2:$o] : ((l_or@X0@X1) => (or3@X0@X1@X2))), file('grundlagen.aut','l_or3_th6'), [hashroot('ddc0084251fe838f1c9b52d968902e8fb7d1845e7b8561445f1b603b00bef701')]).
