thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_power,type, (power : ($i>$i)), file('grundlagen.aut','l_e_st_set'), [hashroot('0c5490ca2f6d61c2d410e7907be97b3bc36b3e4de614e1f5431278dbccad4c79')]).
thf(typ_d_Sep,type, (d_Sep : ($i>($i>$o)>$i)), [file('grundlagen.aut','l_e_ot'),file('grundlagen.aut','l_e_st_setof')], [hashroot('218a016b04c605cddd8baabd01b9102f19cbd21e943087fb26bfbb5c1990d944')]).
thf(typ_d_Sigma,type, (d_Sigma : ($i>($i>$i)>$i)), unknown, [hashroot('75c40645d5fe236c1ca11856fa4f32b0ebb4c95c94a9615ec27513c1c1b6b5d3')]).
thf(typ_ap,type, (ap : ($i>$i>$i)), unknown, [hashroot('a7decc6ab7cd5672b0a906b215a444cd9aa45edcb0967cd0c9d40e207f8df623')]).
thf(beta,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@X0) => ((ap@(d_Sigma@X0@X1)@X2) = (X1@X2)))), unknown, [hashroot('2ad5629b8f82190c8d16e96ea95dadf81dafc029ae9a3396d9cb2d171e6fce86')]).
thf(typ_d_Pi,type, (d_Pi : ($i>($i>$i)>$i)), unknown, [hashroot('68d9661a0d30c2de45df1cf7708fadc581b6b52cb2ba22f5f89c21527ade885f')]).
thf(lam_Pi,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X0) => (in@(X2@X3)@(X1@X3)))) => (in@(d_Sigma@X0@X2)@(d_Pi@X0@X1)))), unknown, [hashroot('0188856999da4593b9c9db87d02370f335d61bff2c902d2a1a4414a301b486fb')]).
thf(ap_Pi,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ! [X3:$i] : ((in@X2@(d_Pi@X0@X1)) => ((in@X3@X0) => (in@(ap@X2@X3)@(X1@X3))))), unknown, [hashroot('273853e08fc3f975cbaf03343d417034c249ee5074275a7321273698d3bc1b72')]).
thf(typ_imp,type, (imp : ($o>$o>$o)), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')], [hashroot('c471f7f9b55e8fce15c5b7f92a344808917c7bdfbac8043315ef5a52fb233e79')]).
thf(def_imp,definition, (imp = (^ [X0:$o] : ^ [X1:$o] : (X0 => X1))), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')]).
thf(typ_d_not,type, (d_not : ($o>$o)), file('grundlagen.aut','l_not'), [hashroot('122455a7450b0bbfcd7daa48af7a7a4bf320a8f085b235e0bfe56f0a1de2967f')]).
thf(def_d_not,definition, (d_not = (^ [X0:$o] : (imp@X0@$false))), file('grundlagen.aut','l_not')).
thf(typ_wel,type, (wel : ($o>$o)), file('grundlagen.aut','l_wel'), [hashroot('3c7461ae99cc5fcbcf4c6eb8756a1630dde3bca516b9da8a16686247350ecfaf')]).
thf(def_wel,definition, (wel = (^ [X0:$o] : (d_not@(d_not@X0)))), file('grundlagen.aut','l_wel')).
thf(l_et,axiom, (! [X0:$o] : ((wel@X0) => X0)), file('grundlagen.aut','l_et'), [hashroot('68f4fbe1a4cdb31228d4806333cc9f14319ec737076e00d4ccc0adf1acc3752f')]).
thf(typ_l_ec,type, (l_ec : ($o>$o>$o)), file('grundlagen.aut','l_ec'), [hashroot('f505a8e15aa62b7fbc5d6fc63dc5c3c8e06aade6488051165390a10e2e57ad6e')]).
thf(def_l_ec,definition, (l_ec = (^ [X0:$o] : ^ [X1:$o] : (imp@X0@(d_not@X1)))), file('grundlagen.aut','l_ec')).
thf(typ_d_and,type, (d_and : ($o>$o>$o)), file('grundlagen.aut','l_and'), [hashroot('256969ce0d10af13735c022198c8269b31ef9b2f35171d9ad3e422d566773b0f')]).
thf(def_d_and,definition, (d_and = (^ [X0:$o] : ^ [X1:$o] : (d_not@(l_ec@X0@X1)))), file('grundlagen.aut','l_and')).
thf(typ_l_or,type, (l_or : ($o>$o>$o)), file('grundlagen.aut','l_or'), [hashroot('d609be23d396e7ff7e091175cfb557c25e3ced2809a41de7e4d98fc9524ed072')]).
thf(def_l_or,definition, (l_or = (^ [X0:$o] : (imp@(d_not@X0)))), file('grundlagen.aut','l_or')).
thf(typ_all,type, (all : ($i>($i>$o)>$o)), file('grundlagen.aut','l_all'), [hashroot('7af775405bb712aec9ce7cdde103343acfd567932781aa9cedcee8d57f9a249d')]).
thf(def_all,definition, (all = (^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@X0))))), file('grundlagen.aut','l_all')).
thf(typ_non,type, (non : ($i>($i>$o)>$i>$o)), file('grundlagen.aut','l_non'), [hashroot('e0c99b0aabd21f260538f4a9cc7b2f9dd1c1be40c3fd3c26d5355897e80de94b')]).
thf(def_non,definition, (non = (^ [X0:$i] : ^ [X1:($i>$o)] : ^ [X2:$i] : (d_not@(X1@X2)))), file('grundlagen.aut','l_non')).
thf(typ_l_some,type, (l_some : ($i>($i>$o)>$o)), file('grundlagen.aut','l_some'), [hashroot('e725afc4936069e5e8c753118fc6844ba6a33e0b7ed57300c90a0659591806c9')]).
thf(def_l_some,definition, (l_some = (^ [X0:$i] : ^ [X1:($i>$o)] : (d_not@(all_of@(^ [X2:$i] : (in@X2@X0))@(non@X0@X1))))), file('grundlagen.aut','l_some')).
thf(typ_or3,type, (or3 : ($o>$o>$o>$o)), file('grundlagen.aut','l_or3'), [hashroot('418f21bcf146bb24b5eab2753c1a8363be2fc645e151822c7c92c194580b7f14')]).
thf(def_or3,definition, (or3 = (^ [X0:$o] : ^ [X1:$o] : ^ [X2:$o] : (l_or@X0@(l_or@X1@X2)))), file('grundlagen.aut','l_or3')).
thf(typ_and3,type, (and3 : ($o>$o>$o>$o)), file('grundlagen.aut','l_and3'), [hashroot('3ad9a6ed2a01263dc8d7b6f481cba22c1d37fbdc4f619b3fff5f7ba9dad2a256')]).
thf(def_and3,definition, (and3 = (^ [X0:$o] : ^ [X1:$o] : ^ [X2:$o] : (d_and@X0@(d_and@X1@X2)))), file('grundlagen.aut','l_and3')).
thf(typ_ec3,type, (ec3 : ($o>$o>$o>$o)), file('grundlagen.aut','l_ec3'), [hashroot('986599080791a6070d17046968b2c7f73c3d807171c7e5030cc97fd520a4e49c')]).
thf(def_ec3,definition, (ec3 = (^ [X0:$o] : ^ [X1:$o] : ^ [X2:$o] : (and3@(l_ec@X0@X1)@(l_ec@X1@X2)@(l_ec@X2@X0)))), file('grundlagen.aut','l_ec3')).
thf(typ_e_is,type, (e_is : ($i>$i>$i>$o)), [file('grundlagen.aut','l_e_is'),file('grundlagen.aut','l_r_ite_is')], [hashroot('8b0ca0dd413f254c6ad2f3c70f60a1023635d6ff3be83f7d4f2b7bf8ee537789')]).
thf(refis,axiom, (! [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@X0))@(^ [X1:$i] : (e_is@X0@X1@X1)))), file('grundlagen.aut','l_e_refis'), [hashroot('1bf646edae380151a0e2748e01eea959b3432fa03a8c3f343db0b2c15e87968a')]).
thf(e_isp,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : ((X1@X2) => ((e_is@X0@X2@X3) => (X1@X3)))))))), file('grundlagen.aut','l_e_isp'), [hashroot('6692832b0345b302e12f939fd8026269a93e31fbada95e487002d5ce57d7133a')]).
thf(typ_amone,type, (amone : ($i>($i>$o)>$o)), file('grundlagen.aut','l_e_amone'), [hashroot('abf9f4c92914eaea25fda61ade255847fa59a23ec25d76f12c98c471d2ae0d1c')]).
thf(def_amone,definition, (amone = (^ [X0:$i] : ^ [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : ((X1@X2) => ((X1@X3) => (e_is@X0@X2@X3))))))))), file('grundlagen.aut','l_e_amone')).
thf(typ_one,type, (one : ($i>($i>$o)>$o)), file('grundlagen.aut','l_e_one'), [hashroot('7091b9e752a98fbbb417ba534750e8d45ae4a3f689bed160c8f19a85db223071')]).
thf(def_one,definition, (one = (^ [X0:$i] : ^ [X1:($i>$o)] : (d_and@(amone@X0@X1)@(l_some@X0@X1)))), file('grundlagen.aut','l_e_one')).
thf(typ_ind,type, (ind : ($i>($i>$o)>$i)), file('grundlagen.aut','l_e_ind'), [hashroot('05a3a837678d78ea927e34ab8f1520d9ffa741c69b24fddb944e1a6b7c6ce154')]).
thf(ind_p,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((one@X0@X1) => (is_of@(ind@X0@X1)@(^ [X2:$i] : (in@X2@X0))))), file('grundlagen.aut','l_e_ind_p'), [hashroot('cb52cb4b70c1b6f750bb76194f40fe20558dd8ffe5dc04377803016edc6e440c')]).
thf(typ_injective,type, (injective : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_injective'), [hashroot('be5cf2b1b2ea79cef9c3ff78402a1be4b403b907b932d6425bb1967b37b53b61')]).
thf(def_injective,definition, (injective = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : (all@X0@(^ [X3:$i] : (all@X0@(^ [X4:$i] : (imp@(e_is@X1@(ap@X2@X3)@(ap@X2@X4))@(e_is@X0@X3@X4)))))))), file('grundlagen.aut','l_e_injective')).
thf(typ_image,type, (image : ($i>$i>$i>$i>$o)), file('grundlagen.aut','l_e_image'), [hashroot('64b250c443ba876e87705ae8c09e08f7ec69e63155f1c02e4adb9668ada8fce1')]).
thf(def_image,definition, (image = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : ^ [X3:$i] : (l_some@X0@(^ [X4:$i] : (e_is@X1@X3@(ap@X2@X4)))))), file('grundlagen.aut','l_e_image')).
thf(typ_tofs,type, (tofs : ($i>$i>$i>$i>$i)), [file('grundlagen.aut','l_e_tofs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_us')], [hashroot('d71dfa49596b38f4345149095b9e30951f7515ea54184386cb1c06dfc05b5408')]).
thf(def_tofs,definition, (tofs = (^ [X0:$i] : ^ [X1:$i] : ap)), [file('grundlagen.aut','l_e_tofs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_us')]).
thf(typ_soft,type, (soft : ($i>$i>$i>$i>$i)), [file('grundlagen.aut','l_e_soft'),file('grundlagen.aut','l_e_inj_so')], [hashroot('19640c3bc3eb360c29926376c3a20cba044317026fb0e5a810f33eee58bbdb28')]).
thf(def_soft,definition, (soft = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : ^ [X3:$i] : (ind@X0@(^ [X4:$i] : (e_is@X1@X3@(ap@X2@X4)))))), [file('grundlagen.aut','l_e_soft'),file('grundlagen.aut','l_e_inj_so')]).
thf(typ_e_in,type, (e_in : ($i>($i>$o)>$i>$i)), file('grundlagen.aut','l_e_in'), [hashroot('40a8d54e1c36f987e5c14c936a463272f0170c12f87e4577d32401d4d2954a6b')]).
thf(e_in_p,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@(d_Sep@X0@X1)))@(^ [X2:$i] : (is_of@(e_in@X0@X1@X2)@(^ [X3:$i] : (in@X3@X0)))))), file('grundlagen.aut','l_e_in_p'), [hashroot('3fd5ddc2bee1c842fb9f7164e355a4663d6b4b4cd7e9500cb535f40620a33aab')]).
thf(e_inp,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@(d_Sep@X0@X1)))@(^ [X2:$i] : (X1@(e_in@X0@X1@X2))))), file('grundlagen.aut','l_e_inp'), [hashroot('a8921f20e3015cdacdae28d1a0862708b3f26c836c7d63b7eade426a1aefe1b0')]).
thf(otax1,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (injective@(d_Sep@X0@X1)@X0@(d_Sigma@(d_Sep@X0@X1)@(e_in@X0@X1)))), file('grundlagen.aut','l_e_otax1'), [hashroot('52a1928b72e190169f28e1a70471dc523a9b0647cbe0051e850d2f911e122b4a')]).
thf(otax2,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : ((X1@X2) => (image@(d_Sep@X0@X1)@X0@(d_Sigma@(d_Sep@X0@X1)@(e_in@X0@X1))@X2))))), file('grundlagen.aut','l_e_otax2'), [hashroot('852baa2053ca1af6d4303ba531f26098375c757c6d962baf3c7f09638c3ba0aa')]).
thf(typ_out,type, (out : ($i>($i>$o)>$i>$i)), file('grundlagen.aut','l_e_out'), [hashroot('6fc658a37010bc78301a6074982c0339667b8258b5cd6365a3524f5d846fd607')]).
thf(def_out,definition, (out = (^ [X0:$i] : ^ [X1:($i>$o)] : (soft@(d_Sep@X0@X1)@X0@(d_Sigma@(d_Sep@X0@X1)@(e_in@X0@X1))))), file('grundlagen.aut','l_e_out')).
thf(typ_esti,type, (esti : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_st_esti'), [hashroot('b5666312851a073ed6c2b15baf0c1169dca70f962aeae381609dcf2fb5290190')]).
thf(setof_p,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (is_of@(d_Sep@X0@X1)@(^ [X2:$i] : (in@X2@(power@X0))))), file('grundlagen.aut','l_e_st_setof_p'), [hashroot('726959124ef0b154680ad4a66876dea2dd2a2ed4ea61021f1b199a04e4cbe2ac')]).
thf(typ_incl,type, (incl : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_st_incl'), [hashroot('109375b120ee551c6b2797d8d33d791659272dc8e60f12e0b9c9fca39c7b0e37')]).
thf(def_incl,definition, (incl = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : (all@X0@(^ [X3:$i] : (imp@(esti@X0@X3@X1)@(esti@X0@X3@X2)))))), file('grundlagen.aut','l_e_st_incl')).
thf(isseti,axiom, (! [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@(power@X0)))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@(power@X0)))@(^ [X2:$i] : ((incl@X0@X1@X2) => ((incl@X0@X2@X1) => (e_is@(power@X0)@X1@X2)))))))), file('grundlagen.aut','l_e_st_isseti'), [hashroot('be2b238751e629a983bdc8c851ef02e86f20bd76386ca0c3070c8a741c3eec5b')]).
thf(typ_nissetprop,type, (nissetprop : ($i>$i>$i>$i>$o)), file('grundlagen.aut','l_e_st_isset_nissetprop'), [hashroot('91edefb202f3c8e7446ce95cfbb4dc96f34c88b7aefa3e828685e23d3b17344e')]).
thf(def_nissetprop,definition, (nissetprop = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : ^ [X3:$i] : (d_and@(esti@X0@X3@X1)@(d_not@(esti@X0@X3@X2))))), file('grundlagen.aut','l_e_st_isset_nissetprop')).
thf(typ_rat,type, (rat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rat'), [hashroot('e034fba97d474a28e571a50e33c2074cc52e74b2bfd0b39f8238729be437398c')]).
thf(typ_rt_is,type, (rt_is : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_is'), [hashroot('843f318e7ed2dd89e753dc9b57323506dffc34fb4e958f5446f64f4b65fed1c7')]).
thf(typ_rt_some,type, (rt_some : (($i>$o)>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_some'), [hashroot('000e1d4b3e8e84e42ee35129ef27e0d1f608ca44d2a1a9e51736c4366076269b')]).
thf(def_rt_some,definition, (rt_some = (l_some@rat)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_some')).
thf(typ_rt_all,type, (rt_all : (($i>$o)>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_all'), [hashroot('6c89362735d593c40c7a8225741b842ae25b4f480a9005ed3acd9677100108be')]).
thf(def_rt_all,definition, (rt_all = (all@rat)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_all')).
thf(typ_rt_in,type, (rt_in : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_in'), [hashroot('7f46f6c8ab68bdfff34e37a546dbc15578516096e082efe58bc537d3ddafcd2c')]).
thf(def_rt_in,definition, (rt_in = (esti@rat)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_in')).
thf(typ_rt_more,type, (rt_more : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_more'), [hashroot('fc203457de3e5904d76f645a9a49a7a58239fc37a460d310d791f0587445d8b1')]).
thf(typ_rt_less,type, (rt_less : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_less'), [hashroot('333ecaa7a27f87dc04dc759475948739bad04fb6190313d42735f20aa00cc085')]).
thf(satz81b,axiom, (all_of@(^ [X0:$i] : (in@X0@rat))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@rat))@(^ [X1:$i] : (ec3@(rt_is@X0@X1)@(rt_more@X0@X1)@(rt_less@X0@X1)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_satz81b'), [hashroot('cbca724babf29e621703befedd1e34f18ced25def6cba3a3002d77fcb7f825e6')]).
thf(satz83,axiom, (all_of@(^ [X0:$i] : (in@X0@rat))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@rat))@(^ [X1:$i] : ((rt_less@X0@X1) => (rt_more@X1@X0)))))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_satz83'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_3134_t2'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_3140_t9'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_3140_t23'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_3140_t43'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_5p205_t34')], [hashroot('c999d019a1644fccb857511ab38eb85ba8b09fbf45e9ee815e2059b698240346')]).
thf(typ_cutprop1,type, (cutprop1 : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cutprop1'), [hashroot('2202f03fe11d08d6ff3e3cc2fb1ff5112a00c9d2ef60de1b958c035457e51751')]).
thf(typ_cutprop2a,type, (cutprop2a : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cutprop2a'), [hashroot('fb1a2bc180831f827389735d2fe2f514023e923305f587580115a235060671f1')]).
thf(def_cutprop2a,definition, (cutprop2a = (^ [X0:$i] : ^ [X1:$i] : (rt_all@(^ [X2:$i] : (imp@(d_not@(rt_in@X2@X0))@(rt_less@X1@X2)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cutprop2a')).
thf(typ_cutprop2,type, (cutprop2 : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cutprop2'), [hashroot('cbb7826890be1042c418e0f36a16854db78afc1319a1f78d1f293fe1bc1c51f8')]).
thf(def_cutprop2,definition, (cutprop2 = (^ [X0:$i] : (rt_all@(^ [X1:$i] : (imp@(rt_in@X1@X0)@(cutprop2a@X0@X1)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cutprop2')).
thf(typ_cutprop3,type, (cutprop3 : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cutprop3'), [hashroot('fe161e5ae3aac66a6f503571c7eca75330fe5289917a05437e97a2f0081cb4ec')]).
thf(typ_cutprop,type, (cutprop : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cutprop'), [hashroot('bd043cfe0c73a400f837f25be116d532b89eb0188641751038f44c008e0a9684')]).
thf(def_cutprop,definition, (cutprop = (^ [X0:$i] : (and3@(cutprop1@X0)@(cutprop2@X0)@(cutprop3@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cutprop')).
thf(typ_cut,type, (cut : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cut'), [hashroot('4e4ed30803b18f94089b782cbb02cbec09b7993c5cdb0300fa191d3e3af33344')]).
thf(def_cut,definition, (cut = (d_Sep@(power@rat)@cutprop)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cut')).
thf(typ_lcl,type, (lcl : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_lcl'), [hashroot('fd56d67eee5ac94ee743ea5d5402bbf5a1cfcf5310118aac1ab8a5862067b310')]).
thf(def_lcl,definition, (lcl = (e_in@(power@rat)@cutprop)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_lcl')).
thf(typ_lrt,type, (lrt : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_lrt'), [hashroot('ab72f5bbe393f6bbcef940f9063cb7313103a495dbf413f153ca5bd1b8ee5ba9')]).
thf(def_lrt,definition, (lrt = (^ [X0:$i] : ^ [X1:$i] : (rt_in@X1@(lcl@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_lrt')).
thf(typ_urt,type, (urt : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_urt'), [hashroot('17a9b4b946c1a402194040679b9220748ce517cd704b3b223a8be89ae1e954fc')]).
thf(def_urt,definition, (urt = (^ [X0:$i] : ^ [X1:$i] : (d_not@(rt_in@X1@(lcl@X0))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_urt')).
thf(typ_rp_is,type, (rp_is : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_is'), [hashroot('d8b675a0120cea29d5c6b85ee93e20272a34d7cbbfb7974311f160661823a44a')]).
thf(def_rp_is,definition, (rp_is = (e_is@cut)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_is')).
thf(typ_rp_nis,type, (rp_nis : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_nis'), [hashroot('443db1f5ac65528e30726227db88fd0d6c0dac5f381e8f3ae78357a00a3edf62')]).
thf(def_rp_nis,definition, (rp_nis = (^ [X0:$i] : ^ [X1:$i] : (d_not@(rp_is@X0@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_nis')).
thf(typ_cutof,type, (cutof : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_cutof'), [hashroot('c02c32fa110194a3876f0caf26abeb006979954ef27677c740e0e3cadd919f02')]).
thf(def_cutof,definition, (cutof = (out@(power@rat)@cutprop)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_cutof')).
thf(typ_rp_more,type, (rp_more : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_more'), [hashroot('c9220f6892c986404f80026f7ab487a4c861d91a0bcda6dad4df303cf540d347')]).
thf(def_rp_more,definition, (rp_more = (^ [X0:$i] : ^ [X1:$i] : (rt_some@(^ [X2:$i] : (d_and@(lrt@X0@X2)@(urt@X1@X2)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_more')).
thf(typ_rp_less,type, (rp_less : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_less'), [hashroot('4bcfc77830cb15b35349105057b9461ce89c6ce8a2e5573c1afd52b123bb0ea9')]).
thf(def_rp_less,definition, (rp_less = (^ [X0:$i] : ^ [X1:$i] : (rt_some@(^ [X2:$i] : (d_and@(urt@X0@X2)@(lrt@X1@X2)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_less')).
thf(satz121,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : ((rp_more@X0@X1) => (rp_less@X1@X0)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz121'), [hashroot('f765fb9c6470dccd5a3cf2cf0e82d2633192ebccdff8f7d9a0d52a3ab533f964')]).
thf(k_2123_b,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (or3@(rp_is@X0@X1)@(rp_more@X0@X1)@(rp_less@X0@X1)))))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2123_b'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz123a'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_3136_t1'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_4146_t1')], [hashroot('2f118645cb0f4a176a2b188f93f12401981b39716c6f219b2e92f69ad034b15d')]).
thf(k_2123_a,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (ec3@(rp_is@X0@X1)@(rp_more@X0@X1)@(rp_less@X0@X1)))))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2123_a'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz123b')], [hashroot('8cd2a53c939f21743d22719468a163a8181e5dac6e7b3b164686d64f486eb636')]).
thf(typ_prodprop,type, (prodprop : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_prodprop'), [hashroot('e5752b4755b6b64df90760d4f791f6cfe273645e122a8f3f7bf0fe050751e26c')]).
thf(typ_prod,type, (prod : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_prod'), [hashroot('e1af1af65c46f968c50b032d1476439819a413f55877f5859b4e82bf7b62e4e7')]).
thf(def_prod,definition, (prod = (^ [X0:$i] : ^ [X1:$i] : (d_Sep@rat@(prodprop@X0@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_prod')).
thf(satz141,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (cutprop@(prod@X0@X1)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz141'), [hashroot('8a2def5e0ee4442ff2bd154a0a469ebc5dfcd28e9dffced9a24f31ea8621be47')]).
thf(typ_rp_ts,type, (rp_ts : ($i>$i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ts'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_iv5d_rs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ivad_rs')], [hashroot('a596bf4c85359c9fb6bd7c2f0e03118408a0e95346ede301036731333237fe2f')]).
thf(def_rp_ts,definition, (rp_ts = (^ [X0:$i] : ^ [X1:$i] : (cutof@(prod@X0@X1)))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ts'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_iv5d_rs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ivad_rs')]).
thf(satz145a,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@cut))@(^ [X2:$i] : ((rp_more@X0@X1) => (rp_more@(rp_ts@X0@X2)@(rp_ts@X1@X2))))))))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz145a'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_4147_t1')], [hashroot('5472eaa259707b402d99aece475e1d81a960089573846c5b4c3d15855f0997a7')]).
thf(rp_ists1,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@cut))@(^ [X2:$i] : ((rp_is@X0@X1) => (rp_is@(rp_ts@X0@X2)@(rp_ts@X1@X2))))))))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ists1'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz145b')], [hashroot('788cbe8066b5aa0f142d569cbe5645fc672f0b64afc352561d7bd10771ba17be')]).
thf(satz146c,conjecture, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@cut))@(^ [X2:$i] : ((rp_less@(rp_ts@X0@X2)@(rp_ts@X1@X2)) => (rp_less@X0@X1)))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz146c'), [hashroot('988775c973bfc140cd1a31f52f76f6e0246f598fc02f0b5968713dd9a3ec5e32')]).
