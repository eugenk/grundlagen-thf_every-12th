thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_power,type, (power : ($i>$i)), file('grundlagen.aut','l_e_st_set'), [hashroot('0c5490ca2f6d61c2d410e7907be97b3bc36b3e4de614e1f5431278dbccad4c79')]).
thf(typ_d_Sep,type, (d_Sep : ($i>($i>$o)>$i)), [file('grundlagen.aut','l_e_ot'),file('grundlagen.aut','l_e_st_setof')], [hashroot('218a016b04c605cddd8baabd01b9102f19cbd21e943087fb26bfbb5c1990d944')]).
thf(typ_ordsucc,type, (ordsucc : ($i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_suc'),file('grundlagen.aut','l_e_st_eq_landau_n_8274_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8276_xs')], [hashroot('6f2bd357b24438c387f3433d2366b843d110c03ef0b8bd82c5a0cb92bfc5b50d')]).
thf(typ_d_Sigma,type, (d_Sigma : ($i>($i>$i)>$i)), unknown, [hashroot('75c40645d5fe236c1ca11856fa4f32b0ebb4c95c94a9615ec27513c1c1b6b5d3')]).
thf(typ_ap,type, (ap : ($i>$i>$i)), unknown, [hashroot('a7decc6ab7cd5672b0a906b215a444cd9aa45edcb0967cd0c9d40e207f8df623')]).
thf(beta,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@X0) => ((ap@(d_Sigma@X0@X1)@X2) = (X1@X2)))), unknown, [hashroot('2ad5629b8f82190c8d16e96ea95dadf81dafc029ae9a3396d9cb2d171e6fce86')]).
thf(typ_d_Pi,type, (d_Pi : ($i>($i>$i)>$i)), unknown, [hashroot('68d9661a0d30c2de45df1cf7708fadc581b6b52cb2ba22f5f89c21527ade885f')]).
thf(lam_Pi,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X0) => (in@(X2@X3)@(X1@X3)))) => (in@(d_Sigma@X0@X2)@(d_Pi@X0@X1)))), unknown, [hashroot('0188856999da4593b9c9db87d02370f335d61bff2c902d2a1a4414a301b486fb')]).
thf(ap_Pi,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ! [X3:$i] : ((in@X2@(d_Pi@X0@X1)) => ((in@X3@X0) => (in@(ap@X2@X3)@(X1@X3))))), unknown, [hashroot('273853e08fc3f975cbaf03343d417034c249ee5074275a7321273698d3bc1b72')]).
thf(typ_imp,type, (imp : ($o>$o>$o)), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')], [hashroot('c471f7f9b55e8fce15c5b7f92a344808917c7bdfbac8043315ef5a52fb233e79')]).
thf(def_imp,definition, (imp = (^ [X0:$o] : ^ [X1:$o] : (X0 => X1))), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')]).
thf(typ_d_not,type, (d_not : ($o>$o)), file('grundlagen.aut','l_not'), [hashroot('122455a7450b0bbfcd7daa48af7a7a4bf320a8f085b235e0bfe56f0a1de2967f')]).
thf(def_d_not,definition, (d_not = (^ [X0:$o] : (imp@X0@$false))), file('grundlagen.aut','l_not')).
thf(typ_wel,type, (wel : ($o>$o)), file('grundlagen.aut','l_wel'), [hashroot('3c7461ae99cc5fcbcf4c6eb8756a1630dde3bca516b9da8a16686247350ecfaf')]).
thf(def_wel,definition, (wel = (^ [X0:$o] : (d_not@(d_not@X0)))), file('grundlagen.aut','l_wel')).
thf(l_et,axiom, (! [X0:$o] : ((wel@X0) => X0)), file('grundlagen.aut','l_et'), [hashroot('68f4fbe1a4cdb31228d4806333cc9f14319ec737076e00d4ccc0adf1acc3752f')]).
thf(typ_l_ec,type, (l_ec : ($o>$o>$o)), file('grundlagen.aut','l_ec'), [hashroot('f505a8e15aa62b7fbc5d6fc63dc5c3c8e06aade6488051165390a10e2e57ad6e')]).
thf(def_l_ec,definition, (l_ec = (^ [X0:$o] : ^ [X1:$o] : (imp@X0@(d_not@X1)))), file('grundlagen.aut','l_ec')).
thf(typ_d_and,type, (d_and : ($o>$o>$o)), file('grundlagen.aut','l_and'), [hashroot('256969ce0d10af13735c022198c8269b31ef9b2f35171d9ad3e422d566773b0f')]).
thf(def_d_and,definition, (d_and = (^ [X0:$o] : ^ [X1:$o] : (d_not@(l_ec@X0@X1)))), file('grundlagen.aut','l_and')).
thf(typ_l_or,type, (l_or : ($o>$o>$o)), file('grundlagen.aut','l_or'), [hashroot('d609be23d396e7ff7e091175cfb557c25e3ced2809a41de7e4d98fc9524ed072')]).
thf(def_l_or,definition, (l_or = (^ [X0:$o] : (imp@(d_not@X0)))), file('grundlagen.aut','l_or')).
thf(typ_all,type, (all : ($i>($i>$o)>$o)), file('grundlagen.aut','l_all'), [hashroot('7af775405bb712aec9ce7cdde103343acfd567932781aa9cedcee8d57f9a249d')]).
thf(def_all,definition, (all = (^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@X0))))), file('grundlagen.aut','l_all')).
thf(typ_non,type, (non : ($i>($i>$o)>$i>$o)), file('grundlagen.aut','l_non'), [hashroot('e0c99b0aabd21f260538f4a9cc7b2f9dd1c1be40c3fd3c26d5355897e80de94b')]).
thf(def_non,definition, (non = (^ [X0:$i] : ^ [X1:($i>$o)] : ^ [X2:$i] : (d_not@(X1@X2)))), file('grundlagen.aut','l_non')).
thf(typ_l_some,type, (l_some : ($i>($i>$o)>$o)), file('grundlagen.aut','l_some'), [hashroot('e725afc4936069e5e8c753118fc6844ba6a33e0b7ed57300c90a0659591806c9')]).
thf(def_l_some,definition, (l_some = (^ [X0:$i] : ^ [X1:($i>$o)] : (d_not@(all_of@(^ [X2:$i] : (in@X2@X0))@(non@X0@X1))))), file('grundlagen.aut','l_some')).
thf(typ_e_is,type, (e_is : ($i>$i>$i>$o)), [file('grundlagen.aut','l_e_is'),file('grundlagen.aut','l_r_ite_is')], [hashroot('8b0ca0dd413f254c6ad2f3c70f60a1023635d6ff3be83f7d4f2b7bf8ee537789')]).
thf(refis,axiom, (! [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@X0))@(^ [X1:$i] : (e_is@X0@X1@X1)))), file('grundlagen.aut','l_e_refis'), [hashroot('1bf646edae380151a0e2748e01eea959b3432fa03a8c3f343db0b2c15e87968a')]).
thf(e_isp,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : ((X1@X2) => ((e_is@X0@X2@X3) => (X1@X3)))))))), file('grundlagen.aut','l_e_isp'), [hashroot('6692832b0345b302e12f939fd8026269a93e31fbada95e487002d5ce57d7133a')]).
thf(typ_amone,type, (amone : ($i>($i>$o)>$o)), file('grundlagen.aut','l_e_amone'), [hashroot('abf9f4c92914eaea25fda61ade255847fa59a23ec25d76f12c98c471d2ae0d1c')]).
thf(def_amone,definition, (amone = (^ [X0:$i] : ^ [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : ((X1@X2) => ((X1@X3) => (e_is@X0@X2@X3))))))))), file('grundlagen.aut','l_e_amone')).
thf(typ_one,type, (one : ($i>($i>$o)>$o)), file('grundlagen.aut','l_e_one'), [hashroot('7091b9e752a98fbbb417ba534750e8d45ae4a3f689bed160c8f19a85db223071')]).
thf(def_one,definition, (one = (^ [X0:$i] : ^ [X1:($i>$o)] : (d_and@(amone@X0@X1)@(l_some@X0@X1)))), file('grundlagen.aut','l_e_one')).
thf(typ_ind,type, (ind : ($i>($i>$o)>$i)), file('grundlagen.aut','l_e_ind'), [hashroot('05a3a837678d78ea927e34ab8f1520d9ffa741c69b24fddb944e1a6b7c6ce154')]).
thf(ind_p,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((one@X0@X1) => (is_of@(ind@X0@X1)@(^ [X2:$i] : (in@X2@X0))))), file('grundlagen.aut','l_e_ind_p'), [hashroot('cb52cb4b70c1b6f750bb76194f40fe20558dd8ffe5dc04377803016edc6e440c')]).
thf(oneax,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((one@X0@X1) => (X1@(ind@X0@X1)))), file('grundlagen.aut','l_e_oneax'), [hashroot('973a4c7481a50552d944a3d58539f23bcab406a282d11ccdf66ced4c3566caac')]).
thf(typ_injective,type, (injective : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_injective'), [hashroot('be5cf2b1b2ea79cef9c3ff78402a1be4b403b907b932d6425bb1967b37b53b61')]).
thf(def_injective,definition, (injective = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : (all@X0@(^ [X3:$i] : (all@X0@(^ [X4:$i] : (imp@(e_is@X1@(ap@X2@X3)@(ap@X2@X4))@(e_is@X0@X3@X4)))))))), file('grundlagen.aut','l_e_injective')).
thf(typ_image,type, (image : ($i>$i>$i>$i>$o)), file('grundlagen.aut','l_e_image'), [hashroot('64b250c443ba876e87705ae8c09e08f7ec69e63155f1c02e4adb9668ada8fce1')]).
thf(def_image,definition, (image = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : ^ [X3:$i] : (l_some@X0@(^ [X4:$i] : (e_is@X1@X3@(ap@X2@X4)))))), file('grundlagen.aut','l_e_image')).
thf(typ_tofs,type, (tofs : ($i>$i>$i>$i>$i)), [file('grundlagen.aut','l_e_tofs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_us')], [hashroot('d71dfa49596b38f4345149095b9e30951f7515ea54184386cb1c06dfc05b5408')]).
thf(def_tofs,definition, (tofs = (^ [X0:$i] : ^ [X1:$i] : ap)), [file('grundlagen.aut','l_e_tofs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_us')]).
thf(typ_soft,type, (soft : ($i>$i>$i>$i>$i)), [file('grundlagen.aut','l_e_soft'),file('grundlagen.aut','l_e_inj_so')], [hashroot('19640c3bc3eb360c29926376c3a20cba044317026fb0e5a810f33eee58bbdb28')]).
thf(def_soft,definition, (soft = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : ^ [X3:$i] : (ind@X0@(^ [X4:$i] : (e_is@X1@X3@(ap@X2@X4)))))), [file('grundlagen.aut','l_e_soft'),file('grundlagen.aut','l_e_inj_so')]).
thf(typ_e_in,type, (e_in : ($i>($i>$o)>$i>$i)), file('grundlagen.aut','l_e_in'), [hashroot('40a8d54e1c36f987e5c14c936a463272f0170c12f87e4577d32401d4d2954a6b')]).
thf(e_in_p,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@(d_Sep@X0@X1)))@(^ [X2:$i] : (is_of@(e_in@X0@X1@X2)@(^ [X3:$i] : (in@X3@X0)))))), file('grundlagen.aut','l_e_in_p'), [hashroot('3fd5ddc2bee1c842fb9f7164e355a4663d6b4b4cd7e9500cb535f40620a33aab')]).
thf(e_inp,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@(d_Sep@X0@X1)))@(^ [X2:$i] : (X1@(e_in@X0@X1@X2))))), file('grundlagen.aut','l_e_inp'), [hashroot('a8921f20e3015cdacdae28d1a0862708b3f26c836c7d63b7eade426a1aefe1b0')]).
thf(otax1,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (injective@(d_Sep@X0@X1)@X0@(d_Sigma@(d_Sep@X0@X1)@(e_in@X0@X1)))), file('grundlagen.aut','l_e_otax1'), [hashroot('52a1928b72e190169f28e1a70471dc523a9b0647cbe0051e850d2f911e122b4a')]).
thf(otax2,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : ((X1@X2) => (image@(d_Sep@X0@X1)@X0@(d_Sigma@(d_Sep@X0@X1)@(e_in@X0@X1))@X2))))), file('grundlagen.aut','l_e_otax2'), [hashroot('852baa2053ca1af6d4303ba531f26098375c757c6d962baf3c7f09638c3ba0aa')]).
thf(typ_out,type, (out : ($i>($i>$o)>$i>$i)), file('grundlagen.aut','l_e_out'), [hashroot('6fc658a37010bc78301a6074982c0339667b8258b5cd6365a3524f5d846fd607')]).
thf(def_out,definition, (out = (^ [X0:$i] : ^ [X1:($i>$o)] : (soft@(d_Sep@X0@X1)@X0@(d_Sigma@(d_Sep@X0@X1)@(e_in@X0@X1))))), file('grundlagen.aut','l_e_out')).
thf(typ_prop1,type, (prop1 : ($o>$i>$i>$i>$i>$o)), [file('grundlagen.aut','l_e_ite_prop1'),file('grundlagen.aut','l_r_ite_prop1')], [hashroot('127f3a67f12abba06108ee16227ccf0156cfa065b5ce338a87dc4a1a04f1e7b0')]).
thf(def_prop1,definition, (prop1 = (^ [X0:$o] : ^ [X1:$i] : ^ [X2:$i] : ^ [X3:$i] : ^ [X4:$i] : (d_and@(imp@X0@(e_is@X1@X4@X2))@(imp@(d_not@X0)@(e_is@X1@X4@X3))))), [file('grundlagen.aut','l_e_ite_prop1'),file('grundlagen.aut','l_r_ite_prop1')]).
thf(typ_ite,type, (ite : ($o>$i>$i>$i>$i)), [file('grundlagen.aut','l_e_ite'),file('grundlagen.aut','l_r_ite')], [hashroot('0d0bd40dc6689040d63b44d98bb841db103214506f7ad3b713d9a03fd773652b')]).
thf(def_ite,definition, (ite = (^ [X0:$o] : ^ [X1:$i] : ^ [X2:$i] : ^ [X3:$i] : (ind@X1@(prop1@X0@X1@X2@X3)))), [file('grundlagen.aut','l_e_ite'),file('grundlagen.aut','l_r_ite')]).
thf(typ_esti,type, (esti : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_st_esti'), [hashroot('b5666312851a073ed6c2b15baf0c1169dca70f962aeae381609dcf2fb5290190')]).
thf(setof_p,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (is_of@(d_Sep@X0@X1)@(^ [X2:$i] : (in@X2@(power@X0))))), file('grundlagen.aut','l_e_st_setof_p'), [hashroot('726959124ef0b154680ad4a66876dea2dd2a2ed4ea61021f1b199a04e4cbe2ac')]).
thf(estii,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : ((X1@X2) => (esti@X0@X2@(d_Sep@X0@X1)))))), file('grundlagen.aut','l_e_st_estii'), [hashroot('ad68ae83636f8c74da7d6c1928894f670f7f9b70a3a394859f89e77c78e07416')]).
thf(estie,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : ((esti@X0@X2@(d_Sep@X0@X1)) => (X1@X2))))), file('grundlagen.aut','l_e_st_estie'), [hashroot('c55812877be6f0f9110f14b90aebd810ab9d4254c2fdadb89279d0b7efae4002')]).
thf(typ_nonempty,type, (nonempty : ($i>$i>$o)), file('grundlagen.aut','l_e_st_nonempty'), [hashroot('589508fa99afe2c763a9af671c29bddf10ecac3858162dcf46ae523265a15d50')]).
thf(def_nonempty,definition, (nonempty = (^ [X0:$i] : ^ [X1:$i] : (l_some@X0@(^ [X2:$i] : (esti@X0@X2@X1))))), file('grundlagen.aut','l_e_st_nonempty')).
thf(typ_incl,type, (incl : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_st_incl'), [hashroot('109375b120ee551c6b2797d8d33d791659272dc8e60f12e0b9c9fca39c7b0e37')]).
thf(def_incl,definition, (incl = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : (all@X0@(^ [X3:$i] : (imp@(esti@X0@X3@X1)@(esti@X0@X3@X2)))))), file('grundlagen.aut','l_e_st_incl')).
thf(isseti,axiom, (! [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@(power@X0)))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@(power@X0)))@(^ [X2:$i] : ((incl@X0@X1@X2) => ((incl@X0@X2@X1) => (e_is@(power@X0)@X1@X2)))))))), file('grundlagen.aut','l_e_st_isseti'), [hashroot('be2b238751e629a983bdc8c851ef02e86f20bd76386ca0c3070c8a741c3eec5b')]).
thf(typ_ecelt,type, (ecelt : ($i>($i>$i>$o)>$i>$i)), file('grundlagen.aut','l_e_st_eq_ecelt'), [hashroot('3843b25d78fd901565bb72e67dffca3f0543d16219b564e93bd1b2e8b8e0ea1f')]).
thf(def_ecelt,definition, (ecelt = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : ^ [X2:$i] : (d_Sep@X0@(X1@X2)))), file('grundlagen.aut','l_e_st_eq_ecelt')).
thf(typ_ecp,type, (ecp : ($i>($i>$i>$o)>$i>$i>$o)), file('grundlagen.aut','l_e_st_eq_ecp'), [hashroot('fa6b51b00e83bf720701259dbd4491863ab2749455492e59ca3912870a5f5051')]).
thf(def_ecp,definition, (ecp = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : ^ [X2:$i] : ^ [X3:$i] : (e_is@(power@X0)@X2@(ecelt@X0@X1@X3)))), file('grundlagen.aut','l_e_st_eq_ecp')).
thf(typ_anec,type, (anec : ($i>($i>$i>$o)>$i>$o)), file('grundlagen.aut','l_e_st_eq_anec'), [hashroot('c9d0bac44c23951a0680ca0d539160d4659408e1c67164511df41008292a3992')]).
thf(def_anec,definition, (anec = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : ^ [X2:$i] : (l_some@X0@(ecp@X0@X1@X2)))), file('grundlagen.aut','l_e_st_eq_anec')).
thf(typ_ect,type, (ect : ($i>($i>$i>$o)>$i)), file('grundlagen.aut','l_e_st_eq_ect'), [hashroot('823ce29188d22e649364fbe792619be134bb5c3790481f138f0c17ca913615c0')]).
thf(def_ect,definition, (ect = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : (d_Sep@(power@X0)@(anec@X0@X1)))), file('grundlagen.aut','l_e_st_eq_ect')).
thf(typ_ectset,type, (ectset : ($i>($i>$i>$o)>$i>$i)), file('grundlagen.aut','l_e_st_eq_ectset'), [hashroot('5c2ecf1f16ae73e953b46c98a2d237b8897dd8e8bce321e6a404be7c930b9b00')]).
thf(def_ectset,definition, (ectset = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : (out@(power@X0)@(anec@X0@X1)))), file('grundlagen.aut','l_e_st_eq_ectset')).
thf(typ_ectelt,type, (ectelt : ($i>($i>$i>$o)>$i>$i)), file('grundlagen.aut','l_e_st_eq_ectelt'), [hashroot('734921f536da98263b509dae80789b4810a3bec5013021675ca4bc3eeeaf7a1b')]).
thf(def_ectelt,definition, (ectelt = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : ^ [X2:$i] : (ectset@X0@X1@(ecelt@X0@X1@X2)))), file('grundlagen.aut','l_e_st_eq_ectelt')).
thf(typ_ecect,type, (ecect : ($i>($i>$i>$o)>$i>$i)), file('grundlagen.aut','l_e_st_eq_ecect'), [hashroot('814e39bc591e991d3258776fa1ab2bc04a7ef3c4078059bfe7c93ce1bc13d4e9')]).
thf(def_ecect,definition, (ecect = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : (e_in@(power@X0)@(anec@X0@X1)))), file('grundlagen.aut','l_e_st_eq_ecect')).
thf(typ_fixfu,type, (fixfu : ($i>($i>$i>$o)>$i>$i>$o)), file('grundlagen.aut','l_e_st_eq_fixfu'), [hashroot('c8ee603cf4c23510dd666716033feffd5c3983b8fe41f60275c6f9120074acc1')]).
thf(def_fixfu,definition, (fixfu = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : ^ [X2:$i] : ^ [X3:$i] : (all_of@(^ [X4:$i] : (in@X4@X0))@(^ [X4:$i] : (all_of@(^ [X5:$i] : (in@X5@X0))@(^ [X5:$i] : ((X1@X4@X5) => (e_is@X2@(ap@X3@X4)@(ap@X3@X5))))))))), file('grundlagen.aut','l_e_st_eq_fixfu')).
thf(typ_d_10_prop1,type, (d_10_prop1 : ($i>($i>$i>$o)>$i>$i>$i>$i>$i>$o)), file('grundlagen.aut','l_e_st_eq_10_prop1'), [hashroot('906ba1bf38946ae8a1baaf7b37392d40317dca1ee7e74585b3ebd7e2f222ff16')]).
thf(def_d_10_prop1,definition, (d_10_prop1 = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : ^ [X2:$i] : ^ [X3:$i] : ^ [X4:$i] : ^ [X5:$i] : ^ [X6:$i] : (d_and@(esti@X0@X6@(ecect@X0@X1@X4))@(e_is@X2@(ap@X3@X6)@X5)))), file('grundlagen.aut','l_e_st_eq_10_prop1')).
thf(typ_prop2,type, (prop2 : ($i>($i>$i>$o)>$i>$i>$i>$i>$o)), file('grundlagen.aut','l_e_st_eq_10_prop2'), [hashroot('de9a18669dd1dfd4a6692f7361da17cb38aa75e5dd54b965fdc0cc9f08b22673')]).
thf(def_prop2,definition, (prop2 = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : ^ [X2:$i] : ^ [X3:$i] : ^ [X4:$i] : ^ [X5:$i] : (l_some@X0@(d_10_prop1@X0@X1@X2@X3@X4@X5)))), file('grundlagen.aut','l_e_st_eq_10_prop2')).
thf(typ_indeq,type, (indeq : ($i>($i>$i>$o)>$i>$i>$i>$i)), file('grundlagen.aut','l_e_st_eq_indeq'), [hashroot('e39ba97935ef59e9ecfca68387318d37c52b8727e7c99436eb2efd3bb06b2358')]).
thf(def_indeq,definition, (indeq = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : ^ [X2:$i] : ^ [X3:$i] : ^ [X4:$i] : (ind@X2@(prop2@X0@X1@X2@X3@X4)))), file('grundlagen.aut','l_e_st_eq_indeq')).
thf(typ_nat,type, (nat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_nat'), [hashroot('a70cf6792dc8538ebedafac6b0b6924bbcece48e47a0c0cb77b73b3cb5eba62f')]).
thf(typ_n_is,type, (n_is : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_is'),file('grundlagen.aut','l_e_st_eq_landau_n_29_i')], [hashroot('f438ef4d45e17e52a3513bcba965b0f71de0b41c1f2de432a49525837a2585a6')]).
thf(def_n_is,definition, (n_is = (e_is@nat)), [file('grundlagen.aut','l_e_st_eq_landau_n_is'),file('grundlagen.aut','l_e_st_eq_landau_n_29_i')]).
thf(typ_nis,type, (nis : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_nis'), [hashroot('3fd3dd3f94a190d22a5db911977f86d4e9ea9b3dea63d7982205749691662311')]).
thf(def_nis,definition, (nis = (^ [X0:$i] : ^ [X1:$i] : (d_not@(n_is@X0@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_nis')).
thf(typ_n_all,type, (n_all : (($i>$o)>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_all'), [hashroot('e66de03c63adc714a3c556fbbe3180d9d72c12de6daae2b0e3b32fb35350ccc0')]).
thf(typ_n_1,type, (n_1 : $i), file('grundlagen.aut','l_e_st_eq_landau_n_1'), [hashroot('72510cdcd4be46383fe2950c32184464e04a3a48f2bbb6fb2e0e938a74c551d6')]).
thf(n_1_p,axiom, (is_of@n_1@(^ [X0:$i] : (in@X0@nat))), file('grundlagen.aut','l_e_st_eq_landau_n_1_p'), [hashroot('7b99e30acc4d1ee1179e423e7c0c1002b7193b2b58635a27a801525bd3ab6927')]).
thf(suc_p,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (is_of@(ordsucc@X0)@(^ [X1:$i] : (in@X1@nat))))), [file('grundlagen.aut','l_e_st_eq_landau_n_suc_p'),file('grundlagen.aut','l_e_st_eq_landau_n_8274_xs_p'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_xs_p'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8276_xs_p')], [hashroot('4b682c03fdb0ba422da554f02f486f38fa8dcf1a28d78a7f50821f808c69f123')]).
thf(n_ax3,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (nis@(ordsucc@X0)@n_1))), file('grundlagen.aut','l_e_st_eq_landau_n_ax3'), [hashroot('5684aa6f556d25f24a0d6a91fa368f619dc42743fb35d9b9a03e8e017f509be1')]).
thf(typ_d_24_prop1,type, (d_24_prop1 : ($i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_24_prop1'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_nt_54_prop1')], [hashroot('b52a03caf6f61ea7b4d7a53c556ef494863fed3c437f64b1390fed5afc3ddc7c')]).
thf(def_d_24_prop1,definition, (d_24_prop1 = (^ [X0:$i] : (n_all@(^ [X1:$i] : (n_is@(ap@X0@(ordsucc@X1))@(ordsucc@(ap@X0@X1))))))), [file('grundlagen.aut','l_e_st_eq_landau_n_24_prop1'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_nt_54_prop1')]).
thf(typ_d_24_prop2,type, (d_24_prop2 : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_24_prop2'), [hashroot('ce45aa38af51cc5b5f1a1d5eaf92d8c233ee8e401f2933477e149047fb282785')]).
thf(def_d_24_prop2,definition, (d_24_prop2 = (^ [X0:$i] : ^ [X1:$i] : (d_and@(n_is@(ap@X1@n_1)@(ordsucc@X0))@(d_24_prop1@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_24_prop2')).
thf(satz4,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (one@(d_Pi@nat@(^ [X1:$i] : nat))@(^ [X1:$i] : (d_and@(n_is@(ap@X1@n_1)@(ordsucc@X0))@(n_all@(^ [X2:$i] : (n_is@(ap@X1@(ordsucc@X2))@(ordsucc@(ap@X1@X2)))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_satz4'), [hashroot('822887f7243053921a6bc21e06b95961df50378446d355458e1cb57300ef3b8e')]).
thf(typ_plus,type, (plus : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_plus'), [hashroot('580a77e177531ab80b102706867051a3d5a1043cf10804cd23c161a34c9ceb42')]).
thf(def_plus,definition, (plus = (^ [X0:$i] : (ind@(d_Pi@nat@(^ [X1:$i] : nat))@(d_24_prop2@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_plus')).
thf(typ_n_pl,type, (n_pl : ($i>$i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_pl'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8281_xpy')], [hashroot('e4ce1a2c9ee7a5e3cbf1009f69f89d52acccf8f30c330687447e7ec121e60f7a')]).
thf(def_n_pl,definition, (n_pl = (^ [X0:$i] : (ap@(plus@X0)))), [file('grundlagen.aut','l_e_st_eq_landau_n_pl'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8281_xpy')]).
thf(satz4e,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (n_is@(ordsucc@X0)@(n_pl@X0@n_1)))), file('grundlagen.aut','l_e_st_eq_landau_n_satz4e'), [hashroot('46b3df80532f81adaf89255935a261f33e5a9f800002a4d0083a7be217a92e1a')]).
thf(typ_iii,type, (iii : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_29_iii'),file('grundlagen.aut','l_e_st_eq_landau_n_less')], [hashroot('6ba9e7ba6008214c808ec0406c3dfdbb0319337c703d55a467d2b6caa77cc18b')]).
thf(typ_lessis,type, (lessis : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_lessis'), [hashroot('6543a1825fc715c37a10a00b8bfcee01bcd1c22f9a984a5a3a79250af7b6563d')]).
thf(def_lessis,definition, (lessis = (^ [X0:$i] : ^ [X1:$i] : (l_or@(iii@X0@X1)@(n_is@X0@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_lessis')).
thf(satz24a,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(lessis@n_1)), file('grundlagen.aut','l_e_st_eq_landau_n_satz24a'), [hashroot('b60c307a7d9fdfc7409f3b5f6c3d6193191c76d87116092dcafe52afd89097f8')]).
thf(typ_d_1to,type, (d_1to : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_1to'), [hashroot('5c820bd8f0eeb404ee39eb850016f1c97e4f2665853b2d7d356903656fae6e72')]).
thf(def_d_1to,definition, (d_1to = (^ [X0:$i] : (d_Sep@nat@(^ [X1:$i] : (lessis@X1@X0))))), file('grundlagen.aut','l_e_st_eq_landau_n_1to')).
thf(typ_outn,type, (outn : ($i>$i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_outn'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_ux')], [hashroot('1bd86067524e5899ba3e9b7596ea4fa4a173ec3c4855ac8a7d64a8ff5acc4c37')]).
thf(def_outn,definition, (outn = (^ [X0:$i] : (out@nat@(^ [X1:$i] : (lessis@X1@X0))))), [file('grundlagen.aut','l_e_st_eq_landau_n_outn'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_ux')]).
thf(typ_inn,type, (inn : ($i>$i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_inn'),file('grundlagen.aut','l_e_st_eq_landau_n_left_ui'),file('grundlagen.aut','l_e_st_eq_landau_n_right_ui'),file('grundlagen.aut','l_e_st_eq_landau_n_8274_u1'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_inn'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8281_n0'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8281_m0')], [hashroot('298681c7793320532b25c2ded15aea88210cc18b8b62bb33b3f6831d35a8c33f')]).
thf(def_inn,definition, (inn = (^ [X0:$i] : (e_in@nat@(^ [X1:$i] : (lessis@X1@X0))))), [file('grundlagen.aut','l_e_st_eq_landau_n_inn'),file('grundlagen.aut','l_e_st_eq_landau_n_left_ui'),file('grundlagen.aut','l_e_st_eq_landau_n_right_ui'),file('grundlagen.aut','l_e_st_eq_landau_n_8274_u1'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_inn'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8281_n0'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8281_m0')]).
thf(typ_n_2,type, (n_2 : $i), file('grundlagen.aut','l_e_st_eq_landau_n_2'), [hashroot('62d319ca7c76a076342ae966a3df30d40b380b95146562a06a30fd3a83f4be64')]).
thf(def_n_2,definition, (n_2 = (n_pl@n_1@n_1)), file('grundlagen.aut','l_e_st_eq_landau_n_2')).
thf(typ_n_1t,type, (n_1t : $i), file('grundlagen.aut','l_e_st_eq_landau_n_1t'), [hashroot('cda30255aee1fd05f381ce67f0a952e3c4cfac27072e166c540eff32e7008fd0')]).
thf(def_n_1t,definition, (n_1t = (outn@n_2@n_1)), file('grundlagen.aut','l_e_st_eq_landau_n_1t')).
thf(typ_n_2t,type, (n_2t : $i), file('grundlagen.aut','l_e_st_eq_landau_n_2t'), [hashroot('7003370d08931f1c86f38ddd92a3681cfa8a995c7c509291cc9fe2383e1b2fc9')]).
thf(def_n_2t,definition, (n_2t = (outn@n_2@n_2)), file('grundlagen.aut','l_e_st_eq_landau_n_2t')).
thf(typ_pair_u0,type, (pair_u0 : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_pair_u0'), [hashroot('4278d00d96b3d858d5986a9c128720e529a1d20c204fdf5d12bbd715b0496020')]).
thf(def_pair_u0,definition, (pair_u0 = (inn@n_2)), file('grundlagen.aut','l_e_st_eq_landau_n_pair_u0')).
thf(typ_pair1type,type, (pair1type : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_pair1type'), [hashroot('983ce931fc2d14d27b2bd13973f5096d519ffd6adc503122f810a8632c69dac6')]).
thf(def_pair1type,definition, (pair1type = (^ [X0:$i] : (d_Pi@(d_1to@n_2)@(^ [X1:$i] : X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_pair1type')).
thf(typ_pair1,type, (pair1 : ($i>$i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_pair1'), [hashroot('22c11c2853d1e588f3ac8516f0fabefcfeb723777f2646245247cb58ebcc0845')]).
thf(def_pair1,definition, (pair1 = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : (d_Sigma@(d_1to@n_2)@(^ [X3:$i] : (ite@(e_is@(d_1to@n_2)@X3@n_1t)@X0@X1@X2))))), file('grundlagen.aut','l_e_st_eq_landau_n_pair1')).
thf(typ_first1,type, (first1 : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_first1'), [hashroot('c570239a491de7b60c8947a59ca974f5d9b42673aad7ffc178316431f953e031')]).
thf(def_first1,definition, (first1 = (^ [X0:$i] : ^ [X1:$i] : (ap@X1@n_1t))), file('grundlagen.aut','l_e_st_eq_landau_n_first1')).
thf(typ_second1,type, (second1 : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_second1'), [hashroot('dc95131b0703782c93e3d49db6e54d235733c59c9379547d981a39250b8dc2c5')]).
thf(def_second1,definition, (second1 = (^ [X0:$i] : ^ [X1:$i] : (ap@X1@n_2t))), file('grundlagen.aut','l_e_st_eq_landau_n_second1')).
thf(typ_rat,type, (rat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rat'), [hashroot('e034fba97d474a28e571a50e33c2074cc52e74b2bfd0b39f8238729be437398c')]).
thf(typ_cutprop,type, (cutprop : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cutprop'), [hashroot('bd043cfe0c73a400f837f25be116d532b89eb0188641751038f44c008e0a9684')]).
thf(typ_cut,type, (cut : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cut'), [hashroot('4e4ed30803b18f94089b782cbb02cbec09b7993c5cdb0300fa191d3e3af33344')]).
thf(def_cut,definition, (cut = (d_Sep@(power@rat)@cutprop)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cut')).
thf(typ_rp_is,type, (rp_is : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_is'), [hashroot('d8b675a0120cea29d5c6b85ee93e20272a34d7cbbfb7974311f160661823a44a')]).
thf(def_rp_is,definition, (rp_is = (e_is@cut)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_is')).
thf(typ_cutof,type, (cutof : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_cutof'), [hashroot('c02c32fa110194a3876f0caf26abeb006979954ef27677c740e0e3cadd919f02')]).
thf(def_cutof,definition, (cutof = (out@(power@rat)@cutprop)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_cutof')).
thf(typ_sumprop,type, (sumprop : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_sumprop'), [hashroot('c6db938089ba98656224de008e77bd466cb030052f29e6f5d6425de652cf591f')]).
thf(typ_sum,type, (sum : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_sum'), [hashroot('ba05f90acc619e884afb4737d1d5cf3117c3a7e34f49c59631c9b778bdf0c015')]).
thf(def_sum,definition, (sum = (^ [X0:$i] : ^ [X1:$i] : (d_Sep@rat@(sumprop@X0@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_sum')).
thf(satz129,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (cutprop@(sum@X0@X1)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz129'), [hashroot('dd2d7bb6a1ca464a1ff2993e2a28d5ce63e54f62725cf526a276b76ae038314e')]).
thf(typ_rp_pl,type, (rp_pl : ($i>$i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_pl'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_iv5d_rps'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ivad_rps')], [hashroot('18d85d3ba788b8521053f32e684da7fc8a8f6133301d4cc395e3732c6060e82c')]).
thf(def_rp_pl,definition, (rp_pl = (^ [X0:$i] : ^ [X1:$i] : (cutof@(sum@X0@X1)))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_pl'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_iv5d_rps'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ivad_rps')]).
thf(satz130,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (rp_is@(rp_pl@X0@X1)@(rp_pl@X1@X0)))))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz130'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_compl')], [hashroot('3eacdc733c4d51695e64ae185f69e92a8cb1db88f6e60ec096bea66fa6761665')]).
thf(satz131,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@cut))@(^ [X2:$i] : (rp_is@(rp_pl@(rp_pl@X0@X1)@X2)@(rp_pl@X0@(rp_pl@X1@X2))))))))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz131'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_asspl1')], [hashroot('2ed6432029d193d98f2bc95dd56bfa9c62531c274d2cdb6cb9a0620f5dfc1e16')]).
thf(rp_ispl1,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@cut))@(^ [X2:$i] : ((rp_is@X0@X1) => (rp_is@(rp_pl@X0@X2)@(rp_pl@X1@X2))))))))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ispl1'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz135b')], [hashroot('2a479c958b3c7723c86142f70efdf5c04d7cbe58a7ed5d8b634cf00de780c6bb')]).
thf(rp_ispl2,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@cut))@(^ [X2:$i] : ((rp_is@X0@X1) => (rp_is@(rp_pl@X2@X0)@(rp_pl@X2@X1))))))))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ispl2'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz135e')], [hashroot('6c0ecd0f342b9c2e542511199a0c7594f47f27d130d3a148e531d27f20ec1c62')]).
thf(satz136b,axiom, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@cut))@(^ [X2:$i] : ((rp_is@(rp_pl@X0@X2)@(rp_pl@X1@X2)) => (rp_is@X0@X1)))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satz136b'), [hashroot('b88a76b16c9e4dc1ab5bf45e2bf9da8f107806a8fbb9185937d87f7b2f627837')]).
thf(typ_dif,type, (dif : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_dif'), [hashroot('cec68cedcff46da8ae374b12e2425580a20cf5fde1ffb9197afff9371c1256e5')]).
thf(def_dif,definition, (dif = (pair1type@cut)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_dif')).
thf(typ_rp_df,type, (rp_df : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_df'), [hashroot('ecccc01497ad9f28d18ca2a6d3717c0764a0e070ad7428f427db5dc72aed09e7')]).
thf(def_rp_df,definition, (rp_df = (pair1@cut)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_df')).
thf(typ_stm,type, (stm : ($i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_stm'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_1a'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_1b'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_1c'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_1d')], [hashroot('2a2189a53222d2c8f1a9eceace529b57e36e644d2c64e6eb4acba2764929172f')]).
thf(def_stm,definition, (stm = (first1@cut)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_stm'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_1a'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_1b'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_1c'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_1d')]).
thf(typ_std,type, (std : ($i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_std'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2a'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2b'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2c'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2d')], [hashroot('362d65e96a7cad4d6c9d9997c2ef18c12945d4fa1090d9535c57b04157d01db2')]).
thf(def_std,definition, (std = (second1@cut)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_std'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2a'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2b'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2c'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2d')]).
thf(typ_rp_eq,type, (rp_eq : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_eq'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_eq')], [hashroot('ca6772f54f9c50dc242e006a36cac737aac5ef964cafef0b5f68a61c5c5b2053')]).
thf(def_rp_eq,definition, (rp_eq = (^ [X0:$i] : ^ [X1:$i] : (rp_is@(rp_pl@(stm@X0)@(std@X1))@(rp_pl@(stm@X1)@(std@X0))))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_eq'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_eq')]).
thf(typ_m0d,type, (m0d : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_m0d'), [hashroot('08d38437716d71ca8191256e38f5acb3f6d76347c9ce85afcc3f64270b4d8f5a')]).
thf(def_m0d,definition, (m0d = (^ [X0:$i] : (rp_df@(std@X0)@(stm@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_m0d')).
thf(typ_r_inn,type, (r_inn : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_inn'), [hashroot('e84a53eecd52752788783335ce4dbede5661a8c225e4fb863ac190a88c9ec490')]).
thf(def_r_inn,definition, (r_inn = (esti@dif)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_inn')).
thf(typ_real,type, (real : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_real'), [hashroot('d2db906daf81bb10db98f39130dcf91ed77efdd52a7c588d62fcf5700a867d0d')]).
thf(def_real,definition, (real = (ect@dif@rp_eq)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_real')).
thf(typ_r_is,type, (r_is : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_is'), [hashroot('118ce08c1ad9511b2d7e90507a848a8dd6d738336d22639ae40514ff1af3c31e')]).
thf(def_r_is,definition, (r_is = (e_is@real)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_is')).
thf(typ_realof,type, (realof : ($i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_realof'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_3r184_ra'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_3r184_rb'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_4r204_ar'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_7r161_ar')], [hashroot('e76be2e75bda768f84c9bf50bd8aa4bd786faf992442d977f1546e3a602b8dfd')]).
thf(def_realof,definition, (realof = (ectelt@dif@rp_eq)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_realof'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_3r184_ra'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_3r184_rb'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_4r204_ar'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_7r161_ar')]).
thf(typ_r_class,type, (r_class : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_class'), [hashroot('dde5a9ef4b51914958df59fa64a0bff56aa542365088caeda3178f9fdf261f83')]).
thf(def_r_class,definition, (r_class = (ecect@dif@rp_eq)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_class')).
thf(typ_r_fixf,type, (r_fixf : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_fixf'), [hashroot('0c6ebb98eac4314cfa1d6b2fb4e739f440e9fa1af077dec961dba359ea4971bb')]).
thf(def_r_fixf,definition, (r_fixf = (fixfu@dif@rp_eq)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_fixf')).
thf(typ_indreal,type, (indreal : ($i>$i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_indreal'), [hashroot('6b1b6b464d0b8457d2d6a540c94b2295a40086b6ad58b5b2c93bba9394e3bd56')]).
thf(def_indreal,definition, (indreal = (indeq@dif@rp_eq)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_indreal')).
thf(typ_r_0,type, (r_0 : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_0'), [hashroot('42c693b88c18ad0ee09158472407fcc7470030c98604483e31c59794f55d1b2c')]).
thf(typ_m0dr,type, (m0dr : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_m0dr'), [hashroot('7b92e60462d5ab924fee35dfb1a1d380d27e01607fb2ce169323770c028f46a6')]).
thf(def_m0dr,definition, (m0dr = (d_Sigma@dif@(^ [X0:$i] : (realof@(m0d@X0))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_m0dr')).
thf(typ_r_m0,type, (r_m0 : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_m0'), [hashroot('fe74a8bd0f960a753b620725606a10d45c666a4115e7fdefaa6be7b4bd6e800c')]).
thf(def_r_m0,definition, (r_m0 = (indreal@real@m0dr)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_m0')).
thf(typ_complex,type, (complex : $i), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_complex'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_cx')], [hashroot('4bb7039770d4387b91af354ed87100d8f3f5905516b480cc18ed3d1800fa7db1')]).
thf(def_complex,definition, (complex = (pair1type@real)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_complex'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_cx')]).
thf(typ_c_is,type, (c_is : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_is'), [hashroot('2942f9a2f85def39885d3c7419f850963ebb1119d90e91f3ab52a37e2e3f1307')]).
thf(def_c_is,definition, (c_is = (e_is@complex)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_is')).
thf(typ_pli,type, (pli : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_pli'), [hashroot('42e7abfbe15016af36b5390050d8f7d13d8c3ddb539bdedafc739cd282d98990')]).
thf(def_pli,definition, (pli = (pair1@real)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_pli')).
thf(typ_c_re,type, (c_re : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_re'), [hashroot('f58bb03aa9423c197f7061fb40228142cda4c2d09180a5e9affaf0ce3b33139a')]).
thf(def_c_re,definition, (c_re = (first1@real)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_re')).
thf(typ_c_im,type, (c_im : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_im'), [hashroot('6825dc26de02c6c24fffa57aaf7cfe8afab7e43e985540af909a6804538df5c1')]).
thf(def_c_im,definition, (c_im = (second1@real)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_im')).
thf(typ_c_conj,type, (c_conj : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_conj'), [hashroot('d3af2e38aac93af66d3f3183ae14931993bc5e75844d952b5cc1dc5bb0efdffc')]).
thf(def_c_conj,definition, (c_conj = (^ [X0:$i] : (pli@(c_re@X0)@(r_m0@(c_im@X0))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_conj')).
thf(satz259b,axiom, (all_of@(^ [X0:$i] : (in@X0@complex))@(^ [X0:$i] : ((r_is@(c_im@X0)@r_0) => (c_is@(c_conj@X0)@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_satz259b'), [hashroot('41dbe7645f02d612fe406124f1f1b530612920ffa196bb105d52c24b53c6cea4')]).
thf(satz269d,conjecture, (all_of@(^ [X0:$i] : (in@X0@complex))@(^ [X0:$i] : ((r_is@(c_im@X0)@r_0) => (c_is@X0@(c_conj@X0))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_satz269d'), [hashroot('2f03f80da6169d7969d73dd6e3f34d1222d05363b989d64b8863d94ddfacc2b2')]).
