thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_d_Sep,type, (d_Sep : ($i>($i>$o)>$i)), [file('grundlagen.aut','l_e_ot'),file('grundlagen.aut','l_e_st_setof')], [hashroot('218a016b04c605cddd8baabd01b9102f19cbd21e943087fb26bfbb5c1990d944')]).
thf(typ_ordsucc,type, (ordsucc : ($i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_suc'),file('grundlagen.aut','l_e_st_eq_landau_n_8274_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8276_xs')], [hashroot('6f2bd357b24438c387f3433d2366b843d110c03ef0b8bd82c5a0cb92bfc5b50d')]).
thf(typ_d_Sigma,type, (d_Sigma : ($i>($i>$i)>$i)), unknown, [hashroot('75c40645d5fe236c1ca11856fa4f32b0ebb4c95c94a9615ec27513c1c1b6b5d3')]).
thf(typ_ap,type, (ap : ($i>$i>$i)), unknown, [hashroot('a7decc6ab7cd5672b0a906b215a444cd9aa45edcb0967cd0c9d40e207f8df623')]).
thf(typ_d_Pi,type, (d_Pi : ($i>($i>$i)>$i)), unknown, [hashroot('68d9661a0d30c2de45df1cf7708fadc581b6b52cb2ba22f5f89c21527ade885f')]).
thf(lam_Pi,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X0) => (in@(X2@X3)@(X1@X3)))) => (in@(d_Sigma@X0@X2)@(d_Pi@X0@X1)))), unknown, [hashroot('0188856999da4593b9c9db87d02370f335d61bff2c902d2a1a4414a301b486fb')]).
thf(ap_Pi,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ! [X3:$i] : ((in@X2@(d_Pi@X0@X1)) => ((in@X3@X0) => (in@(ap@X2@X3)@(X1@X3))))), unknown, [hashroot('273853e08fc3f975cbaf03343d417034c249ee5074275a7321273698d3bc1b72')]).
thf(typ_imp,type, (imp : ($o>$o>$o)), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')], [hashroot('c471f7f9b55e8fce15c5b7f92a344808917c7bdfbac8043315ef5a52fb233e79')]).
thf(def_imp,definition, (imp = (^ [X0:$o] : ^ [X1:$o] : (X0 => X1))), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')]).
thf(typ_d_not,type, (d_not : ($o>$o)), file('grundlagen.aut','l_not'), [hashroot('122455a7450b0bbfcd7daa48af7a7a4bf320a8f085b235e0bfe56f0a1de2967f')]).
thf(def_d_not,definition, (d_not = (^ [X0:$o] : (imp@X0@$false))), file('grundlagen.aut','l_not')).
thf(typ_wel,type, (wel : ($o>$o)), file('grundlagen.aut','l_wel'), [hashroot('3c7461ae99cc5fcbcf4c6eb8756a1630dde3bca516b9da8a16686247350ecfaf')]).
thf(def_wel,definition, (wel = (^ [X0:$o] : (d_not@(d_not@X0)))), file('grundlagen.aut','l_wel')).
thf(l_et,axiom, (! [X0:$o] : ((wel@X0) => X0)), file('grundlagen.aut','l_et'), [hashroot('68f4fbe1a4cdb31228d4806333cc9f14319ec737076e00d4ccc0adf1acc3752f')]).
thf(typ_l_ec,type, (l_ec : ($o>$o>$o)), file('grundlagen.aut','l_ec'), [hashroot('f505a8e15aa62b7fbc5d6fc63dc5c3c8e06aade6488051165390a10e2e57ad6e')]).
thf(def_l_ec,definition, (l_ec = (^ [X0:$o] : ^ [X1:$o] : (imp@X0@(d_not@X1)))), file('grundlagen.aut','l_ec')).
thf(typ_d_and,type, (d_and : ($o>$o>$o)), file('grundlagen.aut','l_and'), [hashroot('256969ce0d10af13735c022198c8269b31ef9b2f35171d9ad3e422d566773b0f')]).
thf(def_d_and,definition, (d_and = (^ [X0:$o] : ^ [X1:$o] : (d_not@(l_ec@X0@X1)))), file('grundlagen.aut','l_and')).
thf(typ_l_or,type, (l_or : ($o>$o>$o)), file('grundlagen.aut','l_or'), [hashroot('d609be23d396e7ff7e091175cfb557c25e3ced2809a41de7e4d98fc9524ed072')]).
thf(def_l_or,definition, (l_or = (^ [X0:$o] : (imp@(d_not@X0)))), file('grundlagen.aut','l_or')).
thf(typ_all,type, (all : ($i>($i>$o)>$o)), file('grundlagen.aut','l_all'), [hashroot('7af775405bb712aec9ce7cdde103343acfd567932781aa9cedcee8d57f9a249d')]).
thf(def_all,definition, (all = (^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@X0))))), file('grundlagen.aut','l_all')).
thf(typ_non,type, (non : ($i>($i>$o)>$i>$o)), file('grundlagen.aut','l_non'), [hashroot('e0c99b0aabd21f260538f4a9cc7b2f9dd1c1be40c3fd3c26d5355897e80de94b')]).
thf(def_non,definition, (non = (^ [X0:$i] : ^ [X1:($i>$o)] : ^ [X2:$i] : (d_not@(X1@X2)))), file('grundlagen.aut','l_non')).
thf(typ_l_some,type, (l_some : ($i>($i>$o)>$o)), file('grundlagen.aut','l_some'), [hashroot('e725afc4936069e5e8c753118fc6844ba6a33e0b7ed57300c90a0659591806c9')]).
thf(def_l_some,definition, (l_some = (^ [X0:$i] : ^ [X1:($i>$o)] : (d_not@(all_of@(^ [X2:$i] : (in@X2@X0))@(non@X0@X1))))), file('grundlagen.aut','l_some')).
thf(typ_e_is,type, (e_is : ($i>$i>$i>$o)), [file('grundlagen.aut','l_e_is'),file('grundlagen.aut','l_r_ite_is')], [hashroot('8b0ca0dd413f254c6ad2f3c70f60a1023635d6ff3be83f7d4f2b7bf8ee537789')]).
thf(refis,axiom, (! [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@X0))@(^ [X1:$i] : (e_is@X0@X1@X1)))), file('grundlagen.aut','l_e_refis'), [hashroot('1bf646edae380151a0e2748e01eea959b3432fa03a8c3f343db0b2c15e87968a')]).
thf(e_isp,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : ((X1@X2) => ((e_is@X0@X2@X3) => (X1@X3)))))))), file('grundlagen.aut','l_e_isp'), [hashroot('6692832b0345b302e12f939fd8026269a93e31fbada95e487002d5ce57d7133a')]).
thf(typ_amone,type, (amone : ($i>($i>$o)>$o)), file('grundlagen.aut','l_e_amone'), [hashroot('abf9f4c92914eaea25fda61ade255847fa59a23ec25d76f12c98c471d2ae0d1c')]).
thf(def_amone,definition, (amone = (^ [X0:$i] : ^ [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : ((X1@X2) => ((X1@X3) => (e_is@X0@X2@X3))))))))), file('grundlagen.aut','l_e_amone')).
thf(typ_one,type, (one : ($i>($i>$o)>$o)), file('grundlagen.aut','l_e_one'), [hashroot('7091b9e752a98fbbb417ba534750e8d45ae4a3f689bed160c8f19a85db223071')]).
thf(def_one,definition, (one = (^ [X0:$i] : ^ [X1:($i>$o)] : (d_and@(amone@X0@X1)@(l_some@X0@X1)))), file('grundlagen.aut','l_e_one')).
thf(typ_ind,type, (ind : ($i>($i>$o)>$i)), file('grundlagen.aut','l_e_ind'), [hashroot('05a3a837678d78ea927e34ab8f1520d9ffa741c69b24fddb944e1a6b7c6ce154')]).
thf(ind_p,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((one@X0@X1) => (is_of@(ind@X0@X1)@(^ [X2:$i] : (in@X2@X0))))), file('grundlagen.aut','l_e_ind_p'), [hashroot('cb52cb4b70c1b6f750bb76194f40fe20558dd8ffe5dc04377803016edc6e440c')]).
thf(typ_injective,type, (injective : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_injective'), [hashroot('be5cf2b1b2ea79cef9c3ff78402a1be4b403b907b932d6425bb1967b37b53b61')]).
thf(def_injective,definition, (injective = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : (all@X0@(^ [X3:$i] : (all@X0@(^ [X4:$i] : (imp@(e_is@X1@(ap@X2@X3)@(ap@X2@X4))@(e_is@X0@X3@X4)))))))), file('grundlagen.aut','l_e_injective')).
thf(typ_image,type, (image : ($i>$i>$i>$i>$o)), file('grundlagen.aut','l_e_image'), [hashroot('64b250c443ba876e87705ae8c09e08f7ec69e63155f1c02e4adb9668ada8fce1')]).
thf(def_image,definition, (image = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : ^ [X3:$i] : (l_some@X0@(^ [X4:$i] : (e_is@X1@X3@(ap@X2@X4)))))), file('grundlagen.aut','l_e_image')).
thf(typ_tofs,type, (tofs : ($i>$i>$i>$i>$i)), [file('grundlagen.aut','l_e_tofs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_us')], [hashroot('d71dfa49596b38f4345149095b9e30951f7515ea54184386cb1c06dfc05b5408')]).
thf(def_tofs,definition, (tofs = (^ [X0:$i] : ^ [X1:$i] : ap)), [file('grundlagen.aut','l_e_tofs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_us')]).
thf(typ_soft,type, (soft : ($i>$i>$i>$i>$i)), [file('grundlagen.aut','l_e_soft'),file('grundlagen.aut','l_e_inj_so')], [hashroot('19640c3bc3eb360c29926376c3a20cba044317026fb0e5a810f33eee58bbdb28')]).
thf(def_soft,definition, (soft = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : ^ [X3:$i] : (ind@X0@(^ [X4:$i] : (e_is@X1@X3@(ap@X2@X4)))))), [file('grundlagen.aut','l_e_soft'),file('grundlagen.aut','l_e_inj_so')]).
thf(typ_e_in,type, (e_in : ($i>($i>$o)>$i>$i)), file('grundlagen.aut','l_e_in'), [hashroot('40a8d54e1c36f987e5c14c936a463272f0170c12f87e4577d32401d4d2954a6b')]).
thf(e_in_p,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@(d_Sep@X0@X1)))@(^ [X2:$i] : (is_of@(e_in@X0@X1@X2)@(^ [X3:$i] : (in@X3@X0)))))), file('grundlagen.aut','l_e_in_p'), [hashroot('3fd5ddc2bee1c842fb9f7164e355a4663d6b4b4cd7e9500cb535f40620a33aab')]).
thf(otax1,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (injective@(d_Sep@X0@X1)@X0@(d_Sigma@(d_Sep@X0@X1)@(e_in@X0@X1)))), file('grundlagen.aut','l_e_otax1'), [hashroot('52a1928b72e190169f28e1a70471dc523a9b0647cbe0051e850d2f911e122b4a')]).
thf(otax2,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : ((X1@X2) => (image@(d_Sep@X0@X1)@X0@(d_Sigma@(d_Sep@X0@X1)@(e_in@X0@X1))@X2))))), file('grundlagen.aut','l_e_otax2'), [hashroot('852baa2053ca1af6d4303ba531f26098375c757c6d962baf3c7f09638c3ba0aa')]).
thf(typ_out,type, (out : ($i>($i>$o)>$i>$i)), file('grundlagen.aut','l_e_out'), [hashroot('6fc658a37010bc78301a6074982c0339667b8258b5cd6365a3524f5d846fd607')]).
thf(def_out,definition, (out = (^ [X0:$i] : ^ [X1:($i>$o)] : (soft@(d_Sep@X0@X1)@X0@(d_Sigma@(d_Sep@X0@X1)@(e_in@X0@X1))))), file('grundlagen.aut','l_e_out')).
thf(typ_prop1,type, (prop1 : ($o>$i>$i>$i>$i>$o)), [file('grundlagen.aut','l_e_ite_prop1'),file('grundlagen.aut','l_r_ite_prop1')], [hashroot('127f3a67f12abba06108ee16227ccf0156cfa065b5ce338a87dc4a1a04f1e7b0')]).
thf(def_prop1,definition, (prop1 = (^ [X0:$o] : ^ [X1:$i] : ^ [X2:$i] : ^ [X3:$i] : ^ [X4:$i] : (d_and@(imp@X0@(e_is@X1@X4@X2))@(imp@(d_not@X0)@(e_is@X1@X4@X3))))), [file('grundlagen.aut','l_e_ite_prop1'),file('grundlagen.aut','l_r_ite_prop1')]).
thf(typ_ite,type, (ite : ($o>$i>$i>$i>$i)), [file('grundlagen.aut','l_e_ite'),file('grundlagen.aut','l_r_ite')], [hashroot('0d0bd40dc6689040d63b44d98bb841db103214506f7ad3b713d9a03fd773652b')]).
thf(def_ite,definition, (ite = (^ [X0:$o] : ^ [X1:$i] : ^ [X2:$i] : ^ [X3:$i] : (ind@X1@(prop1@X0@X1@X2@X3)))), [file('grundlagen.aut','l_e_ite'),file('grundlagen.aut','l_r_ite')]).
thf(typ_nat,type, (nat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_nat'), [hashroot('a70cf6792dc8538ebedafac6b0b6924bbcece48e47a0c0cb77b73b3cb5eba62f')]).
thf(typ_n_is,type, (n_is : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_is'),file('grundlagen.aut','l_e_st_eq_landau_n_29_i')], [hashroot('f438ef4d45e17e52a3513bcba965b0f71de0b41c1f2de432a49525837a2585a6')]).
thf(def_n_is,definition, (n_is = (e_is@nat)), [file('grundlagen.aut','l_e_st_eq_landau_n_is'),file('grundlagen.aut','l_e_st_eq_landau_n_29_i')]).
thf(typ_n_all,type, (n_all : (($i>$o)>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_all'), [hashroot('e66de03c63adc714a3c556fbbe3180d9d72c12de6daae2b0e3b32fb35350ccc0')]).
thf(typ_n_1,type, (n_1 : $i), file('grundlagen.aut','l_e_st_eq_landau_n_1'), [hashroot('72510cdcd4be46383fe2950c32184464e04a3a48f2bbb6fb2e0e938a74c551d6')]).
thf(n_1_p,axiom, (is_of@n_1@(^ [X0:$i] : (in@X0@nat))), file('grundlagen.aut','l_e_st_eq_landau_n_1_p'), [hashroot('7b99e30acc4d1ee1179e423e7c0c1002b7193b2b58635a27a801525bd3ab6927')]).
thf(typ_d_24_prop1,type, (d_24_prop1 : ($i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_24_prop1'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_nt_54_prop1')], [hashroot('b52a03caf6f61ea7b4d7a53c556ef494863fed3c437f64b1390fed5afc3ddc7c')]).
thf(def_d_24_prop1,definition, (d_24_prop1 = (^ [X0:$i] : (n_all@(^ [X1:$i] : (n_is@(ap@X0@(ordsucc@X1))@(ordsucc@(ap@X0@X1))))))), [file('grundlagen.aut','l_e_st_eq_landau_n_24_prop1'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_nt_54_prop1')]).
thf(typ_d_24_prop2,type, (d_24_prop2 : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_24_prop2'), [hashroot('ce45aa38af51cc5b5f1a1d5eaf92d8c233ee8e401f2933477e149047fb282785')]).
thf(def_d_24_prop2,definition, (d_24_prop2 = (^ [X0:$i] : ^ [X1:$i] : (d_and@(n_is@(ap@X1@n_1)@(ordsucc@X0))@(d_24_prop1@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_24_prop2')).
thf(satz4,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (one@(d_Pi@nat@(^ [X1:$i] : nat))@(^ [X1:$i] : (d_and@(n_is@(ap@X1@n_1)@(ordsucc@X0))@(n_all@(^ [X2:$i] : (n_is@(ap@X1@(ordsucc@X2))@(ordsucc@(ap@X1@X2)))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_satz4'), [hashroot('822887f7243053921a6bc21e06b95961df50378446d355458e1cb57300ef3b8e')]).
thf(typ_plus,type, (plus : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_plus'), [hashroot('580a77e177531ab80b102706867051a3d5a1043cf10804cd23c161a34c9ceb42')]).
thf(def_plus,definition, (plus = (^ [X0:$i] : (ind@(d_Pi@nat@(^ [X1:$i] : nat))@(d_24_prop2@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_plus')).
thf(typ_n_pl,type, (n_pl : ($i>$i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_pl'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8281_xpy')], [hashroot('e4ce1a2c9ee7a5e3cbf1009f69f89d52acccf8f30c330687447e7ec121e60f7a')]).
thf(def_n_pl,definition, (n_pl = (^ [X0:$i] : (ap@(plus@X0)))), [file('grundlagen.aut','l_e_st_eq_landau_n_pl'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8281_xpy')]).
thf(typ_iii,type, (iii : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_29_iii'),file('grundlagen.aut','l_e_st_eq_landau_n_less')], [hashroot('6ba9e7ba6008214c808ec0406c3dfdbb0319337c703d55a467d2b6caa77cc18b')]).
thf(typ_lessis,type, (lessis : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_lessis'), [hashroot('6543a1825fc715c37a10a00b8bfcee01bcd1c22f9a984a5a3a79250af7b6563d')]).
thf(def_lessis,definition, (lessis = (^ [X0:$i] : ^ [X1:$i] : (l_or@(iii@X0@X1)@(n_is@X0@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_lessis')).
thf(satz24a,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(lessis@n_1)), file('grundlagen.aut','l_e_st_eq_landau_n_satz24a'), [hashroot('b60c307a7d9fdfc7409f3b5f6c3d6193191c76d87116092dcafe52afd89097f8')]).
thf(typ_d_428_prop1,type, (d_428_prop1 : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_428_prop1'), [hashroot('0720040b7a304b57cc7ca1fd117b0adfa32487f594f247b9c0df732820ac7d4f')]).
thf(def_d_428_prop1,definition, (d_428_prop1 = (^ [X0:$i] : ^ [X1:$i] : (n_all@(^ [X2:$i] : (n_is@(ap@X1@(ordsucc@X2))@(n_pl@(ap@X1@X2)@X0)))))), file('grundlagen.aut','l_e_st_eq_landau_n_428_prop1')).
thf(typ_d_428_prop2,type, (d_428_prop2 : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_428_prop2'), [hashroot('827553f1b0c36a1af105bef47a35924d6aae6284f5b0e069eeb50c5cafa274fe')]).
thf(def_d_428_prop2,definition, (d_428_prop2 = (^ [X0:$i] : ^ [X1:$i] : (d_and@(n_is@(ap@X1@n_1)@X0)@(d_428_prop1@X0@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_428_prop2')).
thf(satz28,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (one@(d_Pi@nat@(^ [X1:$i] : nat))@(^ [X1:$i] : (d_and@(n_is@(ap@X1@n_1)@X0)@(n_all@(^ [X2:$i] : (n_is@(ap@X1@(ordsucc@X2))@(n_pl@(ap@X1@X2)@X0))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_satz28'), [hashroot('eb0977fc9cab6a975a8eee13589209eb4721ae21be8c81c8c1bbac363ed3b73f')]).
thf(typ_times,type, (times : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_times'), [hashroot('099909cb9d3d5db37ff946d98c9ed234b8ce34d87a24d8458a853f9584fb8471')]).
thf(def_times,definition, (times = (^ [X0:$i] : (ind@(d_Pi@nat@(^ [X1:$i] : nat))@(d_428_prop2@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_times')).
thf(typ_n_ts,type, (n_ts : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_ts'), [hashroot('cdb38002ec50032c73ab99baa63a64bd8fb5024e6bbe7f52fec86452b7a91fd9')]).
thf(def_n_ts,definition, (n_ts = (^ [X0:$i] : (ap@(times@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_ts')).
thf(typ_d_1to,type, (d_1to : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_1to'), [hashroot('5c820bd8f0eeb404ee39eb850016f1c97e4f2665853b2d7d356903656fae6e72')]).
thf(def_d_1to,definition, (d_1to = (^ [X0:$i] : (d_Sep@nat@(^ [X1:$i] : (lessis@X1@X0))))), file('grundlagen.aut','l_e_st_eq_landau_n_1to')).
thf(typ_outn,type, (outn : ($i>$i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_outn'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_ux')], [hashroot('1bd86067524e5899ba3e9b7596ea4fa4a173ec3c4855ac8a7d64a8ff5acc4c37')]).
thf(def_outn,definition, (outn = (^ [X0:$i] : (out@nat@(^ [X1:$i] : (lessis@X1@X0))))), [file('grundlagen.aut','l_e_st_eq_landau_n_outn'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_ux')]).
thf(typ_n_2,type, (n_2 : $i), file('grundlagen.aut','l_e_st_eq_landau_n_2'), [hashroot('62d319ca7c76a076342ae966a3df30d40b380b95146562a06a30fd3a83f4be64')]).
thf(def_n_2,definition, (n_2 = (n_pl@n_1@n_1)), file('grundlagen.aut','l_e_st_eq_landau_n_2')).
thf(typ_n_1t,type, (n_1t : $i), file('grundlagen.aut','l_e_st_eq_landau_n_1t'), [hashroot('cda30255aee1fd05f381ce67f0a952e3c4cfac27072e166c540eff32e7008fd0')]).
thf(def_n_1t,definition, (n_1t = (outn@n_2@n_1)), file('grundlagen.aut','l_e_st_eq_landau_n_1t')).
thf(typ_n_2t,type, (n_2t : $i), file('grundlagen.aut','l_e_st_eq_landau_n_2t'), [hashroot('7003370d08931f1c86f38ddd92a3681cfa8a995c7c509291cc9fe2383e1b2fc9')]).
thf(def_n_2t,definition, (n_2t = (outn@n_2@n_2)), file('grundlagen.aut','l_e_st_eq_landau_n_2t')).
thf(typ_pair1type,type, (pair1type : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_pair1type'), [hashroot('983ce931fc2d14d27b2bd13973f5096d519ffd6adc503122f810a8632c69dac6')]).
thf(def_pair1type,definition, (pair1type = (^ [X0:$i] : (d_Pi@(d_1to@n_2)@(^ [X1:$i] : X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_pair1type')).
thf(typ_pair1,type, (pair1 : ($i>$i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_pair1'), [hashroot('22c11c2853d1e588f3ac8516f0fabefcfeb723777f2646245247cb58ebcc0845')]).
thf(def_pair1,definition, (pair1 = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : (d_Sigma@(d_1to@n_2)@(^ [X3:$i] : (ite@(e_is@(d_1to@n_2)@X3@n_1t)@X0@X1@X2))))), file('grundlagen.aut','l_e_st_eq_landau_n_pair1')).
thf(typ_first1,type, (first1 : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_first1'), [hashroot('c570239a491de7b60c8947a59ca974f5d9b42673aad7ffc178316431f953e031')]).
thf(def_first1,definition, (first1 = (^ [X0:$i] : ^ [X1:$i] : (ap@X1@n_1t))), file('grundlagen.aut','l_e_st_eq_landau_n_first1')).
thf(typ_second1,type, (second1 : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_second1'), [hashroot('dc95131b0703782c93e3d49db6e54d235733c59c9379547d981a39250b8dc2c5')]).
thf(def_second1,definition, (second1 = (^ [X0:$i] : ^ [X1:$i] : (ap@X1@n_2t))), file('grundlagen.aut','l_e_st_eq_landau_n_second1')).
thf(typ_frac,type, (frac : $i), file('grundlagen.aut','l_e_st_eq_landau_n_frac'), [hashroot('c222f7117ac203e0edab1ed9e37462fb740b2ea273a724c7102a6cdf50a70879')]).
thf(def_frac,definition, (frac = (pair1type@nat)), file('grundlagen.aut','l_e_st_eq_landau_n_frac')).
thf(typ_n_fr,type, (n_fr : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_fr'), [hashroot('a30b8c74ce8215df04a60b87d123c41da7671199a416dd104649c5ef12dcaaa9')]).
thf(def_n_fr,definition, (n_fr = (pair1@nat)), file('grundlagen.aut','l_e_st_eq_landau_n_fr')).
thf(typ_num,type, (num : ($i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_num'),file('grundlagen.aut','l_e_st_eq_landau_n_1x'),file('grundlagen.aut','l_e_st_eq_landau_n_1y'),file('grundlagen.aut','l_e_st_eq_landau_n_1z'),file('grundlagen.aut','l_e_st_eq_landau_n_1u'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_5115_z')], [hashroot('7e682bb36437569d16c169692bde2d45461074b023c77ed34bc9c1b835f64e88')]).
thf(def_num,definition, (num = (first1@nat)), [file('grundlagen.aut','l_e_st_eq_landau_n_num'),file('grundlagen.aut','l_e_st_eq_landau_n_1x'),file('grundlagen.aut','l_e_st_eq_landau_n_1y'),file('grundlagen.aut','l_e_st_eq_landau_n_1z'),file('grundlagen.aut','l_e_st_eq_landau_n_1u'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_5115_z')]).
thf(typ_den,type, (den : ($i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_den'),file('grundlagen.aut','l_e_st_eq_landau_n_2x'),file('grundlagen.aut','l_e_st_eq_landau_n_2y'),file('grundlagen.aut','l_e_st_eq_landau_n_2z'),file('grundlagen.aut','l_e_st_eq_landau_n_2u'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_5115_v')], [hashroot('ee7af7a6ea637687054ec699d0a7407534e9d88dc420c1e7bc0f269a2a02c812')]).
thf(def_den,definition, (den = (second1@nat)), [file('grundlagen.aut','l_e_st_eq_landau_n_den'),file('grundlagen.aut','l_e_st_eq_landau_n_2x'),file('grundlagen.aut','l_e_st_eq_landau_n_2y'),file('grundlagen.aut','l_e_st_eq_landau_n_2z'),file('grundlagen.aut','l_e_st_eq_landau_n_2u'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_5115_v')]).
thf(typ_n_eq,type, (n_eq : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_eq'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_eq')], [hashroot('e55588efa2d65b223ca727021f7e0aa395778376e99ad9ce041a74b2566176e4')]).
thf(typ_moref,type, (moref : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_moref'), [hashroot('bde92bf0e2da4e14488b138389309844e2ea9aea89d702285924c2b43fbfbb03')]).
thf(satz44,axiom, (all_of@(^ [X0:$i] : (in@X0@frac))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@frac))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@frac))@(^ [X2:$i] : (all_of@(^ [X3:$i] : (in@X3@frac))@(^ [X3:$i] : ((moref@X0@X1) => ((n_eq@X0@X2) => ((n_eq@X1@X3) => (moref@X2@X3)))))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_satz44'), [hashroot('0dc123d3b43907951e92c5843181278f75f4cf9fbb0c83247055a439afa51aa2')]).
thf(typ_n_tf,type, (n_tf : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_tf'), [hashroot('8ab1071bf05c0b85647dcc3bd582cd258578beabcfa788dcb51faa0fbe9ac296')]).
thf(def_n_tf,definition, (n_tf = (^ [X0:$i] : ^ [X1:$i] : (n_fr@(n_ts@(num@X0)@(num@X1))@(n_ts@(den@X0)@(den@X1))))), file('grundlagen.aut','l_e_st_eq_landau_n_tf')).
thf(satz69,axiom, (all_of@(^ [X0:$i] : (in@X0@frac))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@frac))@(^ [X1:$i] : (n_eq@(n_tf@X0@X1)@(n_tf@X1@X0)))))), [file('grundlagen.aut','l_e_st_eq_landau_n_satz69'),file('grundlagen.aut','l_e_st_eq_landau_n_comtf')], [hashroot('52391a0619269e4ae0be441c476ef87e4f0c3293296bee18d49f92b434b69a2c')]).
thf(satz73a,axiom, (all_of@(^ [X0:$i] : (in@X0@frac))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@frac))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@frac))@(^ [X2:$i] : ((moref@(n_tf@X0@X2)@(n_tf@X1@X2)) => (moref@X0@X1)))))))), file('grundlagen.aut','l_e_st_eq_landau_n_satz73a'), [hashroot('460b83112708c32d103bfd70e505a8287d3a22b12b5fd56f4c5ac85b65571e0a')]).
thf(satz73d,conjecture, (all_of@(^ [X0:$i] : (in@X0@frac))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@frac))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@frac))@(^ [X2:$i] : ((moref@(n_tf@X2@X0)@(n_tf@X2@X1)) => (moref@X0@X1)))))))), file('grundlagen.aut','l_e_st_eq_landau_n_satz73d'), [hashroot('aef63ec2eee31ed6b5c9ac760b5c4da9de44ea9e190e9871fbdd72fb4b6bf6bf')]).
