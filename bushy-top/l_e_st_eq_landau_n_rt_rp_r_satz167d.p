thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_imp,type, (imp : ($o>$o>$o)), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')], [hashroot('c471f7f9b55e8fce15c5b7f92a344808917c7bdfbac8043315ef5a52fb233e79')]).
thf(def_imp,definition, (imp = (^ [X0:$o] : ^ [X1:$o] : (X0 => X1))), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')]).
thf(typ_d_not,type, (d_not : ($o>$o)), file('grundlagen.aut','l_not'), [hashroot('122455a7450b0bbfcd7daa48af7a7a4bf320a8f085b235e0bfe56f0a1de2967f')]).
thf(def_d_not,definition, (d_not = (^ [X0:$o] : (imp@X0@$false))), file('grundlagen.aut','l_not')).
thf(typ_wel,type, (wel : ($o>$o)), file('grundlagen.aut','l_wel'), [hashroot('3c7461ae99cc5fcbcf4c6eb8756a1630dde3bca516b9da8a16686247350ecfaf')]).
thf(def_wel,definition, (wel = (^ [X0:$o] : (d_not@(d_not@X0)))), file('grundlagen.aut','l_wel')).
thf(l_et,axiom, (! [X0:$o] : ((wel@X0) => X0)), file('grundlagen.aut','l_et'), [hashroot('68f4fbe1a4cdb31228d4806333cc9f14319ec737076e00d4ccc0adf1acc3752f')]).
thf(typ_l_ec,type, (l_ec : ($o>$o>$o)), file('grundlagen.aut','l_ec'), [hashroot('f505a8e15aa62b7fbc5d6fc63dc5c3c8e06aade6488051165390a10e2e57ad6e')]).
thf(def_l_ec,definition, (l_ec = (^ [X0:$o] : ^ [X1:$o] : (imp@X0@(d_not@X1)))), file('grundlagen.aut','l_ec')).
thf(typ_d_and,type, (d_and : ($o>$o>$o)), file('grundlagen.aut','l_and'), [hashroot('256969ce0d10af13735c022198c8269b31ef9b2f35171d9ad3e422d566773b0f')]).
thf(def_d_and,definition, (d_and = (^ [X0:$o] : ^ [X1:$o] : (d_not@(l_ec@X0@X1)))), file('grundlagen.aut','l_and')).
thf(typ_l_or,type, (l_or : ($o>$o>$o)), file('grundlagen.aut','l_or'), [hashroot('d609be23d396e7ff7e091175cfb557c25e3ced2809a41de7e4d98fc9524ed072')]).
thf(def_l_or,definition, (l_or = (^ [X0:$o] : (imp@(d_not@X0)))), file('grundlagen.aut','l_or')).
thf(typ_and3,type, (and3 : ($o>$o>$o>$o)), file('grundlagen.aut','l_and3'), [hashroot('3ad9a6ed2a01263dc8d7b6f481cba22c1d37fbdc4f619b3fff5f7ba9dad2a256')]).
thf(def_and3,definition, (and3 = (^ [X0:$o] : ^ [X1:$o] : ^ [X2:$o] : (d_and@X0@(d_and@X1@X2)))), file('grundlagen.aut','l_and3')).
thf(typ_ec3,type, (ec3 : ($o>$o>$o>$o)), file('grundlagen.aut','l_ec3'), [hashroot('986599080791a6070d17046968b2c7f73c3d807171c7e5030cc97fd520a4e49c')]).
thf(def_ec3,definition, (ec3 = (^ [X0:$o] : ^ [X1:$o] : ^ [X2:$o] : (and3@(l_ec@X0@X1)@(l_ec@X1@X2)@(l_ec@X2@X0)))), file('grundlagen.aut','l_ec3')).
thf(typ_real,type, (real : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_real'), [hashroot('d2db906daf81bb10db98f39130dcf91ed77efdd52a7c588d62fcf5700a867d0d')]).
thf(typ_r_is,type, (r_is : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_is'), [hashroot('118ce08c1ad9511b2d7e90507a848a8dd6d738336d22639ae40514ff1af3c31e')]).
thf(typ_r_more,type, (r_more : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_more'), [hashroot('02e889e63d339bc3f14b4cc18b59cfadfb10127fd623a8e5541a6f8fc2eb8b55')]).
thf(typ_r_less,type, (r_less : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_less'), [hashroot('82899c761044efd1e6f68b31677f13f557af10881932b504b587a1151ad7b637')]).
thf(satz167b,axiom, (all_of@(^ [X0:$i] : (in@X0@real))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@real))@(^ [X1:$i] : (ec3@(r_is@X0@X1)@(r_more@X0@X1)@(r_less@X0@X1)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_satz167b'), [hashroot('e0d04f596e4ef0d5969d1ddda478f730110617c76691ac16f439504b0ab3a81a')]).
thf(typ_r_lessis,type, (r_lessis : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_lessis'), [hashroot('64e6b7b4374413780cbf4ba7774492107d93e5c168bcb7c6a4c9e58e5cd86bc7')]).
thf(def_r_lessis,definition, (r_lessis = (^ [X0:$i] : ^ [X1:$i] : (l_or@(r_less@X0@X1)@(r_is@X0@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_lessis')).
thf(satz167d,conjecture, (all_of@(^ [X0:$i] : (in@X0@real))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@real))@(^ [X1:$i] : ((r_lessis@X0@X1) => (d_not@(r_more@X0@X1))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_satz167d'), [hashroot('8dd77cb8de21b0932b17177ec10ac7d92440a90b4fa9214847bbcd5a80f9e9fa')]).
