thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_d_Pi,type, (d_Pi : ($i>($i>$i)>$i)), unknown, [hashroot('68d9661a0d30c2de45df1cf7708fadc581b6b52cb2ba22f5f89c21527ade885f')]).
thf(typ_d_not,type, (d_not : ($o>$o)), file('grundlagen.aut','l_not'), [hashroot('122455a7450b0bbfcd7daa48af7a7a4bf320a8f085b235e0bfe56f0a1de2967f')]).
thf(typ_l_some,type, (l_some : ($i>($i>$o)>$o)), file('grundlagen.aut','l_some'), [hashroot('e725afc4936069e5e8c753118fc6844ba6a33e0b7ed57300c90a0659591806c9')]).
thf(some_th4,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((d_not@(l_some@X0@X1)) => (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (d_not@(X1@X2)))))), file('grundlagen.aut','l_some_th4'), [hashroot('7edf6699b7dca75cd7812a65a7922e7026537d073513bd3b9acb706724bc1f9a')]).
thf(typ_bijective,type, (bijective : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_bijective'), [hashroot('77c34396f1eed2094ddb58caccda11863c7b0cf105f3fe3380f3195a5ba38ccd')]).
thf(typ_nat,type, (nat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_nat'), [hashroot('a70cf6792dc8538ebedafac6b0b6924bbcece48e47a0c0cb77b73b3cb5eba62f')]).
thf(typ_iii,type, (iii : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_29_iii'),file('grundlagen.aut','l_e_st_eq_landau_n_less')], [hashroot('6ba9e7ba6008214c808ec0406c3dfdbb0319337c703d55a467d2b6caa77cc18b')]).
thf(typ_d_1to,type, (d_1to : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_1to'), [hashroot('5c820bd8f0eeb404ee39eb850016f1c97e4f2665853b2d7d356903656fae6e72')]).
thf(satz274,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : ((iii@X0@X1) => (d_not@(l_some@(d_Pi@(d_1to@X0)@(^ [X2:$i] : (d_1to@X1)))@(bijective@(d_1to@X0)@(d_1to@X1))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_satz274'), [hashroot('df2380df1dc93778659a2bb1ec54db11568a61eca17bd6b6cb97fb3620783bc2')]).
thf(satz274a,conjecture, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : ((iii@X0@X1) => (all_of@(^ [X2:$i] : (in@X2@(d_Pi@(d_1to@X0)@(^ [X3:$i] : (d_1to@X1)))))@(^ [X2:$i] : (d_not@(bijective@(d_1to@X0)@(d_1to@X1)@X2))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_satz274a'), [hashroot('34246b92ed2a4fab5a67a84563dc9938eecd1fa155c557b528c4f174714214a6')]).
