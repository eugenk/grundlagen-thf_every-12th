thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_power,type, (power : ($i>$i)), file('grundlagen.aut','l_e_st_set'), [hashroot('0c5490ca2f6d61c2d410e7907be97b3bc36b3e4de614e1f5431278dbccad4c79')]).
thf(typ_d_and,type, (d_and : ($o>$o>$o)), file('grundlagen.aut','l_and'), [hashroot('256969ce0d10af13735c022198c8269b31ef9b2f35171d9ad3e422d566773b0f')]).
thf(typ_all,type, (all : ($i>($i>$o)>$o)), file('grundlagen.aut','l_all'), [hashroot('7af775405bb712aec9ce7cdde103343acfd567932781aa9cedcee8d57f9a249d')]).
thf(def_all,definition, (all = (^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@X0))))), file('grundlagen.aut','l_all')).
thf(typ_real,type, (real : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_real'), [hashroot('d2db906daf81bb10db98f39130dcf91ed77efdd52a7c588d62fcf5700a867d0d')]).
thf(typ_r_all,type, (r_all : (($i>$o)>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_all'), [hashroot('71ab421aeb8b6fa16699f1ff0abe7c6d85cfc1f732c8cdefdd5f23fc8f6c0845')]).
thf(def_r_all,definition, (r_all = (all@real)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_all')).
thf(typ_r_in,type, (r_in : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_in'), [hashroot('cbdf7ced68081c529175c89c9d8641cecf320c8045df68677b054a3c949d394d')]).
thf(typ_pos,type, (pos : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_pos'), [hashroot('d62d1203e5678a698b0796b86304e2af37d1befc7c6740c45d925f334c9d5def')]).
thf(typ_r_less,type, (r_less : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_less'), [hashroot('82899c761044efd1e6f68b31677f13f557af10881932b504b587a1151ad7b637')]).
thf(k_5r205_t24,axiom, (all_of@(^ [X0:$i] : (in@X0@(power@real)))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@real))@(^ [X1:$i] : ((d_and@(pos@X1)@(r_in@X1@X0)) => (r_in@X1@X0)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_5r205_t24'), [hashroot('ecdc1a256d3285b4967ec3aea850e5adeb610f14df9d6eea12f320082d6b1b04')]).
thf(k_5r205_t32,conjecture, (all_of@(^ [X0:$i] : (in@X0@(power@real)))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@(power@real)))@(^ [X1:$i] : ((r_all@(^ [X2:$i] : ((r_in@X2@X0) => (r_all@(^ [X3:$i] : ((r_in@X3@X1) => (r_less@X2@X3))))))) => (all_of@(^ [X2:$i] : (in@X2@real))@(^ [X2:$i] : ((d_and@(pos@X2)@(r_in@X2@X0)) => (all_of@(^ [X3:$i] : (in@X3@real))@(^ [X3:$i] : ((r_in@X3@X1) => (r_less@X2@X3)))))))))))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_5r205_t32'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_5r205_t45')], [hashroot('fbcdcbf94b0e3e9a30b7bba69ab81ef24518e2c9da0cc31490f13c56c09f210f')]).
