thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_d_Sep,type, (d_Sep : ($i>($i>$o)>$i)), [file('grundlagen.aut','l_e_ot'),file('grundlagen.aut','l_e_st_setof')], [hashroot('218a016b04c605cddd8baabd01b9102f19cbd21e943087fb26bfbb5c1990d944')]).
thf(typ_e_in,type, (e_in : ($i>($i>$o)>$i>$i)), file('grundlagen.aut','l_e_in'), [hashroot('40a8d54e1c36f987e5c14c936a463272f0170c12f87e4577d32401d4d2954a6b')]).
thf(e_inp,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@(d_Sep@X0@X1)))@(^ [X2:$i] : (X1@(e_in@X0@X1@X2))))), file('grundlagen.aut','l_e_inp'), [hashroot('a8921f20e3015cdacdae28d1a0862708b3f26c836c7d63b7eade426a1aefe1b0')]).
thf(typ_cut,type, (cut : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cut'), [hashroot('4e4ed30803b18f94089b782cbb02cbec09b7993c5cdb0300fa191d3e3af33344')]).
thf(typ_ratrp,type, (ratrp : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ratrp'), [hashroot('df4c5f77bea535fbe2ee3ec0b709a41df6782f28e2fde989fd5313c63cda41c6')]).
thf(typ_ratt,type, (ratt : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_ratt'), [hashroot('a34be347475fa26c851b0491e675605a03e116ff55fd2caa530c851a80d711ea')]).
thf(def_ratt,definition, (ratt = (d_Sep@cut@ratrp)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_ratt')).
thf(typ_rpofrtt,type, (rpofrtt : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_rpofrtt'), [hashroot('1a2230c26229aafb6d0c6779126a9cfd783cc4caafa66257413edeeddde530a6')]).
thf(def_rpofrtt,definition, (rpofrtt = (e_in@cut@ratrp)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_rpofrtt')).
thf(rtt_ratrpi,conjecture, (all_of@(^ [X0:$i] : (in@X0@ratt))@(^ [X0:$i] : (ratrp@(rpofrtt@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_ratrpi'), [hashroot('4bb07cf1df01812c167e381c50478e2706c82cb29a69dc8bb9aef9fe082a6222')]).
