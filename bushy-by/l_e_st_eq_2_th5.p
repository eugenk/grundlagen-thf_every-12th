thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_power,type, (power : ($i>$i)), file('grundlagen.aut','l_e_st_set'), [hashroot('0c5490ca2f6d61c2d410e7907be97b3bc36b3e4de614e1f5431278dbccad4c79')]).
thf(typ_imp,type, (imp : ($o>$o>$o)), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')], [hashroot('c471f7f9b55e8fce15c5b7f92a344808917c7bdfbac8043315ef5a52fb233e79')]).
thf(def_imp,definition, (imp = (^ [X0:$o] : ^ [X1:$o] : (X0 => X1))), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')]).
thf(typ_l_some,type, (l_some : ($i>($i>$o)>$o)), file('grundlagen.aut','l_some'), [hashroot('e725afc4936069e5e8c753118fc6844ba6a33e0b7ed57300c90a0659591806c9')]).
thf(someapp,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((l_some@X0@X1) => (! [X2:$o] : ((all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : (imp@(X1@X3)@X2))) => X2)))), file('grundlagen.aut','l_someapp'), [hashroot('d4d5f414178883980c8b8bc2074a4d3d039316e9f9d7903e7ec83c226af3539c')]).
thf(typ_nonempty,type, (nonempty : ($i>$i>$o)), file('grundlagen.aut','l_e_st_nonempty'), [hashroot('589508fa99afe2c763a9af671c29bddf10ecac3858162dcf46ae523265a15d50')]).
thf(typ_ecp,type, (ecp : ($i>($i>$i>$o)>$i>$i>$o)), file('grundlagen.aut','l_e_st_eq_ecp'), [hashroot('fa6b51b00e83bf720701259dbd4491863ab2749455492e59ca3912870a5f5051')]).
thf(typ_anec,type, (anec : ($i>($i>$i>$o)>$i>$o)), file('grundlagen.aut','l_e_st_eq_anec'), [hashroot('c9d0bac44c23951a0680ca0d539160d4659408e1c67164511df41008292a3992')]).
thf(def_anec,definition, (anec = (^ [X0:$i] : ^ [X1:($i>$i>$o)] : ^ [X2:$i] : (l_some@X0@(ecp@X0@X1@X2)))), file('grundlagen.aut','l_e_st_eq_anec')).
thf(eq_2_t4,axiom, (! [X0:$i] : ! [X1:($i>$i>$o)] : ((all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (X1@X2@X2))) => (all_of@(^ [X2:$i] : (in@X2@(power@X0)))@(^ [X2:$i] : (all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : ((ecp@X0@X1@X2@X3) => (nonempty@X0@X2)))))))), file('grundlagen.aut','l_e_st_eq_2_t4'), [hashroot('1fc17321cb67150eeb334fdc1eda69548d723bab964c975e4d0a55f0d2d06aa8')]).
thf(k_2_th5,conjecture, (! [X0:$i] : ! [X1:($i>$i>$o)] : ((all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (X1@X2@X2))) => (all_of@(^ [X2:$i] : (in@X2@(power@X0)))@(^ [X2:$i] : ((anec@X0@X1@X2) => (nonempty@X0@X2)))))), file('grundlagen.aut','l_e_st_eq_2_th5'), [hashroot('f90d4be5bcf4d2b9730a6140f985b42ee962513b76321edd066db9371baeb7e5')]).
