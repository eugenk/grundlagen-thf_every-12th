thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_e_is,type, (e_is : ($i>$i>$i>$o)), [file('grundlagen.aut','l_e_is'),file('grundlagen.aut','l_r_ite_is')], [hashroot('8b0ca0dd413f254c6ad2f3c70f60a1023635d6ff3be83f7d4f2b7bf8ee537789')]).
thf(typ_pair1,type, (pair1 : ($i>$i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_pair1'), [hashroot('22c11c2853d1e588f3ac8516f0fabefcfeb723777f2646245247cb58ebcc0845')]).
thf(typ_second1,type, (second1 : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_second1'), [hashroot('dc95131b0703782c93e3d49db6e54d235733c59c9379547d981a39250b8dc2c5')]).
thf(second1is1,axiom, (! [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@X0))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : (e_is@X0@(second1@X0@(pair1@X0@X1@X2))@X2)))))), file('grundlagen.aut','l_e_st_eq_landau_n_second1is1'), [hashroot('f3d05c26d8b5d602a80e102783970d55fc55e5e06685fe39cfb377949f427958')]).
thf(typ_cut,type, (cut : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cut'), [hashroot('4e4ed30803b18f94089b782cbb02cbec09b7993c5cdb0300fa191d3e3af33344')]).
thf(typ_rp_is,type, (rp_is : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_is'), [hashroot('d8b675a0120cea29d5c6b85ee93e20272a34d7cbbfb7974311f160661823a44a')]).
thf(def_rp_is,definition, (rp_is = (e_is@cut)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_is')).
thf(typ_rp_df,type, (rp_df : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_df'), [hashroot('ecccc01497ad9f28d18ca2a6d3717c0764a0e070ad7428f427db5dc72aed09e7')]).
thf(def_rp_df,definition, (rp_df = (pair1@cut)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_df')).
thf(typ_std,type, (std : ($i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_std'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2a'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2b'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2c'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2d')], [hashroot('362d65e96a7cad4d6c9d9997c2ef18c12945d4fa1090d9535c57b04157d01db2')]).
thf(def_std,definition, (std = (second1@cut)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_std'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2a'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2b'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2c'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_2d')]).
thf(stdis,conjecture, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : (rp_is@(std@(rp_df@X0@X1))@X1))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_stdis'), [hashroot('540241d8aeac054011b85b61acc7b6e7d7ff4adffd70ed9de7ecd7d1c62f893b')]).
