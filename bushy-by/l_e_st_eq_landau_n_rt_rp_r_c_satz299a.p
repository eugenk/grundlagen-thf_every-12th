thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_ordsucc,type, (ordsucc : ($i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_suc'),file('grundlagen.aut','l_e_st_eq_landau_n_8274_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8276_xs')], [hashroot('6f2bd357b24438c387f3433d2366b843d110c03ef0b8bd82c5a0cb92bfc5b50d')]).
thf(typ_ap,type, (ap : ($i>$i>$i)), unknown, [hashroot('a7decc6ab7cd5672b0a906b215a444cd9aa45edcb0967cd0c9d40e207f8df623')]).
thf(typ_imp,type, (imp : ($o>$o>$o)), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')], [hashroot('c471f7f9b55e8fce15c5b7f92a344808917c7bdfbac8043315ef5a52fb233e79')]).
thf(def_imp,definition, (imp = (^ [X0:$o] : ^ [X1:$o] : (X0 => X1))), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')]).
thf(typ_d_not,type, (d_not : ($o>$o)), file('grundlagen.aut','l_not'), [hashroot('122455a7450b0bbfcd7daa48af7a7a4bf320a8f085b235e0bfe56f0a1de2967f')]).
thf(imp_th3,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X1) => ((imp@X0@X1) => (d_not@X0)))), file('grundlagen.aut','l_imp_th3'), [hashroot('a757d28bd2958af930c1a84d0628d57d516363eea09fd8bf5745aa31fa10c6a6')]).
thf(typ_e_is,type, (e_is : ($i>$i>$i>$o)), [file('grundlagen.aut','l_e_is'),file('grundlagen.aut','l_r_ite_is')], [hashroot('8b0ca0dd413f254c6ad2f3c70f60a1023635d6ff3be83f7d4f2b7bf8ee537789')]).
thf(typ_nat,type, (nat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_nat'), [hashroot('a70cf6792dc8538ebedafac6b0b6924bbcece48e47a0c0cb77b73b3cb5eba62f')]).
thf(typ_n_is,type, (n_is : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_is'),file('grundlagen.aut','l_e_st_eq_landau_n_29_i')], [hashroot('f438ef4d45e17e52a3513bcba965b0f71de0b41c1f2de432a49525837a2585a6')]).
thf(typ_nis,type, (nis : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_nis'), [hashroot('3fd3dd3f94a190d22a5db911977f86d4e9ea9b3dea63d7982205749691662311')]).
thf(def_nis,definition, (nis = (^ [X0:$i] : ^ [X1:$i] : (d_not@(n_is@X0@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_nis')).
thf(typ_n_1,type, (n_1 : $i), file('grundlagen.aut','l_e_st_eq_landau_n_1'), [hashroot('72510cdcd4be46383fe2950c32184464e04a3a48f2bbb6fb2e0e938a74c551d6')]).
thf(n_ax3,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (nis@(ordsucc@X0)@n_1))), file('grundlagen.aut','l_e_st_eq_landau_n_ax3'), [hashroot('5684aa6f556d25f24a0d6a91fa368f619dc42743fb35d9b9a03e8e017f509be1')]).
thf(typ_c_natt,type, (c_natt : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_natt'), [hashroot('e6b2b2866455836ac64e48d6650d617c8daa0caf8d98616b655fd514c11ccbc5')]).
thf(typ_c_nofnt,type, (c_nofnt : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_nofnt'), [hashroot('251b95d851549353d548de97f839af4c279d354864ed85235c22cf7a43a2c0c4')]).
thf(c_nofnt_p,axiom, (all_of@(^ [X0:$i] : (in@X0@c_natt))@(^ [X0:$i] : (is_of@(c_nofnt@X0)@(^ [X1:$i] : (in@X1@nat))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_nofnt_p'), [hashroot('29c588f2a14059b3c9014cca495a5a0ee97249f5ee875a2eac15eeb5242bef3b')]).
thf(typ_c_1t,type, (c_1t : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_1t'), [hashroot('0d9d6fa4bbd1313e34864b80382f6f294f7bc9183b3332b4fd0929c88302f9a9')]).
thf(typ_c_suct,type, (c_suct : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_suct'), [hashroot('82ddb96d8ddea1c41f3cadf7f6e03f1ce6cfc26857d5781bdd0a7b22901d1d1b')]).
thf(k_10299_t1,axiom, (all_of@(^ [X0:$i] : (in@X0@c_natt))@(^ [X0:$i] : ((e_is@c_natt@(ap@c_suct@X0)@c_1t) => (n_is@(ordsucc@(c_nofnt@X0))@n_1)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_10299_t1'), [hashroot('13a36f973ddbf6ce22d2ac497fbd627cd2f6663b23239e150bec47bddec39ba0')]).
thf(satz299a,conjecture, (all_of@(^ [X0:$i] : (in@X0@c_natt))@(^ [X0:$i] : (d_not@(e_is@c_natt@(ap@c_suct@X0)@c_1t)))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_satz299a'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_ax3t')], [hashroot('1ba3eddd8318cf05a4a010982ec007a53762060ff09cf9daff05b09ca4b45278')]).
