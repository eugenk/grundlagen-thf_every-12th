thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_d_not,type, (d_not : ($o>$o)), file('grundlagen.aut','l_not'), [hashroot('122455a7450b0bbfcd7daa48af7a7a4bf320a8f085b235e0bfe56f0a1de2967f')]).
thf(typ_dif,type, (dif : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_dif'), [hashroot('cec68cedcff46da8ae374b12e2425580a20cf5fde1ffb9197afff9371c1256e5')]).
thf(typ_posd,type, (posd : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_posd'), [hashroot('ed09be3796714fb52455a8850c5fee30577f3e284bbb6261415c1a713a44333c')]).
thf(typ_zero,type, (zero : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_zero'), [hashroot('193dc54f7551ae20db0cc4c54474dc43c7d85aeae10539bfa6778d5e74225adb')]).
thf(typ_negd,type, (negd : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_negd'), [hashroot('70036c244370d51ffbaa6654fe1575ae894dea31a33d728214840b8a4a7561dc')]).
thf(nnot0d,axiom, (all_of@(^ [X0:$i] : (in@X0@dif))@(^ [X0:$i] : ((negd@X0) => (d_not@(zero@X0))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_nnot0d'), [hashroot('a7a58ea58c360bea38859f68395b8bdd61344929c0e9c7bed94227c144ce9598')]).
thf(typ_rp_td,type, (rp_td : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_td'), [hashroot('152f6ec34784c918b4f2d5b3e61b7da9027d3addf69c1589e2a4b0b585a3a255')]).
thf(rp_td_p,axiom, (all_of@(^ [X0:$i] : (in@X0@dif))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@dif))@(^ [X1:$i] : (is_of@(rp_td@X0@X1)@(^ [X2:$i] : (in@X2@dif))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_td_p'), [hashroot('d51c06103e8d5691d815c9a37140a44cd309a3b85fd3c273d4180b4f01a978d3')]).
thf(ntdpn,axiom, (all_of@(^ [X0:$i] : (in@X0@dif))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@dif))@(^ [X1:$i] : ((posd@X0) => ((negd@X1) => (negd@(rp_td@X0@X1)))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ntdpn'), [hashroot('6f64275ad7dd8045f58ec74722678aa9c6d1d88c34ce3cc807b9403c95823113')]).
thf(k_4d192_t3,conjecture, (all_of@(^ [X0:$i] : (in@X0@dif))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@dif))@(^ [X1:$i] : ((posd@X0) => ((negd@X1) => (d_not@(zero@(rp_td@X0@X1))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_4d192_t3'), [hashroot('a9e8e084b23170f0c9b642b5f429542ff2f05f039a44ce7a729d0c119a643f16')]).
