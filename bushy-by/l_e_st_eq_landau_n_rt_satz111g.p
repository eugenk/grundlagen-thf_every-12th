thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_l_some,type, (l_some : ($i>($i>$o)>$o)), file('grundlagen.aut','l_some'), [hashroot('e725afc4936069e5e8c753118fc6844ba6a33e0b7ed57300c90a0659591806c9')]).
thf(typ_amone,type, (amone : ($i>($i>$o)>$o)), file('grundlagen.aut','l_e_amone'), [hashroot('abf9f4c92914eaea25fda61ade255847fa59a23ec25d76f12c98c471d2ae0d1c')]).
thf(typ_one,type, (one : ($i>($i>$o)>$o)), file('grundlagen.aut','l_e_one'), [hashroot('7091b9e752a98fbbb417ba534750e8d45ae4a3f689bed160c8f19a85db223071')]).
thf(e_onei,axiom, (! [X0:$i] : ! [X1:($i>$o)] : ((amone@X0@X1) => ((l_some@X0@X1) => (one@X0@X1)))), file('grundlagen.aut','l_e_onei'), [hashroot('28d87e2a3b3a3db302eefe1d8cb5c5dbd60af9af0c37ccc827d4901169395a69')]).
thf(typ_nat,type, (nat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_nat'), [hashroot('a70cf6792dc8538ebedafac6b0b6924bbcece48e47a0c0cb77b73b3cb5eba62f')]).
thf(typ_n_some,type, (n_some : (($i>$o)>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_some'), [hashroot('25dbba53aea6be6e2f767148e9033334d9ae05f69ba26e54579bb754d6a6ebba')]).
thf(def_n_some,definition, (n_some = (l_some@nat)), file('grundlagen.aut','l_e_st_eq_landau_n_some')).
thf(typ_n_one,type, (n_one : (($i>$o)>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_one'), [hashroot('cc08051c6ab7a7041b2d8a12ee56dabe6646cafa70c5396e212c41db0d13efe5')]).
thf(def_n_one,definition, (n_one = (one@nat)), file('grundlagen.aut','l_e_st_eq_landau_n_one')).
thf(typ_rat,type, (rat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rat'), [hashroot('e034fba97d474a28e571a50e33c2074cc52e74b2bfd0b39f8238729be437398c')]).
thf(typ_natprop,type, (natprop : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_natprop'), [hashroot('ffa3afd875c1e2b8ea785eb98c77eaf458cd7030077db283b1c01ee617c88b7b')]).
thf(typ_natrt,type, (natrt : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_natrt'), [hashroot('206fe848603d9612c1ee1c9cee8bd9bafcb52b525e3d7db768b3694d9e1fdb56')]).
thf(def_natrt,definition, (natrt = (^ [X0:$i] : (n_some@(natprop@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_natrt')).
thf(ii5_t23,axiom, (all_of@(^ [X0:$i] : (in@X0@rat))@(^ [X0:$i] : (amone@nat@(natprop@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_ii5_t23'), [hashroot('7858bf00266dbe8553ca3fb16fb3f6666074cb016916028f80e1f5e0af9f2dd4')]).
thf(satz111g,conjecture, (all_of@(^ [X0:$i] : (in@X0@rat))@(^ [X0:$i] : ((natrt@X0) => (n_one@(natprop@X0))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_satz111g'), [hashroot('1adcc1be752e6d62b760dc78779fa3bb5d5d5f33a02ab62d5d89af09feaca9df')]).
