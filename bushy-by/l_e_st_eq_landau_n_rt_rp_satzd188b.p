thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_dif,type, (dif : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_dif'), [hashroot('cec68cedcff46da8ae374b12e2425580a20cf5fde1ffb9197afff9371c1256e5')]).
thf(typ_rp_eq,type, (rp_eq : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_eq'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_eq')], [hashroot('ca6772f54f9c50dc242e006a36cac737aac5ef964cafef0b5f68a61c5c5b2053')]).
thf(typ_zero,type, (zero : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_zero'), [hashroot('193dc54f7551ae20db0cc4c54474dc43c7d85aeae10539bfa6778d5e74225adb')]).
thf(typ_rp_pd,type, (rp_pd : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_pd'), [hashroot('df036bd89eef2859e847c7adf6aced0bf2ab50f8a65aa787840f143d78954d0b')]).
thf(typ_rp_md,type, (rp_md : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_md'), [hashroot('a05b2abd7d58d1db3d4d4e59561a35da67b7a2ca8771129aa61c1961ca546590')]).
thf(satzd182b,axiom, (all_of@(^ [X0:$i] : (in@X0@dif))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@dif))@(^ [X1:$i] : ((zero@(rp_md@X0@X1)) => (rp_eq@X0@X1)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satzd182b'), [hashroot('6917d3971494fe1772b353b21e983c7bccb57ee881d7a6dabd9d6ea6f4dae874')]).
thf(k_3d188_t4,axiom, (all_of@(^ [X0:$i] : (in@X0@dif))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@dif))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@dif))@(^ [X2:$i] : ((rp_eq@(rp_pd@X0@X2)@(rp_pd@X1@X2)) => (zero@(rp_md@X0@X1))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_3d188_t4'), [hashroot('b8b72d6e24f3f6f9b60dac046013f5da4f5ee49634bc0b7d6aab7d6aad7572d1')]).
thf(satzd188b,conjecture, (all_of@(^ [X0:$i] : (in@X0@dif))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@dif))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@dif))@(^ [X2:$i] : ((rp_eq@(rp_pd@X0@X2)@(rp_pd@X1@X2)) => (rp_eq@X0@X1)))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_satzd188b'), [hashroot('6bb3182417021c96cf0491f3aaf90efb8eda9cfd69c841a9c761f875ae01bd76')]).
