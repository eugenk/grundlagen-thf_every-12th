thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_ec3,type, (ec3 : ($o>$o>$o>$o)), file('grundlagen.aut','l_ec3'), [hashroot('986599080791a6070d17046968b2c7f73c3d807171c7e5030cc97fd520a4e49c')]).
thf(typ_frac,type, (frac : $i), file('grundlagen.aut','l_e_st_eq_landau_n_frac'), [hashroot('c222f7117ac203e0edab1ed9e37462fb740b2ea273a724c7102a6cdf50a70879')]).
thf(typ_n_eq,type, (n_eq : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_eq'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_eq')], [hashroot('e55588efa2d65b223ca727021f7e0aa395778376e99ad9ce041a74b2566176e4')]).
thf(typ_moref,type, (moref : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_moref'), [hashroot('bde92bf0e2da4e14488b138389309844e2ea9aea89d702285924c2b43fbfbb03')]).
thf(typ_lessf,type, (lessf : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_lessf'), [hashroot('9937e17543f6dfbec93fe5618c53859bd8c22fb5ef59aa0170ad797651421703')]).
thf(satz41b,axiom, (all_of@(^ [X0:$i] : (in@X0@frac))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@frac))@(^ [X1:$i] : (ec3@(n_eq@X0@X1)@(moref@X0@X1)@(lessf@X0@X1)))))), [file('grundlagen.aut','l_e_st_eq_landau_n_satz41b'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_581_t6')], [hashroot('4b7396fb457b97c88e1be0d276135dff78078dc932ad5d4df481a541bcc9bfed')]).
thf(typ_n_pf,type, (n_pf : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_pf'), [hashroot('835704b2481f78978ded4d6d98f92aa0990b18ea3d34396e2af4cf1f9b8596ac')]).
thf(n_pf_p,axiom, (all_of@(^ [X0:$i] : (in@X0@frac))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@frac))@(^ [X1:$i] : (is_of@(n_pf@X0@X1)@(^ [X2:$i] : (in@X2@frac))))))), file('grundlagen.aut','l_e_st_eq_landau_n_pf_p'), [hashroot('cd0eecab7d155ffe08e92190878735677868f2cc96c68bb45717a1c310f5bd13')]).
thf(k_363_t2,conjecture, (all_of@(^ [X0:$i] : (in@X0@frac))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@frac))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@frac))@(^ [X2:$i] : (ec3@(n_eq@(n_pf@X0@X2)@(n_pf@X1@X2))@(moref@(n_pf@X0@X2)@(n_pf@X1@X2))@(lessf@(n_pf@X0@X2)@(n_pf@X1@X2))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_363_t2'), [hashroot('d661dafff7525ed7f418543e6c1c48df2ab610b460be6e57ffbbaf49a91644d4')]).
