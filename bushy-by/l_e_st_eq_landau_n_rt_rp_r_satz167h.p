thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_imp,type, (imp : ($o>$o>$o)), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')], [hashroot('c471f7f9b55e8fce15c5b7f92a344808917c7bdfbac8043315ef5a52fb233e79')]).
thf(def_imp,definition, (imp = (^ [X0:$o] : ^ [X1:$o] : (X0 => X1))), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')]).
thf(typ_d_not,type, (d_not : ($o>$o)), file('grundlagen.aut','l_not'), [hashroot('122455a7450b0bbfcd7daa48af7a7a4bf320a8f085b235e0bfe56f0a1de2967f')]).
thf(typ_wel,type, (wel : ($o>$o)), file('grundlagen.aut','l_wel'), [hashroot('3c7461ae99cc5fcbcf4c6eb8756a1630dde3bca516b9da8a16686247350ecfaf')]).
thf(def_wel,definition, (wel = (^ [X0:$o] : (d_not@(d_not@X0)))), file('grundlagen.aut','l_wel')).
thf(l_weli,axiom, (! [X0:$o] : (X0 => (wel@X0))), file('grundlagen.aut','l_weli'), [hashroot('b001b5c8fe4dbeee8d0cd64f883b86da672179a6bf2c4b5029c6c7ef33fd8bb2')]).
thf(imp_th3,axiom, (! [X0:$o] : ! [X1:$o] : ((d_not@X1) => ((imp@X0@X1) => (d_not@X0)))), file('grundlagen.aut','l_imp_th3'), [hashroot('a757d28bd2958af930c1a84d0628d57d516363eea09fd8bf5745aa31fa10c6a6')]).
thf(typ_real,type, (real : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_real'), [hashroot('d2db906daf81bb10db98f39130dcf91ed77efdd52a7c588d62fcf5700a867d0d')]).
thf(typ_r_less,type, (r_less : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_less'), [hashroot('82899c761044efd1e6f68b31677f13f557af10881932b504b587a1151ad7b637')]).
thf(typ_r_moreis,type, (r_moreis : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_moreis'), [hashroot('4a39bfebaf4d39795a82f92f0925753a5becec4cb9a5c6eb78098410a3ac8cf9')]).
thf(satz167c,axiom, (all_of@(^ [X0:$i] : (in@X0@real))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@real))@(^ [X1:$i] : ((r_moreis@X0@X1) => (d_not@(r_less@X0@X1))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_satz167c'), [hashroot('a5ded5422c584de72579fff72a223f309fe4708e63ba154f887d6fdc5ad685b1')]).
thf(satz167h,conjecture, (all_of@(^ [X0:$i] : (in@X0@real))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@real))@(^ [X1:$i] : ((r_less@X0@X1) => (d_not@(r_moreis@X0@X1))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_satz167h'), [hashroot('2b23c324770eb8644761ff89817c48638542f18f81b454146e477c328e5184d1')]).
