thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_d_Sep,type, (d_Sep : ($i>($i>$o)>$i)), [file('grundlagen.aut','l_e_ot'),file('grundlagen.aut','l_e_st_setof')], [hashroot('218a016b04c605cddd8baabd01b9102f19cbd21e943087fb26bfbb5c1990d944')]).
thf(typ_e_is,type, (e_is : ($i>$i>$i>$o)), [file('grundlagen.aut','l_e_is'),file('grundlagen.aut','l_r_ite_is')], [hashroot('8b0ca0dd413f254c6ad2f3c70f60a1023635d6ff3be83f7d4f2b7bf8ee537789')]).
thf(typ_out,type, (out : ($i>($i>$o)>$i>$i)), file('grundlagen.aut','l_e_out'), [hashroot('6fc658a37010bc78301a6074982c0339667b8258b5cd6365a3524f5d846fd607')]).
thf(isouti,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : ((X1@X2) => (all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : ((X1@X3) => ((e_is@X0@X2@X3) => (e_is@(d_Sep@X0@X1)@(out@X0@X1@X2)@(out@X0@X1@X3)))))))))), file('grundlagen.aut','l_e_isouti'), [hashroot('bfab65c2151cbc5374daa4f54be65d86a890d1bbd7949e5034000167c4a969d9')]).
thf(typ_cut,type, (cut : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_cut'), [hashroot('4e4ed30803b18f94089b782cbb02cbec09b7993c5cdb0300fa191d3e3af33344')]).
thf(typ_rp_is,type, (rp_is : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_is'), [hashroot('d8b675a0120cea29d5c6b85ee93e20272a34d7cbbfb7974311f160661823a44a')]).
thf(def_rp_is,definition, (rp_is = (e_is@cut)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_is')).
thf(typ_ratrp,type, (ratrp : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_ratrp'), [hashroot('df4c5f77bea535fbe2ee3ec0b709a41df6782f28e2fde989fd5313c63cda41c6')]).
thf(typ_ratt,type, (ratt : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_ratt'), [hashroot('a34be347475fa26c851b0491e675605a03e116ff55fd2caa530c851a80d711ea')]).
thf(def_ratt,definition, (ratt = (d_Sep@cut@ratrp)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_ratt')).
thf(typ_rttofrp,type, (rttofrp : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_rttofrp'), [hashroot('5ce652ac89db89d6f6a0cc6415e61cfba7329f0d80082a2e2de12c151ea95fa1')]).
thf(def_rttofrp,definition, (rttofrp = (out@cut@ratrp)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_rttofrp')).
thf(typ_rtt_is,type, (rtt_is : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_is'), [hashroot('3ca9ae93018314b0fd23839e75b5bf1a6115a8b48f3353bd38b628004641d13e')]).
thf(def_rtt_is,definition, (rtt_is = (e_is@ratt)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_is')).
thf(isrpertt,conjecture, (all_of@(^ [X0:$i] : (in@X0@cut))@(^ [X0:$i] : ((ratrp@X0) => (all_of@(^ [X1:$i] : (in@X1@cut))@(^ [X1:$i] : ((ratrp@X1) => ((rp_is@X0@X1) => (rtt_is@(rttofrp@X0)@(rttofrp@X1))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_rtt_isrpertt'), [hashroot('c6c8dc2f2b4d059d1f97a7b294f326448d28c33680e399fd90812bc931400dc8')]).
