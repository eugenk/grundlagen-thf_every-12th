thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_complex,type, (complex : $i), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_complex'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_cx')], [hashroot('4bb7039770d4387b91af354ed87100d8f3f5905516b480cc18ed3d1800fa7db1')]).
thf(typ_c_is,type, (c_is : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_is'), [hashroot('2942f9a2f85def39885d3c7419f850963ebb1119d90e91f3ab52a37e2e3f1307')]).
thf(typ_c_0c,type, (c_0c : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_0c'), [hashroot('2ef05baba59e81d3bfea71e3bff42e72e8dbb9ec5a3b48bf53368f5e144caa74')]).
thf(c_0c_p,axiom, (is_of@c_0c@(^ [X0:$i] : (in@X0@complex))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_0c_p'), [hashroot('1f7b4111e0d9ef3425a1dac32126bb934eb14d648490a2c29c014f3cd0a4aab5')]).
thf(typ_c_pl,type, (c_pl : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_pl'), [hashroot('fee19fe906a9f611a6cec52e1fac57d6b3b2dcdefc01f6179c6ec55d97ee05f0')]).
thf(typ_c_mn,type, (c_mn : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_mn'), [hashroot('00b89474c880f2f5f7bf5bfac318915a22525f6f7348b5e63c209d32b2975529')]).
thf(satz212h,axiom, (all_of@(^ [X0:$i] : (in@X0@complex))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@complex))@(^ [X1:$i] : (c_is@(c_pl@X1@(c_mn@X0@X1))@X0))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_satz212h'), [hashroot('4c35728262b155d7f46deedbe2286a92cc09b5626a1532d2a3bc9bf3b62b9580')]).
thf(typ_c_m0,type, (c_m0 : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_m0'), [hashroot('c639588c6ee049f3c7a3558deb0f93f0c3afee34e13fdacb4173d06c43600696')]).
thf(def_c_m0,definition, (c_m0 = (c_mn@c_0c)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_m0')).
thf(satz216,conjecture, (all_of@(^ [X0:$i] : (in@X0@complex))@(^ [X0:$i] : (c_is@(c_pl@X0@(c_m0@X0))@c_0c))), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_satz216'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_2216_anders')], [hashroot('c56ee9f441f0eb1372d1952436cddf1396e8cde6a4fa80d1e744bc7c68c0e9f9')]).
