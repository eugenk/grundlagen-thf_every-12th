thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_d_Sigma,type, (d_Sigma : ($i>($i>$i)>$i)), unknown, [hashroot('75c40645d5fe236c1ca11856fa4f32b0ebb4c95c94a9615ec27513c1c1b6b5d3')]).
thf(typ_ap,type, (ap : ($i>$i>$i)), unknown, [hashroot('a7decc6ab7cd5672b0a906b215a444cd9aa45edcb0967cd0c9d40e207f8df623')]).
thf(beta,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:$i] : ((in@X2@X0) => ((ap@(d_Sigma@X0@X1)@X2) = (X1@X2)))), unknown, [hashroot('2ad5629b8f82190c8d16e96ea95dadf81dafc029ae9a3396d9cb2d171e6fce86')]).
thf(typ_d_Pi,type, (d_Pi : ($i>($i>$i)>$i)), unknown, [hashroot('68d9661a0d30c2de45df1cf7708fadc581b6b52cb2ba22f5f89c21527ade885f')]).
thf(lam_Pi,axiom, (! [X0:$i] : ! [X1:($i>$i)] : ! [X2:($i>$i)] : ((! [X3:$i] : ((in@X3@X0) => (in@(X2@X3)@(X1@X3)))) => (in@(d_Sigma@X0@X2)@(d_Pi@X0@X1)))), unknown, [hashroot('0188856999da4593b9c9db87d02370f335d61bff2c902d2a1a4414a301b486fb')]).
thf(typ_e_is,type, (e_is : ($i>$i>$i>$o)), [file('grundlagen.aut','l_e_is'),file('grundlagen.aut','l_r_ite_is')], [hashroot('8b0ca0dd413f254c6ad2f3c70f60a1023635d6ff3be83f7d4f2b7bf8ee537789')]).
thf(e_isf,axiom, (! [X0:$i] : ! [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@(d_Pi@X0@(^ [X3:$i] : X1))))@(^ [X2:$i] : (all_of@(^ [X3:$i] : (in@X3@X0))@(^ [X3:$i] : (all_of@(^ [X4:$i] : (in@X4@X0))@(^ [X4:$i] : ((e_is@X0@X3@X4) => (e_is@X1@(ap@X2@X3)@(ap@X2@X4)))))))))), file('grundlagen.aut','l_e_isf'), [hashroot('a28a576347efd792da01db6a1de02e2bbf0fa818d0fc9924d47377ef8429261e')]).
thf(typ_real,type, (real : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_real'), [hashroot('d2db906daf81bb10db98f39130dcf91ed77efdd52a7c588d62fcf5700a867d0d')]).
thf(typ_r_is,type, (r_is : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_is'), [hashroot('118ce08c1ad9511b2d7e90507a848a8dd6d738336d22639ae40514ff1af3c31e')]).
thf(def_r_is,definition, (r_is = (e_is@real)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_is')).
thf(typ_indreal2,type, (indreal2 : ($i>$i>$i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_indreal2'), [hashroot('151599bbac7b9db59e07ff356d650bf6fdfefc878602a3a2b9bab35a384f4bc2')]).
thf(typ_timesdr,type, (timesdr : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_timesdr'), [hashroot('5515779e902dad332d2e7c81aacd9d8d1f3b470184d434e69e9eb733dadcd2cd')]).
thf(typ_r_ts,type, (r_ts : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_ts'), [hashroot('4faaeb7a5086607e34bf07e9d5a26f1a5683fae80d27a62976d1415c9e3473a6')]).
thf(def_r_ts,definition, (r_ts = (indreal2@real@timesdr)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_ts')).
thf(r_ts_p,axiom, (all_of@(^ [X0:$i] : (in@X0@real))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@real))@(^ [X1:$i] : (is_of@(r_ts@X0@X1)@(^ [X2:$i] : (in@X2@real))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_ts_p'), [hashroot('1e5c326c8a20626d318bb7825a8486545aec9ec528086d834f5f8ee70f196165')]).
thf(r_ists2,conjecture, (all_of@(^ [X0:$i] : (in@X0@real))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@real))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@real))@(^ [X2:$i] : ((r_is@X0@X1) => (r_is@(r_ts@X2@X0)@(r_ts@X2@X1))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_ists2'), [hashroot('405a1df6b13d915e17dda055cf849c9baad20e34a637a9ec3fa78068974a9625')]).
