thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_d_Sep,type, (d_Sep : ($i>($i>$o)>$i)), [file('grundlagen.aut','l_e_ot'),file('grundlagen.aut','l_e_st_setof')], [hashroot('218a016b04c605cddd8baabd01b9102f19cbd21e943087fb26bfbb5c1990d944')]).
thf(typ_ordsucc,type, (ordsucc : ($i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_suc'),file('grundlagen.aut','l_e_st_eq_landau_n_8274_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_xs'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8276_xs')], [hashroot('6f2bd357b24438c387f3433d2366b843d110c03ef0b8bd82c5a0cb92bfc5b50d')]).
thf(typ_esti,type, (esti : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_st_esti'), [hashroot('b5666312851a073ed6c2b15baf0c1169dca70f962aeae381609dcf2fb5290190')]).
thf(estie,axiom, (! [X0:$i] : ! [X1:($i>$o)] : (all_of@(^ [X2:$i] : (in@X2@X0))@(^ [X2:$i] : ((esti@X0@X2@(d_Sep@X0@X1)) => (X1@X2))))), file('grundlagen.aut','l_e_st_estie'), [hashroot('c55812877be6f0f9110f14b90aebd810ab9d4254c2fdadb89279d0b7efae4002')]).
thf(typ_nat,type, (nat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_nat'), [hashroot('a70cf6792dc8538ebedafac6b0b6924bbcece48e47a0c0cb77b73b3cb5eba62f')]).
thf(typ_n_in,type, (n_in : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_in'), [hashroot('ea4a9ed2e4adc3c23ed63f257bda25991cf49f65d9a11139d385e981c72ad063')]).
thf(def_n_in,definition, (n_in = (esti@nat)), file('grundlagen.aut','l_e_st_eq_landau_n_in')).
thf(typ_n_1,type, (n_1 : $i), file('grundlagen.aut','l_e_st_eq_landau_n_1'), [hashroot('72510cdcd4be46383fe2950c32184464e04a3a48f2bbb6fb2e0e938a74c551d6')]).
thf(typ_i1_s,type, (i1_s : (($i>$o)>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_i1_s'), [hashroot('9d522c1cc25e15031109ab3da954fd985d1cb195a0313b70133d3b16361da38f')]).
thf(def_i1_s,definition, (i1_s = (d_Sep@nat)), file('grundlagen.aut','l_e_st_eq_landau_n_i1_s')).
thf(i1_t4,axiom, (! [X0:($i>$o)] : ((X0@n_1) => ((all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : ((X0@X1) => (X0@(ordsucc@X1))))) => (all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : (n_in@X1@(i1_s@X0))))))), file('grundlagen.aut','l_e_st_eq_landau_n_i1_t4'), [hashroot('1e4c8fbe9a615203c3d5f03be3eea5d01330305da5d42556a133ed13b27a07e9')]).
thf(induction,conjecture, (! [X0:($i>$o)] : ((X0@n_1) => ((all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : ((X0@X1) => (X0@(ordsucc@X1))))) => (all_of@(^ [X1:$i] : (in@X1@nat))@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_induction'), [hashroot('85a26d83578e4cf270d16146d3d1d3907634f07a02c6bb4c55072802ce298a7b')]).
