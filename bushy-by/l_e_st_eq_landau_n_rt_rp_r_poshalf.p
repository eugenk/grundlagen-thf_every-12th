thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_real,type, (real : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_real'), [hashroot('d2db906daf81bb10db98f39130dcf91ed77efdd52a7c588d62fcf5700a867d0d')]).
thf(typ_r_nis,type, (r_nis : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_nis'), [hashroot('cec753b6fc627b1083d6d464a80d1a1090f32e23f86fdbaa6ea68fb7a51af35a')]).
thf(typ_r_0,type, (r_0 : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_0'), [hashroot('42c693b88c18ad0ee09158472407fcc7470030c98604483e31c59794f55d1b2c')]).
thf(typ_pos,type, (pos : ($i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_pos'), [hashroot('d62d1203e5678a698b0796b86304e2af37d1befc7c6740c45d925f334c9d5def')]).
thf(pnot0,axiom, (all_of@(^ [X0:$i] : (in@X0@real))@(^ [X0:$i] : ((pos@X0) => (r_nis@X0@r_0)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_pnot0'), [hashroot('480823d52498f671b6bf5c0e38f4b6f230700694de1ae1ef32664e1be9f7cdf3')]).
thf(typ_d_1rl,type, (d_1rl : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_1rl'), [hashroot('64fb4905a8571bea3c8069a56b8bc2713431e5fa9efadde0bf571a68cbe8523a')]).
thf(k_1rl_p,axiom, (is_of@d_1rl@(^ [X0:$i] : (in@X0@real))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_1rl_p'), [hashroot('b99e364b706dca6a86e8409a51ed32a5a84346237c3edc93d4909ca4fafbbd62')]).
thf(r_pos1,axiom, (pos@d_1rl), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_pos1'), [hashroot('6295f62b5967f8f978e2502db5f36ace7e4ba568f30b0ff496be7aa61a2b1c55')]).
thf(typ_r_ov,type, (r_ov : ($i>$i>$i)), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_ov'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_4r204_ros')], [hashroot('ff8a198a1f84a29c4bdf876159b74ec995b6f820445fb9e50cfe9172f57a538d')]).
thf(posovpp,axiom, (all_of@(^ [X0:$i] : (in@X0@real))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@real))@(^ [X1:$i] : ((r_nis@X1@r_0) => ((pos@X0) => ((pos@X1) => (pos@(r_ov@X0@X1))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_posovpp'), [hashroot('3839995b6f86d87e04fb936999b5c78f9077caad86ca1e044f6cbf4ffda82b35')]).
thf(typ_d_2rl,type, (d_2rl : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_2rl'), [hashroot('fc2d7ba2e75789ea5e251b9dccfe96936748d7abedc5a0c4f13cc9e3706debad')]).
thf(k_2rl_p,axiom, (is_of@d_2rl@(^ [X0:$i] : (in@X0@real))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_2rl_p'), [hashroot('e882ddd78eda5e9c71c0270127f28285f1acbe470357a02ef3db5a4a877ba501')]).
thf(r_pos2,axiom, (pos@d_2rl), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_pos2'), [hashroot('3f16b2b17025ecc5abdde21933ce0db6ff501005fddf21b521da91bf470bfc1b')]).
thf(typ_half,type, (half : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_half'), [hashroot('18d143bb27a5c9478bcf2289af9b52a7e1fa9f7294b82f4a7c600af40f36d10a')]).
thf(def_half,definition, (half = (r_ov@d_1rl@d_2rl)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_half')).
thf(poshalf,conjecture, (pos@half), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_poshalf'), [hashroot('c28e0aadb9a1238b631d04845f0fb590108b568d4f43df3c4549fd49b6aa0c19')]).
