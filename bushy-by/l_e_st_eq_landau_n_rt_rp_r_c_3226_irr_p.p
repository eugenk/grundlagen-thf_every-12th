thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_real,type, (real : $i), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_real'), [hashroot('d2db906daf81bb10db98f39130dcf91ed77efdd52a7c588d62fcf5700a867d0d')]).
thf(typ_r_ts,type, (r_ts : ($i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_ts'), [hashroot('4faaeb7a5086607e34bf07e9d5a26f1a5683fae80d27a62976d1415c9e3473a6')]).
thf(r_ts_p,axiom, (all_of@(^ [X0:$i] : (in@X0@real))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@real))@(^ [X1:$i] : (is_of@(r_ts@X0@X1)@(^ [X2:$i] : (in@X2@real))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_ts_p'), [hashroot('1e5c326c8a20626d318bb7825a8486545aec9ec528086d834f5f8ee70f196165')]).
thf(typ_complex,type, (complex : $i), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_complex'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_cx')], [hashroot('4bb7039770d4387b91af354ed87100d8f3f5905516b480cc18ed3d1800fa7db1')]).
thf(typ_c_re,type, (c_re : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_re'), [hashroot('f58bb03aa9423c197f7061fb40228142cda4c2d09180a5e9affaf0ce3b33139a')]).
thf(c_re_p,axiom, (all_of@(^ [X0:$i] : (in@X0@complex))@(^ [X0:$i] : (is_of@(c_re@X0)@(^ [X1:$i] : (in@X1@real))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_re_p'), [hashroot('d0a116edc743f935951521a2dccdfd389380590db7f6e623c5f987c00d33d2a0')]).
thf(typ_c_im,type, (c_im : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_im'), [hashroot('6825dc26de02c6c24fffa57aaf7cfe8afab7e43e985540af909a6804538df5c1')]).
thf(c_im_p,axiom, (all_of@(^ [X0:$i] : (in@X0@complex))@(^ [X0:$i] : (is_of@(c_im@X0)@(^ [X1:$i] : (in@X1@real))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_im_p'), [hashroot('0472f42ed40306336b42b31e0d9f65f2136bcb85ced561000f46a14312911512')]).
thf(typ_irr,type, (irr : ($i>$i>$i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_3226_irr'), [hashroot('1ba3e9e8d0a5c441af5d7862dd1516fb1acbbf1e6267afa65ff8517b78b30b0d')]).
thf(def_irr,definition, (irr = (^ [X0:$i] : ^ [X1:$i] : ^ [X2:$i] : (r_ts@(r_ts@(c_im@X0)@(c_re@X1))@(c_re@X2)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_3226_irr')).
thf(irr_p,conjecture, (all_of@(^ [X0:$i] : (in@X0@complex))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@complex))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@complex))@(^ [X2:$i] : (is_of@(irr@X0@X1@X2)@(^ [X3:$i] : (in@X3@real))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_3226_irr_p'), [hashroot('a2f4a2d20f05b60c437bfff4e23b9efeda64d3f5dc33dd7e555201a98bc43d33')]).
