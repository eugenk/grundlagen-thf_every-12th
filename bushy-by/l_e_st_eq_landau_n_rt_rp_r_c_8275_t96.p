thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_d_Pi,type, (d_Pi : ($i>($i>$i)>$i)), unknown, [hashroot('68d9661a0d30c2de45df1cf7708fadc581b6b52cb2ba22f5f89c21527ade885f')]).
thf(typ_all,type, (all : ($i>($i>$o)>$o)), file('grundlagen.aut','l_all'), [hashroot('7af775405bb712aec9ce7cdde103343acfd567932781aa9cedcee8d57f9a249d')]).
thf(def_all,definition, (all = (^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@X0))))), file('grundlagen.aut','l_all')).
thf(typ_nat,type, (nat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_nat'), [hashroot('a70cf6792dc8538ebedafac6b0b6924bbcece48e47a0c0cb77b73b3cb5eba62f')]).
thf(typ_d_1to,type, (d_1to : ($i>$i)), file('grundlagen.aut','l_e_st_eq_landau_n_1to'), [hashroot('5c820bd8f0eeb404ee39eb850016f1c97e4f2665853b2d7d356903656fae6e72')]).
thf(typ_complex,type, (complex : $i), [file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_complex'),file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_cx')], [hashroot('4bb7039770d4387b91af354ed87100d8f3f5905516b480cc18ed3d1800fa7db1')]).
thf(typ_prop6,type, (prop6 : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_prop6'), [hashroot('420b092acd6e73b03ba226dbba3f95deb0064add76d9cc017847bba02fbe4d5e')]).
thf(typ_prop7,type, (prop7 : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_prop7'), [hashroot('4bab611ab92381a4914ad1fc6c9e3f231d5ef17fe673ba16857fadde7db846a5')]).
thf(def_prop7,definition, (prop7 = (^ [X0:$i] : ^ [X1:$i] : (all@(d_Pi@(d_1to@X1)@(^ [X2:$i] : complex))@(prop6@X0@X1)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_prop7')).
thf(k_8275_t95,axiom, (all_of@(^ [X0:$i] : (in@X0@(d_Pi@complex@(^ [X1:$i] : (d_Pi@complex@(^ [X2:$i] : complex))))))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@nat))@(prop7@X0)))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_t95'), [hashroot('12f2135a1478582126a78cfb6cad6a6135d0d9fb8982bff1efd4ea194afdbbdc')]).
thf(k_8275_t96,conjecture, (all_of@(^ [X0:$i] : (in@X0@(d_Pi@complex@(^ [X1:$i] : (d_Pi@complex@(^ [X2:$i] : complex))))))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@(d_Pi@(d_1to@X1)@(^ [X3:$i] : complex))))@(prop6@X0@X1)))))), file('grundlagen.aut','l_e_st_eq_landau_n_rt_rp_r_c_8275_t96'), [hashroot('27cb9eaf34e673de77670ee1cd6ec1fe4ad6f13ba31e5f9a5f1d80c9a1362a2b')]).
