thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_nat,type, (nat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_nat'), [hashroot('a70cf6792dc8538ebedafac6b0b6924bbcece48e47a0c0cb77b73b3cb5eba62f')]).
thf(typ_n_some,type, (n_some : (($i>$o)>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_some'), [hashroot('25dbba53aea6be6e2f767148e9033334d9ae05f69ba26e54579bb754d6a6ebba')]).
thf(typ_diffprop,type, (diffprop : ($i>$i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_diffprop'), [hashroot('2e8b48492fbe98ad8abd255647365aed09e61c9fa8f45d82090ec63cde5e7179')]).
thf(typ_d_29_ii,type, (d_29_ii : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_29_ii'),file('grundlagen.aut','l_e_st_eq_landau_n_more')], [hashroot('0fa564fe1936df3a9a68920cb60fb83fe8c399deb0b6e0fc234e27ebfc6cae48')]).
thf(def_d_29_ii,definition, (d_29_ii = (^ [X0:$i] : ^ [X1:$i] : (n_some@(diffprop@X0@X1)))), [file('grundlagen.aut','l_e_st_eq_landau_n_29_ii'),file('grundlagen.aut','l_e_st_eq_landau_n_more')]).
thf(typ_iii,type, (iii : ($i>$i>$o)), [file('grundlagen.aut','l_e_st_eq_landau_n_29_iii'),file('grundlagen.aut','l_e_st_eq_landau_n_less')], [hashroot('6ba9e7ba6008214c808ec0406c3dfdbb0319337c703d55a467d2b6caa77cc18b')]).
thf(def_iii,definition, (iii = (^ [X0:$i] : ^ [X1:$i] : (n_some@(diffprop@X1@X0)))), [file('grundlagen.aut','l_e_st_eq_landau_n_29_iii'),file('grundlagen.aut','l_e_st_eq_landau_n_less')]).
thf(typ_moreis,type, (moreis : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_moreis'), [hashroot('95f95f3214cfc9250bd088fb15491daf7bacc7e48f1e481efc187a17d3e1ff47')]).
thf(typ_lessis,type, (lessis : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_lessis'), [hashroot('6543a1825fc715c37a10a00b8bfcee01bcd1c22f9a984a5a3a79250af7b6563d')]).
thf(satz13,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : ((moreis@X0@X1) => (lessis@X1@X0)))))), file('grundlagen.aut','l_e_st_eq_landau_n_satz13'), [hashroot('a331ce424fa0685187c3f3673a82e3035b21646d47a88b07cdf24d4bcd74a023')]).
thf(satz16b,axiom, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@nat))@(^ [X2:$i] : ((iii@X0@X1) => ((lessis@X1@X2) => (iii@X0@X2))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_satz16b'), [hashroot('89da30153d7844f57211f733bdac12ce73b45a4f3f31f029648b634ed1567d87')]).
thf(satz16c,conjecture, (all_of@(^ [X0:$i] : (in@X0@nat))@(^ [X0:$i] : (all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@nat))@(^ [X2:$i] : ((moreis@X0@X1) => ((d_29_ii@X1@X2) => (d_29_ii@X0@X2))))))))), file('grundlagen.aut','l_e_st_eq_landau_n_satz16c'), [hashroot('12a787d980ffabd70f4c93f995ed0964a205320a287435aded85fce6527534db')]).
