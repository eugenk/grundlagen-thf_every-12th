thf(typ_is_of,type, (is_of : ($i>($i>$o)>$o)), unknown, [hashroot('01b8315391fd465f550f1a3788664028f3a6126b6bded775854e3b7d33765cd9')]).
thf(def_is_of,definition, (is_of = (^ [X0:$i] : ^ [X1:($i>$o)] : (X1@X0))), unknown).
thf(typ_all_of,type, (all_of : (($i>$o)>($i>$o)>$o)), unknown, [hashroot('67f26991e531d6c57a2161d295d3cdf1f416713aa711d23ccb703b2431633300')]).
thf(def_all_of,definition, (all_of = (^ [X0:($i>$o)] : ^ [X1:($i>$o)] : ! [X2:$i] : ((is_of@X2@X0) => (X1@X2)))), unknown).
thf(typ_in,type, (in : ($i>$i>$o)), unknown, [hashroot('fb8da7eb5b1b399e7321179dac9e9f65773d7331e1e30554e3911e4325e1ef19')]).
thf(typ_imp,type, (imp : ($o>$o>$o)), [file('grundlagen.aut','l_imp'),file('grundlagen.aut','l_r_imp')], [hashroot('c471f7f9b55e8fce15c5b7f92a344808917c7bdfbac8043315ef5a52fb233e79')]).
thf(l_mp,axiom, (! [X0:$o] : ! [X1:$o] : (X0 => ((imp@X0@X1) => X1))), [file('grundlagen.aut','l_mp'),file('grundlagen.aut','l_r_mp')], [hashroot('015cd300e0c67d86633e1f151d83470dfe10f6723378f7a60a6693da57c11a26')]).
thf(typ_nat,type, (nat : $i), file('grundlagen.aut','l_e_st_eq_landau_n_nat'), [hashroot('a70cf6792dc8538ebedafac6b0b6924bbcece48e47a0c0cb77b73b3cb5eba62f')]).
thf(typ_lessis,type, (lessis : ($i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_lessis'), [hashroot('6543a1825fc715c37a10a00b8bfcee01bcd1c22f9a984a5a3a79250af7b6563d')]).
thf(typ_lbprop,type, (lbprop : (($i>$o)>$i>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_327_lbprop'), [hashroot('a072d99a121fa742fa71c248cd762fbdde0664005bad0cc1b4d6e672b0e8e1f3')]).
thf(def_lbprop,definition, (lbprop = (^ [X0:($i>$o)] : ^ [X1:$i] : ^ [X2:$i] : (imp@(X0@X2)@(lessis@X1@X2)))), file('grundlagen.aut','l_e_st_eq_landau_n_327_lbprop')).
thf(typ_min,type, (min : (($i>$o)>$i>$o)), file('grundlagen.aut','l_e_st_eq_landau_n_min'), [hashroot('a36035fb977370c0f64bcaf3dcdd0bc9c93a0696d45ebdf181c4634b900162b4')]).
thf(k_327_t41,axiom, (! [X0:($i>$o)] : (all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : ((min@X0@X1) => (X0@X1))))), [file('grundlagen.aut','l_e_st_eq_landau_n_327_t41'),file('grundlagen.aut','l_e_st_eq_landau_n_327_t42')], [hashroot('205d51eef0bcd681626c508177ff45c936a493d25cc01c7d1807a202d888ed2d')]).
thf(k_327_t44,axiom, (! [X0:($i>$o)] : (all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@nat))@(^ [X2:$i] : ((min@X0@X2) => (lbprop@X0@X2@X1))))))), file('grundlagen.aut','l_e_st_eq_landau_n_327_t44'), [hashroot('59be7fdc9e6ce4766c6507a99c0d23c3032ddcc262e578265320d79a4124939e')]).
thf(k_327_t46,conjecture, (! [X0:($i>$o)] : (all_of@(^ [X1:$i] : (in@X1@nat))@(^ [X1:$i] : (all_of@(^ [X2:$i] : (in@X2@nat))@(^ [X2:$i] : ((min@X0@X1) => ((min@X0@X2) => (lessis@X2@X1)))))))), file('grundlagen.aut','l_e_st_eq_landau_n_327_t46'), [hashroot('c3cf187501083a9238a22bfb1afdc33cec0e7abede5337c156e589741da932a2')]).
